package com.xzm.baselibrary.utils;

import android.app.Application;
import android.content.Context;

import com.xzm.baselibrary.app.BaseApplication;

/**
 * Created by xuzongmeng on 2018/1/21.
 */

public class ContextHolder {

    public static Context getContext() {
        return BaseApplication.getContext();
    }
}
