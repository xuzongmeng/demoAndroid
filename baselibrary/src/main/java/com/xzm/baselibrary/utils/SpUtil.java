package com.xzm.baselibrary.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;


import com.xzm.baselibrary.app.BaseApplication;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Set;

public class SpUtil {

    private static final String TAG = SpUtil.class.getSimpleName();

    public static String PREFERENCE_NAME = "base_sp";

    private static Context mContext = BaseApplication.getContext();

    private SpUtil() {
        throw new AssertionError();
    }

    public static void initContext(Context context) {
        mContext = context;
    }

    public static boolean putString(String key, String value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getString(String key) {
        return getString(key, null);
    }

    public static String getString(String key, String defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static boolean putInt(String key, int value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }



    public static int getInt(String key) {
        return getInt(key, -1);
    }

    public static int getInt(String key, int defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }

    public static boolean putLong(String key, long value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(String key) {
        return getLong(key, -1);
    }

    public static long getLong(String key, long defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getLong(key, defaultValue);
    }

    public static boolean putFloat(String key, float value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static float getFloat(String key) {
        return getFloat(key, -1);
    }

    public static float getFloat(String key, float defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getFloat(key, defaultValue);
    }

    public static boolean putBoolean(String key, boolean value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getBoolean(key, defaultValue);
    }

    public static void putObject(String key, Object object) {
        SharedPreferences sp = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(baos);
            out.writeObject(object);
            String objectVal = new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(key, objectVal);
            editor.commit();
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        finally {
            if (baos != null) {
                try {
                    baos.close();
                }
                catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
    }

    @SuppressWarnings( "unchecked" )
    public static <T> T getObject(String key, Class<T> clazz) {
        SharedPreferences sp = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (sp.contains(key)) {
            String objectVal = sp.getString(key, null);
            byte[] buffer = Base64.decode(objectVal, Base64.DEFAULT);
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(bais);
                T t = (T)ois.readObject();
                return t;
            }
            catch (StreamCorruptedException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            catch (ClassNotFoundException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            finally {

                if (bais != null) {
                    try {
                        bais.close();
                    }
                    catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
                if (ois != null) {
                    try {
                        ois.close();
                    }
                    catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
            }
        }
        return null;
    }

    public static boolean putStringSet(String key, Set<String> value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putStringSet(key, value);
        return editor.commit();
    }

    public static Set<String> getStringSet(String key) {
        return getStringSet(key, null);
    }

    public static Set<String> getStringSet(String key, Set<String> defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        return settings.getStringSet(key, defaultValue);
    }

    public static boolean remove(String key) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME,
            Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        return editor.commit();
    }
}
