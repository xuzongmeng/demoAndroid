package com.xzm.baselibrary.net;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   :
 */
public class OkHttpInterceptor implements Interceptor{

    String TAG = "CustomInterceptor";
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request()
                .newBuilder()
                .header("User-agent", "Android")
                .build();
        return chain.proceed(request);
    }
//    @Override
//    public Response intercept(Interceptor.Chain chain) throws IOException {
//        Request original = chain.request();
//
//        HttpUrl originalHttpUrl = original.url();
//
//        HttpUrl.Builder builder = originalHttpUrl
//                .newBuilder();
//        String currentUserInfo = SpUtil.getString("user_info", "");
//
//
//        String ACCESS_TOKEN = "XO6y6TJ8yP8f15tE07r5uGOYj4VlBn8hB2EDlf";
//        if(!TextUtils.isEmpty(currentUserInfo)){
//            try {
//                JSONObject jsonObject = new JSONObject(currentUserInfo);
//                ACCESS_TOKEN = jsonObject.getString("accessToken");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        HttpUrl url = builder.build();
//        Request request = original
//                .newBuilder()
//                .addHeader("ACCESS_TOKEN",ACCESS_TOKEN)
//                .url(url)
//                .build();
//        okhttp3.Response response = chain.proceed(request);
//
//        return response;
//    }
}
