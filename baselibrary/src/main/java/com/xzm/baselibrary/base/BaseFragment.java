//package com.xzm.baselibrary.base;
//
//import android.content.Context;
//import android.databinding.DataBindingUtil;
//import android.databinding.ViewDataBinding;
//import android.os.Bundle;
//import android.support.annotation.LayoutRes;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//
//import com.xzm.baselibrary.R;
//import com.xzm.baselibrary.databinding.FragmentBaseLayoutBinding;
//import com.xzm.baselibrary.utils.DeviceUtil;
//import com.xzm.baselibrary.utils.ToastUtil;
//import com.xzm.baselibrary.widget.LoadingLayout;
//
//
///**
// * fragment基类
// *
// * @author xuzongmeng
// */
//public abstract class BaseFragment<VB extends ViewDataBinding> extends Fragment {
//    protected static final String TAG = "BaseFragment";
//    public Context mContext;
//    /**
//     * 是否是第一次加载数据，可以通过此标志在子类判断 是否加载中， 可避免重复请求网络
//     */
//    protected boolean isLoad = false;
//    protected VB binding;
//    /**
//     * 视图是否已经初初始化
//     */
//    private boolean isInit = false;
//    private FrameLayout mContainer;
//    private LoadingLayout mLoadingLayout;
//    private LayoutInflater mInflater;
//    private String currentClassName;
//    private FragmentBaseLayoutBinding baseBinding;
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mContext = context;
//    }
//
//    // 处理fragment的布局
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        isInit = true;
//        this.mInflater = inflater;
//        baseBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_base_layout, null, false);
//        mContainer = baseBinding.flBaseFragmentContainer;
//        mLoadingLayout = baseBinding.loadinglayoutBaseFragment;
//        mLoadingLayout.setOnReloadListener(v -> {
//            showLoading();
//            onReLoadDate();
//        });
//        putContentView(getLayoutID());
//        initViews(baseBinding.getRoot());
//        isCanLoadData();
//        /* 初始化的时候去加载数据*/
//        return baseBinding.getRoot();
//    }
//
//    // 依附的activity创建完成
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        currentClassName = this.getClass().getSimpleName();
//        initDates();
//    }
////    /**
////     * 利用DataBindingUtil 将子Layout填充到父容器中
////     */
////    private <T extends ViewDataBinding> T putContentView(@LayoutRes int resId) {
////        return DataBindingUtil.inflate(getLayoutInflater(), resId, baseBinding.flBaseFragmentContainer, true);
////    }
//    /**
//     * 利用DataBindingUtil 将子Layout填充到父容器中
//     */
//    protected  void putContentView(@LayoutRes int resId) {
//        binding = DataBindingUtil.inflate(getLayoutInflater(), resId, baseBinding.flBaseFragmentContainer, true);
//    }
//    /**
//     * 视图是否已经对用户可见，系统的方法
//     */
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        Log.d(TAG, "基类Fragment=" + currentClassName + "可见性=" + isVisibleToUser);
//        Log.d(TAG, "setUserVisibleHint");
//        isCanLoadData();
//    }
//
//    /**
//     * 是否可以加载数据
//     * 可以加载数据的条件：
//     * 1.视图已经初始化
//     * 2.视图对用户可见
//     */
//    private void isCanLoadData() {
//        if (!isInit) {
//            return;
//        }
//        if (getUserVisibleHint()) {
//            lazyLoad();
//            isLoad = true;
//        } else {
//            if (isLoad) {
//                stopLoad();
//            }
//        }
//    }
//
//    /**
//     * 设置布局ID，子类必须实现滴方法
//     *
//     * @return
//     */
//    public abstract int getLayoutID();
//
//    /**
//     * getLayoutID完成后 初始化View，由子类去实现
//     *
//     */
//    protected abstract void initViews(View view);
//
//    /**
//     * 当视图初始化并且对用户可见的时候去真正的加载数据
//     * 初始化数据, 可以不实现 如果想事实更新 每个Fragment 来回切换时候数据，可以重载此方法，
//     * 如果每个Fragment只想请求一次网络数据 那么可以在子类用isLoadDate标志去判断
//     */
//    protected void lazyLoad() {
//
//    }
//
//    /**
//     * 当视图已经对用户不可见并且加载过数据，如果需要在切换到其他页面时停止加载数据，可以调用此方法
//     */
//    protected void stopLoad() {
//    }
//
//    /**
//     * 初始化数据, 可以不实现，如果不想用懒加载滴加载数据 可以重载此方法,
//     * 所有Fragment一次性加载所有数据
//     */
//
//    protected void initDates() {
//    }
////    public static BaseFragment newInstance(Fragment fragemntClass) {
////        fragemntClass fragment = new fragemntClass();
////        Bundle bundle = new Bundle();
////        bundle.putString(PLAYER_ID, play_id);
////        fragment.setArguments(bundle);
////        return fragment;
////    }
//
//    /**
//     * 重新加载数据   当子类需要时 可以重载此方法
//     */
//    public void onReLoadDate() {
//
//    }
//
//    /**
//     * 默认是显示Toolbar标题
//     */
//    public boolean isShowToolBar() {
//        return true;
//    }
//
//    /**
//     * 显示加载中
//     */
//    public void showLoading() {
//        if (DeviceUtil.isConnectNet(mContext)) {
//            mLoadingLayout.setStatus(LoadingLayout.Loading);
//            Log.d(TAG, "基类联网了");
//        } else {
//            mLoadingLayout.setStatus(LoadingLayout.No_Network);
//            ToastUtil.showToast("联网失败，请检测网络后重试");
//            Log.d(TAG, "基类没有联网");
//        }
//
//    }
//
//
////
////    /**
////     * 显示动画加载中
////     */
////    public void showAnimationLoading() {
////        if (DeviceUtil.isConnectNet(mContext)) {
////            MyContentView myContentView =new MyContentView(mContext);
////            mLoadingLayout.setLoadingPage(myContentView.getContentView());
////            mLoadingLayout.setStatus(LoadingLayout.Loading);
////            Log.d(TAG, "基类联网了");
////        } else {
////            mLoadingLayout.setStatus(LoadingLayout.No_Network);
////            ToastUtil.showToast("联网失败，请检测网络后重试");
////            Log.d(TAG, "基类没有联网");
////        }
////
////    }
//
//
//
//    /*=========================================================================================*/
//
//    /**
//     * 当项目不满足需时 可传入自己LayoutID，显示不同的内容视图 方便扩展
//     * （根据用户自定义的View或者LayoutID）
//     * 这种方式是让LoadingLayout中滴容器去加载View或者Layout
//     */
//    public void showContent(int resId) {
//        mContainer.removeViewAt(0);
//        mContainer.addView(mInflater.inflate(resId, null));
//
//    }
//
//    public void showContent(View view) {
//        mContainer.removeViewAt(0);
//        mContainer.addView(view);
//    }
//    /*=========================================================================================*/
//
//    /**
//     * 当项目不满足需时 可传入自己View，显示不同的内容视图 方便扩展
//     * （根据用户自定义的View或者Layout ）
//     * 这种方式是让LoadingLayout本身去加载View或者Layout
//     */
////    public void showContent(View  view) {
////        mLoadingLayout.setLoadingPage(view);
////        mLoadingLayout.setStatus(LoadingLayout.Loading);
////
////    }
////
////    public void showContent(int  resId) {
////        mLoadingLayout.setLoadingPage(resId);
////        mLoadingLayout.setStatus(LoadingLayout.Loading);
////
////    }
//
//    /**
//     * 显示内容视图
//     */
//    public void showContent() {
//        mLoadingLayout.setStatus(LoadingLayout.Success);
//    }
//
//    /**
//     * 显示空数据 ，暂时没有数据
//     */
//
//    public void showEmpty() {
//        mLoadingLayout.setStatus(LoadingLayout.Empty);
//    }
//
//    /**
//     * 显示页面错误
//     */
//    public void showError() {
//        mLoadingLayout.setStatus(LoadingLayout.Error);
//    }
//
//    /**
//     * 显示没有网络的情况
//     */
//    public void showNoNetwork() {
//        mLoadingLayout.setStatus(LoadingLayout.No_Network);
//    }
//}
