//package com.xzm.baselibrary.base;
//
//import android.content.Context;
//import android.databinding.DataBindingUtil;
//import android.databinding.ViewDataBinding;
//import android.os.Bundle;
//import android.support.annotation.LayoutRes;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.TextView;
//
//import com.xzm.baselibrary.R;
//import com.xzm.baselibrary.databinding.ActivityBaseLayoutBinding;
//import com.xzm.baselibrary.utils.DeviceUtil;
//import com.xzm.baselibrary.utils.ToastUtil;
//import com.xzm.baselibrary.widget.LoadingLayout;
//import com.xzm.baselibrary.widget.MyContentView;
//
//
///**
// * Created by xuzongmeng on 2016/12/12.
// * <p>
// * C:\Users\wwx229495>net_stat -aon|find_str 5037
// * TCP    127.0.0.1:5037         0.0.0.0:0              LISTENING       3676
// * 通过PID查看所有进程
// * C:\Users\wwx229495>task_list /fi "PID eq 3676"   用的时候去掉下划线
// * 系统环境变量下 配置 端口 ANDROID_ADB_SERVER_PORT
// */
//
//public abstract class BaseActivity<VB extends ViewDataBinding> extends AppCompatActivity {
//    private static final String TAG = "Base2Activity";
//    public Context mContext;
//    public Toolbar toolBar;
//    public LayoutInflater mInflater;
//    protected VB binding;
//    protected FrameLayout mContainer;
//    private TextView tvToolbaerTitle;
//    private LoadingLayout mLoadingLayout;
//    private ActivityBaseLayoutBinding baseBinding;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        int resID = getLayoutID();
////        if (resID != 0) {
////
////        }
//        //           requestWindowFeature(Window.FEATURE_NO_TITLE);
//        baseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base_layout);
//        mContext = this;
//        initBaseView();
//        String toolBarTitle = getToolBarTitle();
//        if (!TextUtils.isEmpty(toolBarTitle)) {
//            tvToolbaerTitle.setText(toolBarTitle);
//        }
//        if (!isShowToolBar()) {
//            toolBar.setVisibility(View.GONE);
//        }
//        putContentView(getLayoutID());
//        initViews(savedInstanceState);
//        initDates();
//    }
//
////    /**
////     * 利用DataBindingUtil 将子Layout填充到父容器中
////     */
////    protected  <T extends ViewDataBinding> T putContentView(@LayoutRes int resId) {
////        return DataBindingUtil.inflate(getLayoutInflater(), resId, baseBinding.flContentView, true);
////    }
//
//    /**
//     * 利用DataBindingUtil 将子Layout填充到父容器中
//     */
//    protected  void  putContentView(@LayoutRes int resId) {
//        binding = DataBindingUtil.inflate(getLayoutInflater(), resId, baseBinding.flContentView, true);
//    }
//    /**
//     * 初始化基类的View
//     */
//    private void initBaseView() {
//        mInflater = LayoutInflater.from(this);
//        toolBar = baseBinding.toolbarBase;
//        toolBar.setTitle("");
//        setSupportActionBar(toolBar);
//        tvToolbaerTitle = baseBinding.tvBaseToolbarTitle;
//        mLoadingLayout = baseBinding.loadinglayoutBase;
//        mContainer = baseBinding.flContentView;
//        mLoadingLayout.setOnReloadListener(v -> {
//            showLoading();
//            onReLoadDate();
//        });
//        toolBar.setNavigationOnClickListener(view -> finish());
//    }
//
//    /**
//     * 设置导航栏标题
//     */
//    protected abstract String getToolBarTitle();
//
//    /**
//     * 初始化子View
//     */
//    protected abstract void initViews(Bundle savedInstanceState);
//
//    /**
//     * 获取子Layout
//     */
//    protected abstract int getLayoutID();
//
//    /**
//     * 初始化子类数据
//     */
//    public void initDates() {
//
//    }
//
//    /**
//     * 重新加载数据   当子类需要时 可以重载此方法
//     */
//    public void onReLoadDate() {
//
//    }
//
//    /**
//     * 默认是显示Toolbar标题
//     */
//    public boolean isShowToolBar() {
//        return true;
//    }
//
//    /**
//     * 显示加载中
//     */
//    public void showLoading() {
//        if (DeviceUtil.isConnectNet(this)) {
//            mLoadingLayout.setStatus(LoadingLayout.Loading);
//            Log.d(TAG, "基类联网了");
//        } else {
//            mLoadingLayout.setStatus(LoadingLayout.No_Network);
//            ToastUtil.showToast("联网失败，请检测网络后重试");
//            Log.d(TAG, "基类没有联网");
//        }
//
//    }
//
//
//    /**
//     * 显示动画加载中
//     */
//    public void showAnimationLoading() {
//        if (DeviceUtil.isConnectNet(this)) {
//            MyContentView myContentView = new MyContentView(this);
//            mLoadingLayout.setLoadingPage(myContentView.getContentView());
//            mLoadingLayout.setStatus(LoadingLayout.Loading);
//            Log.d(TAG, "基类联网了");
//        } else {
//            mLoadingLayout.setStatus(LoadingLayout.No_Network);
//            ToastUtil.showToast("联网失败，请检测网络后重试");
//            Log.d(TAG, "基类没有联网");
//        }
//
//    }
//
//
//
//    /*=========================================================================================*/
//
//    /**
//     * 当项目不满足需时 可传入自己LayoutID，显示不同的内容视图 方便扩展
//     * （根据用户自定义的View或者LayoutID）
//     * 这种方式是让LoadingLayout中滴容器去加载View或者Layout
//     */
//    public void showContent(int resId) {
//        if (mContainer.getChildAt(0) != null) {
//            mContainer.removeViewAt(0);
//        }
//        mContainer.addView(mInflater.inflate(resId, null));
//
//    }
//
//    public void showContent(View view) {
//        if (mContainer.getChildAt(0) != null) {
//            mContainer.removeViewAt(0);
//        }
//        mContainer.addView(view);
//    }
//    /*=========================================================================================*/
//
//    /**
//     * 当项目不满足需时 可传入自己View，显示不同的内容视图 方便扩展
//     * （根据用户自定义的View或者Layout ）
//     * 这种方式是让LoadingLayout本身去加载View或者Layout
//     */
////    public void showContent(View  view) {
////        mLoadingLayout.setLoadingPage(view);
////        mLoadingLayout.setStatus(LoadingLayout.Loading);
////
////    }
////
////    public void showContent(int  resId) {
////        mLoadingLayout.setLoadingPage(resId);
////        mLoadingLayout.setStatus(LoadingLayout.Loading);
////
////    }
//
//    /**
//     * 显示内容视图
//     */
//    public void showContent() {
//        mLoadingLayout.setStatus(LoadingLayout.Success);
//    }
//
//    /**
//     * 显示空数据 ，暂时没有数据
//     */
//
//    public void showEmpty() {
//        mLoadingLayout.setStatus(LoadingLayout.Empty);
//    }
//
//    /**
//     * 显示页面错误
//     */
//    public void showError() {
//        mLoadingLayout.setStatus(LoadingLayout.Error);
//    }
//
//    /**
//     * 显示没有网络的情况
//     */
//    public void showNoNetwork() {
//        mLoadingLayout.setStatus(LoadingLayout.No_Network);
//    }
//
//
//    private void testMethod() {
//        LoadingLayout.getConfig()
//                .setErrorText("出错啦~请稍后重试！")
//                .setEmptyText("抱歉，暂无数据")
//                .setNoNetworkText("无网络连接，请检查您的网络···")
//                .setErrorImage(R.mipmap.ic_launcher)
//                .setEmptyImage(R.mipmap.ic_launcher)
//                .setNoNetworkImage(R.mipmap.ic_launcher)
//                .setAllTipTextColor(R.color.gray)
//                .setAllTipTextSize(14)
//                .setReloadButtonText("点我重试哦")
//                .setReloadButtonTextSize(14)
//                .setReloadButtonTextColor(R.color.gray)
//                .setReloadButtonWidthAndHeight(150, 40);
//
//
///**
// * 由于“加载中”的页面，可能每个App都不一样，因此，LoadingLayout支持自定义LoadingPage，如下：
// */
//        LoadingLayout.getConfig()
//                .setLoadingPageLayout(R.layout.widget_loading_page);
//
//    }
//
//
////    @Override
////    public void onBackPressed() {
////        super.onBackPressed();
////        overridePendingTransition(R.anim.activity_anim_in_right, R.anim.activity_anim_in_right);
////    }
//
//}
