//package com.xzm.baselibrary.test;
//
//import android.Manifest;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.graphics.Bitmap;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.content.ContextCompat;
//import android.view.KeyEvent;
//import android.view.ViewGroup;
//import android.webkit.GeolocationPermissions;
//import android.webkit.WebChromeClient;
//import android.webkit.WebResourceError;
//import android.webkit.WebResourceRequest;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.Toast;
//
//import com.orhanobut.logger.Logger;
//import com.xzm.baselibrary.R;
//import com.xzm.baselibrary.annotation.AfterPermissionGranted;
//import com.xzm.baselibrary.app.BaseApplication;
//import com.xzm.baselibrary.base.Base2Activity;
//import com.xzm.baselibrary.constants.Constants;
//import com.xzm.baselibrary.utils.DeviceUtil;
//import com.xzm.baselibrary.utils.EasyPermissions;
//import com.xzm.baselibrary.utils.ToastUtil;
//
//import java.util.List;
//
//public class WebViewActivity extends Base2Activity implements EasyPermissions.PermissionCallbacks {
//    private WebView webView;
//
//
//
//    private void initView() {
////        showNormalDialog();
//        int color = ContextCompat.getColor(BaseApplication.getContext(), R.color.Orange);
////        BarUtils.setStatusBarColor(this, color);
//        webView = findViewById(R.id.web_view);
//    }
//
//    private void initData() {
//        WebSettings settings = webView.getSettings();
//// 缓存(cache)
//        settings.setAppCacheEnabled(true);      // 默认值 false
//        settings.setAppCachePath(getCacheDir().getAbsolutePath());
//// 存储(storage)
//        settings.setDomStorageEnabled(true);    // 默认值 false
//        settings.setDatabaseEnabled(true);      // 默认值 false
//
////        settings.setRenderPriority(WebSettings.RenderPriority.HIGH); //提高渲染的优先级
//
//// 是否支持viewport属性，默认值 false
//// 页面通过`<meta name="viewport" ... />`自适应手机屏幕
//        settings.setUseWideViewPort(true);
//// 是否使用overview mode加载页面，默认值 false
//// 当页面宽度大于WebView宽度时，缩小使页面宽度等于WebView宽度
//        settings.setLoadWithOverviewMode(false);
//// 是否支持Javascript，默认值false
//        settings.setJavaScriptEnabled(true);
//        settings.setBlockNetworkImage(false);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
//            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        }
//        if (DeviceUtil.isConnectNet(this)) {
//            // 根据cache-control决定是否从网络上取数据
//            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
//        } else {
//            // 没网，离线加载，优先加载缓存(即使已经过期)
//            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        }
//
//        //启用地理定位
//        settings.setGeolocationEnabled(true);
//        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
//        //设置定位的数据库路径 已废弃Deprecated
////        settings.setGeolocationDatabasePath(dir);  // deprecation
////        settings.setPluginsEnabled(true);
////        settings.setJavaScriptCanOpenWindowsAutomatically(true);
//
//        webView.setWebViewClient(new MyBridgeWebViewClient());
//        webView.setWebChromeClient(new MyWebChromeClient());
//        String URL = Constants.TEST_ICCBXXX;
//        webView.loadUrl(URL);
//        Logger.d("=请求地址==" + URL);
//    }
//
//    @Override
//    public void onBackPressed() {
//        webView.goBack();
////        super.onBackPressed();
//    }
//
//    @Override
//    public boolean isShowToolBar() {
//        return false;
//    }
//
//    @Override
//    protected String getToolBarTitle() {
//        return "WebView看电影";
//    }
//
//    @Override
//    protected void initViews(Bundle savedInstanceState) {
//        checkLocationPermission();
//    }
//
//    @Override
//    protected int getLayoutID() {
//        return R.layout.activity_web_view;
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            webView.goBack();
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    /**
//     * 重写WebViewClient
//     */
//    private class MyBridgeWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//            return super.shouldOverrideUrlLoading(view, request);
//        }
////        @Override
////        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//////            Log.d("webView=" + url);
////            try {
////                url = URLDecoder.decode(url, "UTF-8");
////            } catch (UnsupportedEncodingException e) {
////                e.printStackTrace();
////            }
////
////            if (url.startsWith("yy://return/")) { // 如果是返回数据
//////                webView.handlerReturnData(url);
////                return true;
////            } else if (url.startsWith("yy://")) { //
//////                webView.flushMessageQueue();
////                return true;
////            } else {
////                return super.shouldOverrideUrlLoading(view, url);
////            }
////        }
//
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            showLoading();
////            if (NetworkUtils.isConnected()) {
////                showLoading();
////            } else {
////                showNoNetwork();
////            }
//            super.onPageStarted(view, url, favicon);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//            showContent();
//        }
//
//        @Override
//        public void onReceivedError(WebView view, WebResourceRequest resourceRequest, WebResourceError error) {
////            showNoNetwork();
//            super.onReceivedError(view, resourceRequest, error);
//        }
//    }
//
//    private class MyWebChromeClient extends WebChromeClient {
//        @Override
//        public void onReceivedIcon(WebView view, Bitmap icon) {
//            super.onReceivedIcon(view, icon);
//        }
//
//        //        @Override
////        public void onGeolocationPermissionsShowPrompt(String origin,GeolocationPermissions.Callback callback) {
////            callback.invoke(origin, true, false);
////            super.onGeolocationPermissionsShowPrompt(origin, callback);
////
////        }
//// 指定源的网页内容在没有设置权限状态下尝试使用地理位置API。
//        @Override
//        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
////            boolean allow = true;   // 是否允许origin使用定位API
////            boolean retain = false; // 内核是否记住这次制授权
//            callback.invoke(origin, true, false);
//            super.onGeolocationPermissionsShowPrompt(origin, callback);
//        }
//
////        @Override
////        public void onGeolocationPermissionsHidePrompt() {
////            super.onGeolocationPermissionsHidePrompt();
////        }
//
//    }
//
//    @AfterPermissionGranted(LOCATION_PERMISSION)
//    private void checkLocationPermission() {
//        if (EasyPermissions.hasPermissions(mContext, Manifest.permission.ACCESS_COARSE_LOCATION,
//                Manifest.permission.ACCESS_FINE_LOCATION)) {
//            initView();
//            initData();
//        } else {
//            // Ask for one permission
//            EasyPermissions.requestPermissions(this, "我们需要访问你的定位权限",
//                    LOCATION_PERMISSION, Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_FINE_LOCATION);
//        }
//    }
//
//    private static final int LOCATION_PERMISSION = 125;
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        // EasyPermissions handles the request result.
//        try {
//            ToastUtil.showToast("处理结果");
//            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
//        } catch (Exception e) {
//            Logger.d("权限获取成功onRequestPermissionsResult");
//        }
//    }
//
//    @Override
//    public void onPermissionsGranted(int requestCode, List<String> perms) {
//        ToastUtil.showToast("同意权限");
//        Logger.i("相机权限获取成功");
//    }
//
//    @Override
//    public void onPermissionsDenied(int requestCode, List<String> perms) {
//        ToastUtil.showToast("拒绝权限");
//        Logger.i("相机权限获取失败");
//        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
//                "我们需要打开为您拍照，请允许获取相机权限",
//                R.string.setting, R.string.cancel, null, perms);
//    }
//
//    @Override
//    public void onReLoadDate() {
//        checkLocationPermission();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (webView != null) {
//            webView.setWebViewClient(null);
//            webView.setWebChromeClient(null);
//            webView.loadDataWithBaseURL
//                    (null, "", "text/html", "utf-8", null);
//            webView.clearHistory();
//            ((ViewGroup) webView.getParent()).removeView(webView);
//            webView.destroy();
//            webView = null;
//        }
//
//        //清空所有Cookie
////        CookieSyncManager.createInstance(BaseApplication.getContext());  //Create a singleton CookieSyncManager within a context
////        CookieManager cookieManager = CookieManager.getInstance(); // the singleton CookieManager instance
////        cookieManager.removeAllCookie();// Removes all cookies.
////        CookieSyncManager.getInstance().sync(); // forces sync manager to sync now
////
////        webView.setWebChromeClient(null);
////        webView.setWebViewClient(null);
////        webView.getSettings().setJavaScriptEnabled(false);
////        webView.clearCache(true);
//    }
//
//
//    private void showNormalDialog(){
//        //创建dialog构造器
//        AlertDialog.Builder normalDialog = new AlertDialog.Builder(this);
//        //设置title
//        normalDialog.setTitle("弹窗");
//        //设置icon
//        normalDialog.setIcon(R.mipmap.ic_launcher_round);
//        //设置内容
//        normalDialog.setMessage("信息");
//        //设置按钮
//        normalDialog.setPositiveButton(getString(R.string.dialog_btn_confirm_text)
//                , new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(WebViewActivity.this,getString(R.string.dialog_btn_confirm_hint_text)
//                                ,Toast.LENGTH_SHORT).show();
//                        dialog.dismiss();
//                    }
//                });
//        //创建并显示
//        normalDialog.create().show();
//    }
//}
