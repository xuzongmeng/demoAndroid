package com.xzm.baselibrary.test;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.orhanobut.logger.Logger;
import com.xzm.baselibrary.R;
import com.xzm.baselibrary.annotation.AfterPermissionGranted;
import com.xzm.baselibrary.app.BaseApplication;
import com.xzm.baselibrary.base.Base2Activity;
import com.xzm.baselibrary.constants.Constants;
import com.xzm.baselibrary.utils.DeviceUtil;
import com.xzm.baselibrary.utils.EasyPermissions;
import com.xzm.baselibrary.utils.ToastUtil;

import java.util.List;

public class WebViewActivity2 extends Base2Activity implements EasyPermissions.PermissionCallbacks {
    private WebView webView;

    @Override
    public boolean isShowToolBar() {
        return true;
    }

    @Override
    protected String getToolBarTitle() {
        return "测试WebView2";
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        checkLocationPermission();
        checkPermission();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int checkPermission = ContextCompat.checkSelfPermission(WebViewActivity2.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (checkPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(WebViewActivity2.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                ActivityCompat.requestPermissions(WebViewActivity2.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_web_view2;
    }
    private void initView() {
        int color = ContextCompat.getColor(BaseApplication.getContext(), R.color.Orange);
//       BarUtils.setStatusBarColor(this, color);
        webView = findViewById(R.id.web_view);
        Button btn_invoke_js1 = findViewById(R.id.btn_invoke_js1);
        Button btn_invoke_js2 =  findViewById(R.id.btn_invoke_js2);
        btn_invoke_js1.setOnClickListener(view -> webView.loadUrl("javascript:javacalljs()"));


         String data="{\n" +
                 "    \"data\": {\n" +
                 "        \"object\": {\n" +
                 "            \"annvalue\": \"43\",\n" +
                 "            \"autoid\": \"12\",\n" +
                 "            \"badpers\": 67,\n" +
                 "            \"createTime\": 1526619446000,\n" +
                 "            \"day\": \"34\",\n" +
                 "            \"goodpers\": 56,\n" +
                 "            \"id\": 1,\n" +
                 "            \"imageurl\": \"http://xzm1102207843.oss-cn-beijing.aliyuncs.com/tyb/home/live_sub_img2.png\",\n" +
                 "            \"importence\": \"2\",\n" +
                 "            \"name\": \"哈哈\",\n" +
                 "            \"prevalue\": \"31\",\n" +
                 "            \"question\": \"黄金正处”卧薪尝胆“时期 有望触及五年高点\",\n" +
                 "            \"time\": 1525576364000,\n" +
                 "            \"updataTime\": 1526619446000\n" +
                 "        }\n" +
                 "    },\n" +
                 "    \"msg\": \"\",\n" +
                 "    \"status\": 0,\n" +
                 "    \"success\": true\n" +
                 "}";
        btn_invoke_js2.setOnClickListener(view -> {

            webView.loadUrl("javascript:javacalljswith(" + data +")");
//                webView.loadUrl("javascript:javacalljswith("+ data +")");
//                webView.loadUrl("javascript:javacalljswith(\"JAVA调用了JS的有XAS参函数\")");
        });

    }
    @JavascriptInterface
    public void localMethods(String arg) {
        ToastUtil.showToast( "安卓端接收" + arg);
    }


    private void initData() {
        WebSettings settings = webView.getSettings();
// 缓存(cache)
        settings.setAppCacheEnabled(true);      // 默认值 false
        settings.setAppCachePath(getCacheDir().getAbsolutePath());
// 存储(storage)
        settings.setDomStorageEnabled(true);    // 默认值 false
        settings.setDatabaseEnabled(true);      // 默认值 false

// 是否支持viewport属性，默认值 false
// 页面通过`<meta name="viewport" ... />`自适应手机屏幕
        settings.setUseWideViewPort(true);
// 是否使用overview mode加载页面，默认值 false
// 当页面宽度大于WebView宽度时，缩小使页面宽度等于WebView宽度
        settings.setLoadWithOverviewMode(false);
// 是否支持Javascript，默认值false
        settings.setJavaScriptEnabled(true);

        settings.setBlockNetworkImage(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        if (DeviceUtil.isConnectNet(this)) {
            // 根据cache-control决定是否从网络上取数据
            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        } else {
            // 没网，离线加载，优先加载缓存(即使已经过期)
            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        //启用地理定位
        settings.setGeolocationEnabled(true);
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        //设置定位的数据库路径 已废弃Deprecated
//       settings.setGeolocationDatabasePath(dir);  // deprecation
//        settings.setPluginsEnabled(true);
//        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUserAgentString("AndroidProject2");
        webView.addJavascriptInterface(this, "jsCallJava");

        webView.setWebViewClient(new MyBridgeWebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient());
        String URL = Constants.JIANG_SU_WU_XI_LOCAL_SERVER;
        webView.loadUrl(URL);
        Logger.d("=请求地址==" + URL);

        // 获取到UserAgentString
        String userAgent = settings.getUserAgentString();
        // 打印结果
        Logger.d("请求地址User Agent", "User Agent:" + userAgent);
//        checkLocationPermission();
    }




    /**
     * 重写WebViewClient
     */
    private class MyBridgeWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
////            Log.d("webView=" + url);
//            try {
//                url = URLDecoder.decode(url, "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//            if (url.startsWith("yy://return/")) { // 如果是返回数据
////                webView.handlerReturnData(url);
//                return true;
//            } else if (url.startsWith("yy://")) { //
////                webView.flushMessageQueue();
//                return true;
//            } else {
//                return super.shouldOverrideUrlLoading(view, url);
//            }
//        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showLoading();
//            if (NetworkUtils.isConnected()) {
//                showLoading();
//            } else {
//                showNoNetwork();
//            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
//            webView.loadUrl("javascript:" + "window.alert('Js injection success')" );
            showContent();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest resourceRequest, WebResourceError error) {
//            showNoNetwork();
            super.onReceivedError(view, resourceRequest, error);
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            callback.invoke(origin, true, false);
            super.onGeolocationPermissionsShowPrompt(origin, callback);

        }
    }

    @AfterPermissionGranted(LOCATION_PERMISSION)
    private void checkLocationPermission() {
        if (EasyPermissions.hasPermissions(mContext, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            initView();
            initData();
        } else {
            EasyPermissions.requestPermissions(this, "我们需要访问你的定位权限",
                    LOCATION_PERMISSION, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private static final int LOCATION_PERMISSION = 125;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            ToastUtil.showToast("处理成功");
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        } catch (Exception e) {
            Logger.d("权限获取成功onRequestPermissionsResult");
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        ToastUtil.showToast("同意权限");
        Logger.i("相机权限获取成功");
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        ToastUtil.showToast("拒绝权限");
        Logger.i("相机权限获取失败");
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "我们需要打开为您拍照，请允许获取相机权限",
                R.string.setting, R.string.cancel, null, perms);
    }

    @Override
    public void onReLoadDate() {
        checkLocationPermission();
    }


    @Override
    public void onBackPressed() {
        webView.goBack();
//        super.onBackPressed();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            webView.goBack();
        }
        return super.onKeyDown(keyCode, event);
    }
}
