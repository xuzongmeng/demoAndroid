package com.xzm.baselibrary.widget;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.indicators.LineScalePulseOutIndicator;
import com.xzm.baselibrary.R;
import com.xzm.baselibrary.utils.ThemeUtil;
import com.xzm.baselibrary.utils.ToastUtil;


/**
 * Created by xuzongmeng on 2016/10/28.
 */

public class MyContentView {
    private View view;
    public TextView contentTextView;
    public BounceLoadingView bounceloadingview;
    private Button btn_layout_content;
    public MyContentView(Context context) {
      initView(context);
    }

    private void initView(Context context) {

        view = LayoutInflater.from(context).inflate(R.layout.layout_my_contentview, null);
        TextView tv_layout_content = (TextView) view.findViewById(R.id.tv_layout_content);
        AVLoadingIndicatorView Avloading = (AVLoadingIndicatorView) view.findViewById(R.id
                .avloading_content); //lineScalePulseOutRapid
        Avloading.setIndicator(new LineScalePulseOutIndicator());
        Avloading.setIndicatorColor(ThemeUtil.getColor(R.color.Red));
//        Avloading.show();
        btn_layout_content = (Button) view.findViewById(R.id.btn_layout_content);
        bounceloadingview = (BounceLoadingView) view.findViewById(R.id.bounceloadingview);

        btn_layout_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.showToast("响应了自定义界面的事件");
            }
        });
        bounceloadingview.setVisibility(View.VISIBLE);
        btn_layout_content.setVisibility(View.VISIBLE);
        tv_layout_content.setText("这是我的自定义View");
        contentTextView = (TextView) view.findViewById(R.id.tv_layout_content);

        bounceloadingview.addBitmap(R.mipmap.ic_launcher);
        bounceloadingview.addBitmap(R.mipmap.ic_launcher_round);
        bounceloadingview.addBitmap(R.mipmap.ic_launcher);
        bounceloadingview.setShadowColor(Color.LTGRAY);
        bounceloadingview.setDuration(700);
        bounceloadingview.start();
    }
  public  View  getContentView(){
      return  view;
  }


    public  void   setTextContent(String text){


        contentTextView.setText(text);
    }



}
