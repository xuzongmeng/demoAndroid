package com.xzm.baselibrary.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.xzm.baselibrary.R;
import com.xzm.baselibrary.utils.ProcessMonitor;
import com.xzm.baselibrary.widget.LoadingLayout;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by xuzongmeng on 2016/8/30.
 */
public class BaseApplication extends Application {
    public static Context mContext;
    public static Application mApplication;
    private int activityCount;//activity的count数
    private boolean isForeground;//是否在前台

    public static Context getContext() {
        return mContext;
    }
    public static Application getBaseApplication() {
        return mApplication;
    }
    /**
     * 判断当前应该是否从前台切换到后台如果只是在API在14以上
     * ProcessMonitor.attach(this);
     * ProcessMonitor.detach(this);
     */

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mApplication=this;
        init();
        MultiDex.install(this);
//        FormatStrategy formatStrategy = CsvFormatStrategy.newBuilder()
//                .tag("AndroidProject")
//                .build();
//        Logger.addLogAdapter(new DiskLogAdapter(formatStrategy));

//        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
////                .showThreadInfo(false)  // (Optional) Whether to show thread info or not. Default true
////                .methodCount(0)         // (Optional) How many method line to show. Default 2
////                .methodOffset(7)        // (Optional) Hides internal method calls up to offset. Default 5
////                .logStrategy(customLog) // (Optional) Changes the log strategy to print out. Default LogCat
//                .tag("xuzongmeng")   // (Optional) Global tag for every log. Default PRETTY_LOGGER
//                .build();

//        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
        Logger.addLogAdapter(new AndroidLogAdapter(){
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                return true;
            }
        });
//        initShareConfig();
//        initBackground();
//
//        ProcessMonitor.attach(this);
    }

    /**
     * 当终止应用程序对象时调用，不保证一定被调用，
     * 当程序是被内核终止以便为其他应用程序释放资源，那
     * 么将不会提醒，并且不调用应用程序的对象的onTerminate方法而直接终止进程
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    /**
     * 当后台程序已经终止资源还匮乏时会调用这个方法。
     * 好的应用程序一般会在这个方法里面释放一些不必
     * 要的资源来应付当后台程序已经终止，前台应用程序内存还不够时的情况。
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();

        ProcessMonitor.detach(this);
    }

    /**
     * 配置改变时触发这个方法
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void initBackground() {

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(Activity activity) {
                activityCount++;
            }

            @Override
            public void onActivityResumed(Activity activity) {
            }

            @Override
            public void onActivityPaused(Activity activity) {
            }

            @Override
            public void onActivityStopped(Activity activity) {
                activityCount--;
                if (0 == activityCount) {
                    isForeground = false;
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
            }
        });


        Log.d("BaseApplication","基类全局Activity个数=" + activityCount);
    }

    private void init() {
//        LoadingLayout.getConfig()
//                .setLoadingPageLayout(R.layout.load_more_footer);

//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
////                .addInterceptor(new LoggerInterceptor("TAG"))
//                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
//                .readTimeout(10000L, TimeUnit.MILLISECONDS)
//                //其他配置
//                .build();
//        LoadingLayout.getConfig()
//                .setErrorText("出错啦~请稍后重试！")
//                .setEmptyText("抱歉，暂无数据")
//                .setNoNetworkText("无网络连接，请检查您的网络···")
//                .setErrorImage(R.mipmap.ic_launcher)
//                .setEmptyImage(R.mipmap.ic_launcher)
//                .setNoNetworkImage(R.mipmap.ic_launcher)
//                .setAllTipTextColor(R.color.gray)
//                .setAllTipTextSize(14)
//                .setReloadButtonText("点我重试哦")
//                .setReloadButtonTextSize(14)
//                .setReloadButtonTextColor(R.color.gray)
//                .setReloadButtonWidthAndHeight(150, 50);


/**
 * 由于“加载中”的页面，可能每个App都不一样，因此，LoadingLayout支持自定义LoadingPage，如下：
 */
        LoadingLayout.getConfig()
                .setLoadingPageLayout(R.layout.widget_loading_page);

    }

    // 保存各个activity实例，在退出应用时，全部finish()掉
    private List<Activity> activities = new ArrayList<Activity>();

    /**
     * 添加Activity
     */
    public void addActivity(Activity activity) {
        if (activity != null) {
            activities.add(activity);
        }
    }

    /**
     * 关闭Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activities.remove(activity);
            activity.finish();
        }
    }
    /**
     * 退出程序
     */
    public void exit() {
        //友盟统计退出程序时关闭统计
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);

        for (Activity activity : activities) {
            if (activity != null) {
                activity.finish();
            }
        }
    }
}
