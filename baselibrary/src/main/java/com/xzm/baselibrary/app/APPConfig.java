package com.xzm.baselibrary.app;

import android.os.Environment;

import com.xzm.baselibrary.BuildConfig;

/**
 * Created by cm on 2016/6/7.
 */
public class APPConfig {
    /***
     * 获取文件的相对路径awdaw
     */

    public  static  boolean  DEBUG= BuildConfig.DEBUG;
    public static String getFilePathInSDCard(String dir, String fileName) {
        return Environment.getExternalStorageDirectory() + "/" + dir + java.io.File.separator + fileName;
    }

    public static class Network {
        public static String WU_YU_BASE_URL = "http://test..com//";
        public static String XUZONGMENG_BASE_URL = "http://www..top:/";
    }

}
