package com.xzm.baselibrary.constants;

/**
 * Created by xuzongmeng on 2017/12/24.
 */
//@Data
public class Constants {
    public static final String baseUrl = "http://192.168.6.135";
    public static final String openId = "05e1164a151d674c80654c422e60cf73c6a19d1e422d7226";
    public static final String cityCode = "131";
    public static final String longitude = "116.39";
    public static final String latitude = "39";
    public static final String seqno = "YH180503171434056752";
    public static final String appid = "1C5A8NARI04K0101007F0000EA0B506B";
    public static final String signCode = "772f08f7261c4eb36ec30ab6f201de76d0ce7ef43847087de6550e8311a64be7";


    /**
     * 江苏农行
     */
    //public static final String JIANG_SU_LOCAL_SERVER = "http://192.168.6.124:8082/#/filmList";
//    public static final String JIANG_SU_WU_XI_LOCAL_SERVER = "http://192.168.6.117:8085/?fromType=wuxi#/Home";
    public static final String JIANG_SU_WU_XI_LOCAL_SERVER = "http://192.168.6.135:8085/?fromType=wuxi#/Home";
    public static final String JIANG_SU_LOCAL_SERVER_ = "http://192.168.200.2:8085/?openid=oxmudxLXzqKWTSnZ8ikaJscicl-Y&signcode=23b4044b11af85609d76089aea44a924#/Home";
    public static final String JIANG_SU_USER_INFO_PRO_SERVER_ = "https://.com/iabcjiangsufilm/?openid=oxmudxLXzqKWTSnZ8ikaJscicl-Y&signcode=23b4044b11af85609d76089aea44a924#/mine"; //江苏农行

    /**
     * 农行总行
     */
    public static final String ABC_TEST_SERVER = "http://10.1.1.58:4888/iabcfilm/#/"; //农行总行cityList
    public static final String ABC_LOCAL_SERVER = "http://192.168.200.2:8082/iabcfilm/#/"; //农行总行本地测试cityList

    /*农行总行正确拼接 //农行总行本地测试cityList*/
    public static final String ABC_USER_INFO_LOCAL_SERVER="http://192.168.200.2:8082/iabcfilm/?" +
            "openId="+Constants.openId + "&cityCode="+Constants.cityCode+"&longitude="+Constants.longitude +
            "&latitude="+Constants.latitude + "&seqno="+Constants.seqno +"&appid="+Constants.appid +"&signCode="+Constants.signCode+"#/MinePage";

    /*农行总行错误拼接*/
//    public static final String mURL1="http://192.168.200.2:8082/iabcfilm/?" +
//            "openId="+Constants.openId + "&cityCode="+Constants.cityCode+"&longitude="+Constants.longitude +
//            "&latitude="+Constants.latitude + "&seqno="+Constants.seqno +"&appid="+Constants.appid +"&signCode="+Constants.signCode +
//              "#/MinePage?";   //农行总行本地测试cityList


    public static final String DOU_PIAO_PRO_SERVER = "https://hxb.com/ihxbfilm/";
    public static final String BEI_JING__PRO_SERVER = "https://bob.com/ibeijingbankfilm/#/"; //北京银行


    /**
     * 百度地图
     */
    public static final String BAI_DU_SERVER = "https://map.baidu.com/mobile/webapp/index/index/"; //百度地图样列
    private static String BAI_DU_MY_SERVER = "http://www.smallxzm.top/xzm/baidu.html";
    public static final String BAI_DU = "https://map.baidu.com/mobile/webapp/index/index/foo=bar/vt=map&universalLinkCbkTag=1";//百度地图

    /**
     * let openId = null;
     * let cityCode = null;
     * let longitude = null;
     * let latitude = null;
     * let seqno = null;
     * let appid = null;
     * let signCode = null;-->
     */

    /**
     * 家ip
     */
//    public static final String TEST_JS_BRIDGE = "http://192.168.1.7:8083/test/index/jsBridge";
//    public static final String TEST_JS_BRIDGE_VUE = "http://192.168.1.7:8086";

    /**
     * 公ip
     */

    public static final String TEST_JS_BRIDGE = "http://192.168.6.124:8083/test/index/jsBridge";
    public static final String TEST_JS_BRIDGE_VUE = "http://192.168.6.135:8086";
//    public static final String TEST_JS_BRIDGE_VUE = "http://127.0.0.1:8086/#/filmList";
//    public static final String TEST_JS_BRIDGE_VUE = "http://172.29.133.1:8086/#/filmList";

    // http://192.168.6.124:8086

    /**
     *  建设银行
     */

    public static final String TEST_ICCB2 = "https://local-ccb..com/i-ccb-film/?inType=SH#/";
    public static final String TEST_ICCB = "https://ccb..com/i-ccb-film";
    public static final String test_mobike = "https://h5.mobike.com";
    public static final String ICCB = "https://.com/i-ccb-film/#/Home";
    public static final String TEST_ICCBXXX = "http://192.168.56.1:8085/#/discovery";
//    public static final String TEST_ICCB = "http://192.168.6.124:8086/#/filmList";
}
