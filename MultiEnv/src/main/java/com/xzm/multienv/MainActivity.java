package com.xzm.multienv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView1 = findViewById(R.id.textView1);
        TextView textView2 = findViewById(R.id.textView2);
        TextView textView3 = findViewById(R.id.textView3);
        TextView textView4 = findViewById(R.id.textView4);
        TextView textView5 = findViewById(R.id.textView5);
        textView1.setText("日志显示开关=" + BuildConfig.LOG_DEBUG);
        textView2.setText("环境" + BuildConfig.DEBUG);
        textView3.setText("酒店机票服务" + BuildConfig.DEBUG);
        textView4.setText("商品服务" + BuildConfig.DEBUG);
        textView5.setText("socket服务" + BuildConfig.SOCKET_SERVER_ADDRESS + BuildConfig.SOCKET_SERVER_PORT);

//        BuildConfig.DEBUG
    }
}
