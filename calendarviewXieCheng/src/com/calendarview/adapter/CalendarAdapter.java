package com.calendarview.adapter;

import java.util.List;
 




import com.calendarview.util.DateInfo;
import com.calendarview.util.DateInfo.Days;
import com.calendarview.util.MyGridView;
import com.demo.calendarview.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 *  日历适配器
 *
 */
public class CalendarAdapter extends BaseAdapter{
	
	private Context context;
	private List<DateInfo> listDate;
	private OnDaySelectListener callBack;// 回调函数
	
	public CalendarAdapter(Context context,List<DateInfo> listDate){
		this.context=context;
		this.listDate=listDate;
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listDate.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listDate.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder vh = null;
		if (view == null) {
			vh = new ViewHolder();
			view = LayoutInflater.from(context).inflate(R.layout.item_calendar, null);// 获取布局，开始初始化
			vh.tv_yyyyMonth = (TextView) view.findViewById(R.id.tv_yyyyMonth); 
			vh.gv_calendar = (MyGridView) view.findViewById(R.id.gv_calendar);
			view.setTag(vh);
		} else {
			vh = (ViewHolder) view.getTag();
		}
		
		DateInfo date= listDate.get(position);
		vh.tv_yyyyMonth.setText(date.getMonth());
		List<Days> listDay=date.getListDay();
		GridView2Adapter gvAdapter=new GridView2Adapter(context,listDay );
		vh.gv_calendar.setAdapter(gvAdapter);
		vh.gv_calendar.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				Days days= (Days) adapterView.getAdapter().getItem(position);
				
				String day = days.getDay();
				if (!"".equals(day)) { 
					if (callBack != null) {// 调用回调函数回调数据
						callBack.onDaySelectListener(arg1, days);
					}
				}
			}
		});
		
		
		return view;
	}
	
	
	static class ViewHolder {
		TextView tv_yyyyMonth;
		MyGridView gv_calendar;
	}

	/**
	 * 自定义监听接口
	 * 
	 * @author Administrator
	 * 
	 */
	public interface OnDaySelectListener {
		void onDaySelectListener(View view, Days days);
	}

	/**
	 * 自定义监听接口设置对象
	 * 
	 * @param o
	 */
	public void setOnDaySelectListener(OnDaySelectListener o) {
		callBack = o;
	}

}
