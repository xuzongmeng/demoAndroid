package com.calendarview.adapter;

import java.text.ParseException;
import java.util.List;

import com.calendarview.util.DateInfo.Days;
import com.demo.calendarview.R;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class GridView2Adapter extends BaseAdapter {

	private Context context;
	private List<Days> listDay;
	public static View viewIn;
	public static View viewOut;

	public GridView2Adapter(Context context, List<Days> listDay) {
		this.context = context;
		this.listDay = listDay;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listDay.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listDay.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		GrideViewHolder holder;
		if (convertView == null) {
			holder = new GrideViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.item_gridview, null);
			holder.tvNL = (TextView) convertView.findViewById(R.id.tv_calendar);
			holder.tvDay = (TextView) convertView.findViewById(R.id.tv_calendar_day);
			convertView.setTag(holder);
		} else {
			holder = (GrideViewHolder) convertView.getTag();
		}
		Days day = listDay.get(position);

		// 公历
		if (!day.getDay().equals("")) {
			holder.tvDay.setText(day.getDisplayDay());
			holder.tvDay.setTextColor(day.getDefTextColor());
			holder.tvDay.setTextSize(TypedValue.COMPLEX_UNIT_PX,day.getDefTextSize());
			holder.tvDay.setTag(day);
			// 农历
			holder.tvNL.setText(day.getDisplayNongliDate());
			holder.tvNL.setTextColor(day.getDefNongTextColor());
			holder.tvNL.setTextSize(TypedValue.COMPLEX_UNIT_PX,day.getDefNongTextSize());
		}
		// 今天
		if (day.isCurDay()) {
			//holder.tvNL.setVisibility(View.GONE);
		}

		// 显示选中信息
		if (day.isBeginDay()) {
			// convertView.setBackgroundColor(Color.parseColor("#33B5E5"));
//			convertView.setBackgroundResource(R.drawable.shape_riliselectbg);
//			holder.tvDay.setTextColor(Color.WHITE);
//			holder.tvDay.setText(day.getDisplayDay());
//			holder.tvNL.setTextColor(Color.WHITE);
//			holder.tvNL.setText("入住");
//			viewIn = convertView;
			selectView(convertView,1);
		}
		if (day.isEndDay()) {
//			convertView.setBackgroundResource(R.drawable.shape_riliselectbg);
//			holder.tvDay.setTextColor(Color.WHITE);
//			holder.tvNL.setTextColor(Color.WHITE);
//			holder.tvNL.setText("离开");
//			viewOut = convertView;
			selectView(convertView,2);
		}

		try {
			// 若日历日期<当前日期，则不能选择
			if (day.getDisplayDay().equals("") || day.getDisplayDay().length() < 1) {
				holder.tvDay.setTextColor(Color.GRAY);
			}
			// 若日历日期-当前日期>90天，则不能选择
			// long
			// dayxc=(dateFormat2.parse(date[0]+"-"+day).getTime()-dateFormat2.parse(nowday).getTime())/nd;
			// if(dayxc>90){
			// holder.tvDay.setTextColor(Color.parseColor("#999999"));
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}
	
	/**
	 * 返回初始状态  
	 * @param convertView
	 * @param typeId  1-in,2-out
	 */
	public static void backInit(View convertView,int typeId){
		GrideViewHolder holder = (GrideViewHolder) convertView.getTag();
		Days days=(Days)holder.tvDay.getTag();
		convertView.setBackgroundColor(Color.WHITE); 
		holder.tvDay.setTextColor(days.getDefTextColor());
		holder.tvNL.setTextColor(days.getDefNongTextColor());
		holder.tvNL.setText(days.getDisplayNongliDate());
	   if(typeId==1){
		   days.setBeginDay(false);
	   }else{
		   days.setEndDay(false);
	   }
	}
	
	/**
	 * 设置选中
	 * @param convertView
	 * @param typeId 1-in,2-out
	 */
	public static void selectView(View convertView,int typeId){
		GrideViewHolder holder = (GrideViewHolder) convertView.getTag();
		Days days=(Days)holder.tvDay.getTag();
		convertView.setBackgroundResource(R.drawable.shape_riliselectbg);
		holder.tvDay.setTextColor(Color.WHITE);
		holder.tvDay.setText(days.getDisplayDay());
		holder.tvNL.setTextColor(Color.WHITE);
		if(typeId==1){
			holder.tvNL.setText("入住");
			viewIn = convertView;
			days.setBeginDay(true);
		}else{
			holder.tvNL.setText("离开");
			viewOut = convertView;
			days.setEndDay(true);
		}
		
		
	}
	

	static class GrideViewHolder {
		TextView tvDay, tvNL;
	}

}
