package com.calendarview.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.calendarview.util.DateInfo.Days;

public class DateUtil {
	
	/** yyyy-MM-dd HH:mm:ss */
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/** yyyy-MM-dd */
	private static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	
	
	/** yyyy */
	private static SimpleDateFormat sd1 = new SimpleDateFormat("yyyy");
	/** MM */
	private static SimpleDateFormat sd2 = new SimpleDateFormat("MM");
	/** dd */
	private static SimpleDateFormat sd3 = new SimpleDateFormat("dd");
	
	/** yyyy-MM */
	private static SimpleDateFormat sd4 = new SimpleDateFormat("yyyy-MM");
	
	/**
	 * yyyy-MM-dd HH:mm:ss 
	 * @param str
	 * @return
	 */
	public static String timestampToString(long str) {
		return sdf.format(new Date(str));
	}
	
	/**
	 * yyyy-MM-dd HH:mm:ss 
	 * @return
	 */
	public static String getCurDateTime() {
		String time = "";
		time = timestampToString(System.currentTimeMillis());
		return time;
	}
	
	
	/**
	 * yyyy-MM-dd 

	 * @return
	 */
	public static String getCurDate() {
		String time = "";
		time = sdf2.format(new Date(System.currentTimeMillis())); 
		return time;
	}
	 
	
	/**
	 * 格式化Date
	 * 
	 * @param date
	 * @return yyyy-MM-dd
	 */
	public static Date formatDate2(String date) {
		Date tdate=null;
		try {
			tdate = sdf2.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tdate;
	}
	
	/**
	 * 在当前时间上添加多少年、月、日
	 * 
	 * @param date
	 *            yyyy-MM-dd
	 * @param type
	 *            Calendar.YEAR 、 Calendar.MONTH 、 Calendar.DATE
	 * @param addNum
	 * @return yyyy-MM-dd
	 */
	public static String addDate(String date, int addType, int addNum) {
		String time = "";
		try {
			Date temp_date = sdf2.parse(date);
			Date temp_date2 = addDate(temp_date, addType, addNum);
			time = sdf2.format(temp_date2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}

	/**
	 * 在当前时间上添加多少年、月、日
	 * 
	 * @param date
	 *            yyyy-MM-dd
	 * @param type
	 *            Calendar.YEAR 、 Calendar.MONTH 、 Calendar.DATE
	 * @param addNum
	 * @return yyyy-MM-dd
	 */
	public static Date addDate(Date date, int addType, int addNum) {
		Date tempDate = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(addType, addNum);
			tempDate = cal.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tempDate;
	}
	
	/**
	 * 获取年yyyy
	 * @param date
	 * @return
	 */
	public static String formatYear(Date date) {
		String yyyy = sd1.format(date);
		return yyyy;
	}

	/**
	 * 获取月MM
	 * @param date
	 * @return
	 */
	public static String formatMonth(Date date) {
		String mm = sd2.format(date);
		return mm;
	}

	/**
	 * 获取日day
	 * @param date
	 * @return
	 */
	public static String formatDay(Date date) {
		String dd = sd3.format(date);
		return dd;
	}
	
	/**
	 * 获取年月YYYY-MM
	 * @param date
	 * @return
	 */
	public static String formatYearMonth(Date date) {
		String dd = sd4.format(date);
		return dd;
	}
	
	/**
	 * 获取对应的农历(如果是阳历或农历节日就优先显示节日)
	 * @param curDate 当前日期 yyyy-MM-dd
	 * @return
	 */
	public static Days getLunar(String curDate){
		Days info=new Days(); 
		Date date=formatDate2(curDate);
		Lunar lunar=new Lunar(date);
		info.setNongliDate(lunar.getLunarDayString());
		if (lunar.isSFestival()) {
			//阳历节日
			info.setDisplayNongliDate(lunar.getSFestivalName());
			info.setHoliday(true);
		} else {
			if (lunar.isLFestival() && lunar.getLunarMonthString().substring(0, 1).equals("闰") == false) {
				//农历节日
				info.setDisplayNongliDate(lunar.getLFestivalName());
				info.setHoliday(true);
			} else {
				String solarTerm=lunar.getTermString();
				if(!solarTerm.equals("")&&solarTerm.equals("清明")){
					//24节气
					info.setDisplayNongliDate("清明");
					info.setHoliday(true);
				}else{
					if (lunar.getLunarDayString().equals("初一")) {
						info.setDisplayNongliDate(lunar.getLunarMonthString() + "月");
					} else {
						info.setDisplayNongliDate(lunar.getLunarDayString());
					}
					info.setHoliday(false);
				}
				
			}
		}
		return info;
	}

}
