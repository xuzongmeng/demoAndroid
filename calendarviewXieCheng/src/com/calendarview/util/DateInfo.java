package com.calendarview.util;

import java.util.List;

/**
 *  日历信息
 *
 */
public class DateInfo {
	
	private String month;//当前月份 yyyy-MM
	private List<Days> listDay; //当前月份日期
	
	
	
	public String getMonth() {
		return month;
	}
 
	public void setMonth(String month) {
		this.month = month;
	}

	public List<Days> getListDay() {
		return listDay;
	}
 
	public void setListDay(List<Days> listDay) {
		this.listDay = listDay;
	}




	public static class Days{
		public Days(){}
		
		private boolean isBeginDay; //是否为入住日期
		private boolean isEndDay;  //是否为离开日期
		private boolean isCurDay;//是否为今天
		private String day; //当天日期
		private String displayDay; //显示日期
		private int defTextColor;//阳历文字默认显示颜色
		private int bgColor;//显示背景颜色
		private float defTextSize;//默认字体大小 
		
		private boolean isHoliday;//是否为节日
		private  String nongliDate; //当天农历
		private String displayNongliDate;//当天显示农历
	    private int defNongTextColor;  //农历字体默认颜色
	    private float defNongTextSize;  //农历字体默认大小 
	     
		
		public String getDay() {
			return day;
		}
		public void setDay(String day) {
			this.day = day;
		}
		public String getDisplayDay() {
			return displayDay;
		}
		public void setDisplayDay(String displayDay) {
			this.displayDay = displayDay;
		}
	  
		public boolean isHoliday() {
			return isHoliday;
		}
		public void setHoliday(boolean isHoliday) {
			this.isHoliday = isHoliday;
		}
		public String getNongliDate() {
			return nongliDate;
		}
		public void setNongliDate(String nongliDate) {
			this.nongliDate = nongliDate;
		}
		public String getDisplayNongliDate() {
			return displayNongliDate;
		}
		public void setDisplayNongliDate(String displayNongliDate) {
			this.displayNongliDate = displayNongliDate;
		}
		public int getDefNongTextColor() {
			return defNongTextColor;
		}
		public void setDefNongTextColor(int defNongTextColor) {
			this.defNongTextColor = defNongTextColor;
		}
		public int getDefTextColor() {
			return defTextColor;
		}
		public void setDefTextColor(int defTextColor) {
			this.defTextColor = defTextColor;
		}
	 
	 
		public boolean isCurDay() {
			return isCurDay;
		}
		public void setCurDay(boolean isCurDay) {
			this.isCurDay = isCurDay;
		}
		public int getBgColor() {
			return bgColor;
		}
		public void setBgColor(int bgColor) {
			this.bgColor = bgColor;
		}
		 
		public float getDefTextSize() {
			return defTextSize;
		}
		public void setDefTextSize(float defTextSize) {
			this.defTextSize = defTextSize;
		}
		 
		public float getDefNongTextSize() {
			return defNongTextSize;
		}
		public void setDefNongTextSize(float defNongTextSize) {
			this.defNongTextSize = defNongTextSize;
		}
		public boolean isBeginDay() {
			return isBeginDay;
		}
		public void setBeginDay(boolean isBeginDay) {
			this.isBeginDay = isBeginDay;
		}
		public boolean isEndDay() {
			return isEndDay;
		}
		public void setEndDay(boolean isEndDay) {
			this.isEndDay = isEndDay;
		} 
	    
	}

}
