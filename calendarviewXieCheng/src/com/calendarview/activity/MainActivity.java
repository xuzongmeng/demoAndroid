package com.calendarview.activity;

  
import java.util.Calendar;

import com.calendarview.util.DateUtil;
import com.demo.calendarview.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle; 
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity{
	
	TextView tvBenginTime,tvEndTime;
	Button btnDouble,btnSinle;
	private int requestCode=100;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_main);
		
		tvBenginTime=(TextView)this.findViewById(R.id.tvBenginTime);
		tvEndTime=(TextView)this.findViewById(R.id.tvEndTime);
		btnDouble=(Button)this.findViewById(R.id.btnDouble);
		btnSinle=(Button)this.findViewById(R.id.btnSinle);
		
		String begDate=DateUtil.getCurDate();
		String endDate=DateUtil.addDate(begDate, Calendar.DATE, 1);
		tvBenginTime.setText(begDate);
		tvEndTime.setText(endDate);
		
		btnDouble.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent intent=new Intent(MainActivity.this,CalendarActivity.class);
				 intent.putExtra("begDate", tvBenginTime.getText());
				 intent.putExtra("endDate", tvEndTime.getText());
				 intent.putExtra("isSelectDouble", true);
				 startActivityForResult(intent, requestCode);
				
			}
		});
		
		btnSinle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent intent=new Intent(MainActivity.this,CalendarActivity.class);
				 intent.putExtra("begDate", tvBenginTime.getText()); 
				 startActivityForResult(intent, requestCode);
				
			}
		});
		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(this.requestCode==requestCode&&resultCode==200){
			String begDate=data.getStringExtra("begDate");
			String endDate=data.getStringExtra("endDate");
			tvBenginTime.setText(begDate);
			tvEndTime.setText(endDate);
		}
	}

}
