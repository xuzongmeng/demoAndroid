package com.calendarview.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.calendarview.adapter.CalendarAdapter;
import com.calendarview.adapter.CalendarAdapter.OnDaySelectListener;
import com.calendarview.adapter.GridView2Adapter;
import com.calendarview.util.DateInfo;
import com.calendarview.util.DateInfo.Days;
import com.calendarview.util.DateUtil;
import com.demo.calendarview.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color; 
import android.os.Bundle;
import android.view.View;
import android.widget.ListView; 

public class CalendarActivity extends Activity implements OnDaySelectListener {

	private ListView lvdate;
	private CalendarAdapter adapter;
	private List<DateInfo> listDate;
	private String begDate, endDate, s_begDate, s_endDate;
	private Context context;
	/** 是否双日期 */
	private boolean isSelectDouble = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_calendar);

		context = this;
		Intent intent = this.getIntent();
		begDate = intent.getStringExtra("begDate");
		if (intent.hasExtra("isSelectDouble")) {
			isSelectDouble = intent.getBooleanExtra("isSelectDouble", false);
		}
		if (isSelectDouble) {
			endDate = intent.getStringExtra("endDate");
		}
		lvdate = (ListView) this.findViewById(R.id.lvdate);

		listDate = new ArrayList<DateInfo>();
		adapter = new CalendarAdapter(this, listDate);
		lvdate.setAdapter(adapter);

		adapter.setOnDaySelectListener(this);
		initDate();

	}

	@Override
	public void onDaySelectListener(View view, Days days) {

		// 若日历日期小于当前日期，或日历日期-当前日期超过三个月，则不能点击
		try {
			if (DateUtil.formatDate2(days.getDay()).getTime() < DateUtil.formatDate2(DateUtil.getCurDate()).getTime()) {
				return;
			}
			// long dayxc = (DateUtil.formatDate2(date).getTime() -
			// DateUtil.formatDate2(nowday).getTime()) / nd;
			// if (dayxc > 90) {
			// return;
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 若以前已经选择了日期，则在进入日历后会显示以选择的日期，该部分作用则是重新点击日历时，清空以前选择的数据（包括背景图案）
		if (!"".equals(begDate) && GridView2Adapter.viewIn != null) {
			GridView2Adapter.backInit(GridView2Adapter.viewIn, 1);
		}
		if (!"".equals(endDate) && GridView2Adapter.viewOut != null) {
			GridView2Adapter.backInit(GridView2Adapter.viewOut, 2);
		}

		//
		if (null == s_begDate || s_begDate.equals("")) {
			s_begDate = days.getDay();
			GridView2Adapter.selectView(view, 1);
			if(!isSelectDouble){
				Intent intent = new Intent();
				intent.putExtra("begDate", s_begDate); 
				setResult(200, intent);
				finish();
			}
			return;
		} else {
			s_endDate = days.getDay();
			if (s_begDate.equals(s_endDate)) {
				return;
			} else {
				if (DateUtil.formatDate2(s_endDate).getTime() < DateUtil.formatDate2(s_begDate).getTime()) {
					GridView2Adapter.selectView(view, 1);
					s_begDate = days.getDay();
					return;
				}
			}

		}

		GridView2Adapter.selectView(view, 2);

		//
		Intent intent = new Intent();
		intent.putExtra("begDate", s_begDate);
		intent.putExtra("endDate", s_endDate);
		setResult(200, intent);
		finish();

	}

	/**
	 * 初始化日期
	 */
	private void initDate() {

		List<DateInfo> list = new ArrayList<DateInfo>();
		Date date = new Date();
		for (int i = 0; i <= 12; i++) {
			DateInfo dateInfo = new DateInfo();
			Date curDate = DateUtil.addDate(date, Calendar.MONTH, i);
			String yyyy = DateUtil.formatYear(curDate);
			String mm = DateUtil.formatMonth(curDate);
			// String dd = DateUtil.formatDay(curDate);
			// list.add(yyyy + "-" + mm + "-" + dd);
			String strMonth = yyyy + "年" + mm + "月";
			dateInfo.setMonth(strMonth);
			List<Days> listDay = setCurMonthDay(curDate);
			Days firstDay = listDay.get(0);// 如果第一天是除夕，那么上个月最后一天就是
			if (i != 0 && firstDay.getDisplayNongliDate().equals("春节")) {
				DateInfo lastMonth = list.get(i - 1);// 上个月
				Days lastDay = lastMonth.getListDay().get(lastMonth.getListDay().size() - 1);
				lastDay.setDisplayNongliDate("除夕");
				lastDay.setDefNongTextColor(context.getResources().getColor(R.color.ri_dhuang));
			}
			dateInfo.setListDay(listDay);
			list.add(dateInfo);

		}

		listDate.clear();
		listDate.addAll(list);
		adapter.notifyDataSetChanged();

	}

	/**
	 * 计算出当月的每天信息
	 * 
	 * @param date
	 *            当月
	 * @return
	 */
	private List<Days> setCurMonthDay(Date date) {
		List<Days> listDay = new ArrayList<DateInfo.Days>();
		String yyyyMM = DateUtil.formatYearMonth(date);
		// 计算当前月的日期
		Calendar cal = Calendar.getInstance();// 获取日历实例
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);// cal设置当前day为当前月第一天
		int tempSum = cal.get(Calendar.DAY_OF_WEEK) - 1;// 获取当前月第一天为星期几
		int dayNumInMonth = cal.getActualMaximum(Calendar.DATE);// 获取当前月有多少天

		for (int i = 0; i < tempSum; i++) {
			Days days = new Days();
			days.setDay("");
			days.setDisplayDay("");
			days.setNongliDate("");
			days.setDisplayNongliDate("");
			listDay.add(days);
		}

		for (int j = 1; j <= dayNumInMonth; j++) {

			String strDay = "" + j;
			if (j < 10) {
				strDay = "0" + j;
			}
			String curDay = yyyyMM + "-" + strDay;

			// 农历
			Days day = DateUtil.getLunar(curDay);
			day.setDefNongTextColor(context.getResources().getColor(R.color.ri_lhui));
			if (day.isHoliday()) {
				day.setDefNongTextColor(context.getResources().getColor(R.color.ri_dhuang));
			}
			day.setDefNongTextSize(context.getResources().getDimension(R.dimen.sp_12));

			// 如果是初一，则计算前一天是否为除夕
			if (day.getDisplayNongliDate().equals("春节")) {
				if (listDay.size() > 0) {
					Days beforeDay = listDay.get(listDay.size() - 1);// 前一天
					if (beforeDay.getDay().length() > 0) {
						beforeDay.setDisplayNongliDate("除夕");
						beforeDay.setDefNongTextColor(context.getResources().getColor(R.color.ri_dhuang));
					}
				}
			}

			//day.setDisNongTextColor(day.getDefTextColor());

			// 公历
			day.setDefTextColor(context.getResources().getColor(R.color.ri_mlv));
			// 周末字体颜色
			if ((j + tempSum) % 7 == 0 || (j + tempSum - 1) % 7 == 0) {
				day.setDefTextColor(context.getResources().getColor(R.color.ri_dhong));
			}
			day.setDefTextSize(context.getResources().getDimension(R.dimen.sp_13));
			if (DateUtil.getCurDate().equals(curDay)) {
				strDay = "今天";
				day.setCurDay(true);
			}
			// 不能和今天混到一起，不然逻辑有冲突
			if (curDay.equals(begDate)) {
				day.setBeginDay(true);
			} else if (curDay.equals(endDate)) {
				day.setEndDay(true);
			}
			day.setDay(curDay);
			day.setDisplayDay(strDay);
			day.setBgColor(Color.WHITE);

		 

			listDay.add(day);
		}

		return listDay;

	}

}
