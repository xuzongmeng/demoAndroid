## 主要功能
- **首页**：各种demo集合
- **关于我们**
## 项目地址
 - [demoAndroid](https://github.com/xzm1102207843/demoAndroid)
## 项目截图
![](AndroidProjectDemo/screenshot/QQ截图20190416175446.png)
![](http://pic1.nipic.com/2008-08-14/2008814183939909_2.jpg)

## 主要开源框架
 - [RxJava](https://github.com/ReactiveX/RxJava)
 - [RxAndroid](https://github.com/ReactiveX/RxAndroid)
 - [RxKotlin](https://github.com/ReactiveX/RxKotlin)
 - [Retrofit](https://github.com/square/retrofit)
 - [okhttp](https://github.com/square/okhttp)
 - [Glide](https://github.com/bumptech/glide)
 - [Anko](https://github.com/Kotlin/anko)