package com.example.goldatom_.citychooseslide;

import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
//    public static LocationUtils mLocationUtils = null;

    private Button btn_choosecity;
    private static final int REQUEST_CODE_CITY = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn_choosecity = findViewById(R.id.btn_choosecity);
        btn_choosecity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddressActivity.class);
                startActivityForResult(intent,REQUEST_CODE_CITY);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_CITY){
            CityBean cityBean = (CityBean) data.getSerializableExtra("city");
            Log.d("====","拿到===" + cityBean.getId());
            Toast.makeText(this, "拿到===" + cityBean.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}