/**
 * Created by IDontKnowYourName on 2015/5/18.
 */
$(function () {

    // var fy = $('.frame').offset().top;
    var fy = $(".frame").offset().top;
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= fy) {
            if ($.browser.msie && $.browser.version == '6.0') $('.frame').css({
                top: ($(window).scrollTop() - fy) + 'px',
                position: 'absolute'
            });
            else $('.frame').css({top: '0px', position: 'fixed'});
        }
        else $('.frame').css({top: '0px', position: 'absolute'});
    }).scroll();
    //end
});