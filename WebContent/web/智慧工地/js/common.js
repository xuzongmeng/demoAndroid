/** 
 * 通用类
 * @author  zhouhl
 * @version 1.0
 */

(function($) {
	var Common = $.Common = $.Class.extend({
		/**
		 * 页面跳转
		 * Create By：   zhouhl          
		 * Create Date：2017-7-10
		 * @param url 要跳转的地址
		 * @param id 要跳转的id
		 *                       
		 */
		eachClick: function(clickType) {
			var aniShow = "fade-in";
			//只有ios支持的功能需要在Android平台隐藏；
			if(mui.os.android) {
				var list = document.querySelectorAll('.ios-only');
				if(list) {
					for(var i = 0; i < list.length; i++) {
						list[i].style.display = 'none';
					}
				}
				//Android平台暂时使用slide-in-right动画
				if(parseFloat(mui.os.version) < 4.4) {
					aniShow = "fade-in";
				}
			}
			var feedbackWebview = plus.webview.create(clickType.url, clickType.id);
			feedbackWebview.addEventListener('titleUpdate', function() {
				feedbackWebview.show(aniShow, 300);
			});

			/*var webview = plus.webview.create(clickType.url);
			webview.show(aniShow, 300);*/
		},
		/**
		 * 将json转换成字符串连接形式  
		 * Create By：   zhouhl          
		 * Create Date：2017-6-22 
		 * @param strJson  相关json
		 * @example 
		 * in: {id:13,name:helen}
		 * output:id=13&name=helen
		 */
		convertJsonToString: function(strJson) {
			var returnValue = "";
			var i = 0;
			for(var key in strJson) {
				if(i == 0) {
					returnValue += key + "=" + strJson[key];
				} else {
					returnValue += "&" + key + "=" + strJson[key];
				}
				i++;
			}

			return returnValue;
		},

		/**
		      * 取得地址栏中相关的参数
		      * Create By：   zhouhl          
		      * Create Date：2017-6-22
		      * @param param 参数名称
		       
		   */

		getQueryString: function(param) {
			var name, value;
			var str = location.href; //取得整个地址栏
			var num = str.indexOf("?")
			str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]

			var arr = str.split("&"); //各个参数放到数组里
			for(var i = 0; i < arr.length; i++) {
				num = arr[i].indexOf("=");
				if(num > 0) {
					name = arr[i].substring(0, num);
					value = arr[i].substr(num + 1);
					this[name] = value;
					if(name == param) {
						return decodeURI(value);
					}
				}
			}
		},

		/**
		 * 显示等待域
		 * Create By：   zhouhl          
		 * Create Date：2015-09-05
		 * @param title        等待的标题
		 * @param loadClass    等待的class
		 */
		waiting: function(opts) {
			var self = this;
			var default_opts = {
				loadClass: "mui-loader-show gray",
				title: "正在加载页面，请稍候..."
			}

			var optsx = self.extend(
				self.extend(
					optsx || {}, default_opts
				),
				opts);

			var waitShow = '<div class="' + optsx.loadClass + '"><div class="mui-loading"  > <div class="mui-spinner"></div></div><br>' + optsx.title + '</div>';
			//var waitShow='<div class="'+optsx.loadClass+'"><div class="mui-loading"  ><img src = "../images/loading.jpg" width="12"  height="12"></div><br>'+optsx.title+'</div>';

			return waitShow;
		},

		/**
		 * 没有数据的显示域
		 * Create By：   zhouhl          
		 * Create Date：2015-09-05
		 * @param title        显示的内容
		 * @param loadClass    显示的class
		 */
		nodata: function(opts) {
			var self = this;
			var default_opts = {
				loadClass: "mui-loader-show gray",
				title: "当前还没有任何数据哦"
			}

			var optsx = self.extend(
				self.extend(
					optsx || {}, default_opts
				),
				opts);

			var waitShow = '<div class="' + optsx.loadClass + '">' + optsx.title + '</div>';
			return waitShow;
		},

		/**
		 * 没有网络的显示 
		 * Create By：   zhouhl          
		 * Create Date：2015-09-06
		 * @param loadClass      显示的class
		 * @param back           返回的函数
		 * @param refresh        刷新的函数  
		 * @param showWaiting    刷新是否显示等待框
		 * @param waitingTitle   属性时等待的标题
		 */
		nonet: function(opts) {
			var self = this;
			var default_opts = {
				loadClass: "mui-loader-show gray",
				back: function() {
					self.back();
				},
				refresh: function(showWaiting, waitingTitle) {
					self.refresh(showWaiting, waitingTitle);
				},
				showWaiting: true,
				waitingTitle: ''
			}

			var optsx = self.extend(
				self.extend(
					optsx || {}, default_opts
				),
				opts);

			var nonetShow = '<div class="' + optsx.loadClass + ' nonet"><li><h3>由于网络原因，该页面无法正常显示，您可以</h3></li><li><h3><a type="refresh">重新加载该页面</a></h3></li><li><h3>或者</h3></li><li><h3><a type="back" >返回上一页面</a></h3></li></div>';

			setTimeout(
				function() {
					mui(".nonet").on('tap', 'a', function(e) {
						if(this.getAttribute("type") == "back") {
							optsx.back();
						} else {
							optsx.refresh(optsx.showWaiting, optsx.waitingTitle);
						}

					});
				}, 500
			);

			return nonetShow;
		},

		/**
		 * 操作异常
		 * Create By：   zhouhl          
		 * Create Date：2015-09-06
		 * @param loadClass      显示的class
		 * @param back           返回的函数
		 * @param refresh        刷新的函数  
		 * @param showWaiting    刷新是否显示等待框
		 * @param waitingTitle   属性时等待的标题
		 */
		errorPage: function(opts) {
			var self = this;
			var default_opts = {
				loadClass: "mui-loader-show gray",
				back: function() {
					self.back();
				},
				refresh: function(showWaiting, waitingTitle) {
					self.refresh(showWaiting, waitingTitle);
				},
				showWaiting: true,
				waitingTitle: ''
			}

			var optsx = self.extend(
				self.extend(
					optsx || {}, default_opts
				),
				opts);

			var nonetShow = '<div class="' + optsx.loadClass + ' nonet"><li><h3>数据加载出现了一点小异常，您可以</h3></li><li><h3><a type="refresh">重新加载该页面</a></h3></li><li><h3>或者</h3></li><li><h3><a type="back" >返回上一页面</a></h3></li></div>';

			setTimeout(
				function() {
					mui(".nonet").on('tap', 'a', function(e) {
						if(this.getAttribute("type") == "back") {
							optsx.back();
						} else {
							optsx.refresh(optsx.showWaiting, optsx.waitingTitle);
						}

					});
				}, 500
			);

			return nonetShow;
		},

		/**
		 * 返回上一页面
		 * Create By：   zhouhl          
		 * Create Date：2015-09-06
		 */
		back: function() {
			var self = this;

			var mainWebviewId = self.replaceAll(plus.webview.currentWebview().id, "-sub", "-main");
			mui.fire(plus.webview.getWebviewById(mainWebviewId), "back");
		},

		/**
		 * 重新载入本页面
		 * Create By：   zhouhl          
		 * Create Date：2015-09-06
		 */
		refresh: function(showWaiting, waitingTitle) {
			var self = this;
			if(showWaiting) {
				plus.nativeUI.showWaiting(waitingTitle);
			}

			mui.fire(plus.webview.currentWebview(), self.getFileName(plus.webview.currentWebview().getURL()));
		},
		/**
		 * 根据url获得文件的路径，文件名，参数，返回json
		 * Create By：      zhouhl          
		 * Create Date： 2015-09-11
		 * @param url   当前页面的url
		 * @param waitingTitle   属性时等待的标题
		 */
		getPathFileNameParamByUrl: function(url) {
			var self = this;
			var fileNameLocation = url.lastIndexOf("/");
			var paramLocation = url.indexOf("?");
			var sysParamLocation = url.indexOf("&auto");
			var fileName = url.substring(fileNameLocation + 1, paramLocation);
			var param = url.substring(paramLocation + 1, sysParamLocation);
			var fileNameLocationBefore = url.substring(0, fileNameLocation);

			var pathLocation = fileNameLocationBefore.lastIndexOf("/");
			var path = fileNameLocationBefore.substring(pathLocation + 1, fileNameLocationBefore.length);
			var filePathName = "../" + path + "/" + fileName;
			var returnValue = {};
			returnValue.fileName = filePathName;
			returnValue.param = self.getJsonFromUrlParam(param);
			return returnValue;
		},

		/**
		 * 将url参数转换为json
		 * Create By：          zhouhl          
		 * Create Date：     2015-09-11
		 * @param param  url 参数
		 * @return 返回的json
		 * @example
		 *   in:boot=1&user=helen&sa=1
		 *   out:{boot:1,user:'helen',sa:1}
		 */
		getJsonFromUrlParam: function(param) {
			var self = this;
			var paramJson = {};
			var paramArray = param.split("&");
			for(var i = 0; i < paramArray.length; i++) {
				var paramEach = paramArray[i];
				var paramEachArray = paramEach.split("=");
				paramJson[paramEachArray[0]] = paramEachArray[1];
			}
			return paramJson;

		},

		/**
		 * 返回上一页面
		 * Create By：          zhouhl          
		 * Create Date：     2017-6-22
		 * @param  init 是否刷新上一页面 默认 false
		 * @param  callback 运行相关回调函数 默认 null
		 * 
		 */

		pageBack: function(opts) {
			var self = this;
			var default_opts = {
				init: false,
				callback: "",
				time: 0
			}
			var optsx = self.extend(
				self.extend(
					optsx || {}, default_opts
				),
				opts);

			//	    if(optsx.init)    
			//	    {    
			// 	    	 mui.fire(plus.webview.getWebviewById(self.getFieldValue("lastWebviewId", event)),self.getFieldValue("lastWebview", event)+".html");	 	  
			//	    }
			//optsx.callback();

			var mainWebviewId = self.replaceAll(plus.webview.currentWebview().id, "-sub", "-main");

			mui.fire(plus.webview.getWebviewById(mainWebviewId), "back", optsx);
		},

		/**
		 * 转换参数 将/替换成|
		 * Create By：          zhouhl          
		 * Create Date：     2015-09-13
		 * @param param  传入的参数
		 */
		convertParam: function(param) {
			var self = this;
			var returnParam = self.replaceAll(param, "/", "\|");
			return returnParam;

		},

		/**
		 * 加载css js
		 * Create By：          zhouhl          
		 * Create Date：     2015-09-24
		 * @param filename 加载文件名
		 * @param filetype 加载文件类型
		 */
		loadjscssfile: function(filename, filetype) {
			if(filetype == "js") {
				var fileref = document.createElement('script');
				fileref.setAttribute("type", "text/javascript");
				fileref.setAttribute("src", filename);
			} else if(filetype == "css") {
				var fileref = document.createElement('link');
				fileref.setAttribute("rel", "stylesheet");
				fileref.setAttribute("type", "text/css");
				fileref.setAttribute("href", filename);
			}
			if(typeof fileref != "undefined") {
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
		},

		/**
		 * 删除已加载的css js
		 * Create By：          zhouhl          
		 * Create Date：     2015-09-24
		 * @param filename 删除文件名
		 * @param filetype 删除文件类型
		 */
		removejscssfile: function(filename, filetype) {
			var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none"
			var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none"
			var allsuspects = document.getElementsByTagName(targetelement)
			for(var i = allsuspects.length; i >= 0; i--) {
				if(allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1)
					allsuspects[i].parentNode.removeChild(allsuspects[i])
			}
		},

		/**
		 * 开启遮罩
		 * Create By：   zhouhl          
		 * Create Date：2017-7-10
		 */
		startshade: function() {
			plus.nativeUI.showWaiting();
		},

		/**
		 * 当前系统平台(Android/iOS/其它平台)
		 * Create By：   zhouhl          
		 * Create Date：2017-7-12
		 */
		judgePlatform: function() {
			switch(plus.os.name) {
				case "Android":
					console.log("我是安卓");
					break;
				case "iOS":
					console.log("我是苹果");
					break;
				default:
					console.log("我是其他");
					break;
			}
		},
		/**
		 * 结束遮罩
		 * Create By：zhouhl          
		 * Create Date：2017-7-10
		 */
		endshade: function() {
			plus.nativeUI.closeWaiting();
		}
	});

	$.fn.common = function(options) {
		var self = this;
		return new Common(self, options);
		//return this;+
	}

}(mui))

/**
 * 将form中的值转换为键值对：
 * @param form {string} 表单selector  例如："#uploadform"
 * @returns {} 表单json对象
 */
function getFormJson(form) {
	var o = {};
	var a = $(form).serializeArray();
	$.each(a, function() {
		if(o[this.name] !== undefined) {
			if(!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
}