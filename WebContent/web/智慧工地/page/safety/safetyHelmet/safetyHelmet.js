var h = mui(document).common();

var app = new Vue({
	el: '#app',
	data: {
		items: [],
		cameraip: "",
		datebegin: "",
		dateend: ""
	},
	methods: {
		imgExamine: function(img) {
			$("#examineImg").attr("src",img);
			//$(e.target).click();
			$("#topPopover").addClass("mui-active");
		},
		gb: function() {
			$("#topPopover").removeClass("mui-active");
		}
	}
})


mui.plusReady(function() { //如果需要调用plus
	searchList(); //初始化
});

mui.init({
	pullRefresh: {
		container: '#pullrefresh',
		down: {
			style: 'circle',
			callback: pulldownRefresh
		},
		up: {
			auto: false,
			contentrefresh: '正在加载...',
			callback: pullupRefresh
		}
	}
});
//下拉刷新
function pulldownRefresh() {
	setTimeout(function() {
		setting.pageNumber=1;
		app.items=[];
		searchList(app.cameraip,app.datebegin,app.dateend);
		mui('#pullrefresh').pullRefresh().endPulldown();
		mui.toast("刷新成功!");
	}, 1500);
}
//上拉加载
function pullupRefresh() {
	setTimeout(function() {
		setting.pageNumber++;
		searchList(app.cameraip,app.datebegin,app.dateend);
	}, 1500);
}

function searchList(cameraip, datebegin, dateend) {
	var xhr = null;
	var url = setting.ip + "safecap/v1/RecordSelectAPI?page="+setting.pageNumber+"&pageSize="+setting.pageSize;
	var data = {
		"cameraip": cameraip,
		"datebegin": datebegin,
		"dateend": dateend
	};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				app.items = app.items.concat(eval('(' + xhr.responseText + ')'));
				mui('#pullrefresh').pullRefresh().endPullup(false); //参数为true代表没有更多数据了。
			}else{
				mui('#pullrefresh').pullRefresh().endPullup(true); //参数为true代表没有更多数据了。
			}
		}
	}
}