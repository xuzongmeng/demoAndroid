// 调用jsonp函数请求当前所在城市
jsonp('https://api.map.baidu.com/api?v=2.0&ak=yXHvNAu1EK4mX9HXWin66bVlhecUawc9&s=1&callback=getCity');
var pm="";
function getCity() {
	function city(result) {
		// 去掉城市名后的"市"
		var city = result.name.substring(0, result.name.length - 1);
		// 请求当前城市的天气数据
		jsonp(createUrl(city));
	}
	var cityName = new BMap.LocalCity();
	cityName.get(city);
}

// 数据请求函数
function jsonp(url) {
	var script = document.createElement('script');
	script.src = url;
	document.body.insertBefore(script, document.body.firstChild);
	document.body.removeChild(script);
}

// 根据城市名创建请求数据及url
function createUrl() {
	var cityName = '';
	cityName = arguments[0];
	var urls = "";
	urls = 'https://sapi.k780.com/?app=weather.today&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json&jsoncallback=getWeather&weaid='
			+ encodeURI(cityName);
	
	return urls;
}

getPM25();
function getPM25() {
	$.ajax({
		type : 'get',
		async : false,
		url : "http://api.k780.com/?app=weather.pm25&weaid=101020100&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json&jsoncallback=data",
		dataType : 'jsonp',
		jsonp : 'callback',
		jsonpCallback : 'data',
		success : function(data) {
			pm=data.result.aqi;
		},
		error : function() {
		}
	});
}

// 数据请求成功回调函数，用于将获取到的数据放入页面相应位置
function getWeather(response) {
	var oSpan = document.getElementsByClassName('info');
	var data = response.result;
	$("#weather").html(data.weather);//今天天气
	$("#weather_curr").html(data.weather_curr);//当前天气
	$("#temp_high").html(data.temp_high);//最高气温
	$("#temp_low").html(data.temp_low);//最低气温
	$("#temperature_curr").html(data.temperature_curr);//当前气温
	$("#temp_curr").html(data.temp_curr);//当前湿度
	$("#wind").html(data.wind);//当前风向
	$("#winp").html(data.winp);//当前风力
	$("#pm").html(pm);//PM2.5值
}

