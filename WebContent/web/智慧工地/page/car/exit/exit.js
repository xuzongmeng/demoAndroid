var h = mui(document).common();

var app = new Vue({
	el: '#app',
	data: {
		items: [],
		carNumber: ""
	},
	methods: {
		search: function() {
			app.items=[];
			searchList(this.carNumber);
		},
		imgExamine: function(img) {
			$("#examineImg").attr("src",img);
			//$(e.target).click();
			$("#topPopover").addClass("mui-active");
		},
		gb: function() {
			$("#topPopover").removeClass("mui-active");
		}
	}
})


mui.plusReady(function() { //如果需要调用plus
	searchList(); //初始化
});

mui.init({
	pullRefresh: {
		container: '#pullrefresh',
		up: {
			auto: false,
			contentrefresh: '正在加载...',
			callback: pullupRefresh
		}
	}
});
 
//上拉加载
function pullupRefresh() {
	setTimeout(function() {
		setting.pageNumber++;
		searchList(app.carNumber);
	}, 1500);
}

function searchList(carNumber) {
	var datebegin="";
	var dateend="";
	var xhr = null;
	var url = setting.ip + "car/v1/CarSelectAPI?page="+setting.pageNumber+"&pageSize="+setting.pageSize;
	if($("#result").html()!=""){
		datebegin=$("#result").html()+":00";
	}
	if($("#result1").html()!=""){
		dateend=$("#result1").html()+":00";
	}
	var data = {
		"carNumber": carNumber,
		"getOut":   "出",
		"datebegin": datebegin,
		"dateend": dateend
	};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				app.items = app.items.concat(eval('(' + xhr.responseText + ')'));
				mui('#pullrefresh').pullRefresh().endPullup(false); //参数为true代表没有更多数据了。
			}else{
				mui('#pullrefresh').pullRefresh().endPullup(true); //参数为true代表没有更多数据了。
			}
		}
	}
}