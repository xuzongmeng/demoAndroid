var h = mui(document).common();

var app = new Vue({
	el: '#app',
	data: {
		items: []
	},
	methods: {
		search: function() {
			this.items = [];
			searchList(this.name);
		}
	}
})


mui.plusReady(function() { //如果需要调用plus
	searchList(); //初始化
});


function searchList() {
	var xhr = null;
	var url = setting.ip + "user/v1/selectPersonSourceCount?page="+setting.pageNumber+"&pageSize="+setting.pageSize;
	if($("#result").html()!=""){
		datebegin=$("#result").html()+":00";
	}
	if($("#result1").html()!=""){
		dateend=$("#result1").html()+":00";
	}
	var data = {};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				var item = eval('(' + xhr.responseText + ')');
				app.items = app.items.concat(item.rows);
			}
		}
	}
}