var h = mui(document).common();

var app = new Vue({
	el: '#app',
	data: {
		items: [],
		username: ""
	},
	methods: {
		search: function() {
			app.items=[];
			searchList(this.username);
		},
		userUpdata: function(id){
			localStorage.setItem("userId",id);
			var htype = {
				obj: this,
				url: 'userUpdata.html',
				id: 'userUpdata'
			}
			h.eachClick(htype);
		},
		userDelete: function(id){
			var btnArray = ['否', '是'];
			mui.confirm('您确定删除该用户吗？', '用户管理', btnArray, function(e) {
				if (e.index == 1) {
					userDelete(id);
				}
			})
		}
	}
})

mui.plusReady(function() { //如果需要调用plus
	searchList(); //初始化
});

//查询页面
function searchList(username) {
	var xhr = null;
	var url = setting.ip + "user/v1/selectUserAPI?page=1&pageSize=50";
	var data = {
		"username": username
	};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				app.items = eval('(' + xhr.responseText + ')');
			}
		}
	}
}

//新增用户
function userAdd(){
	var name1=$("#name1").val();
	var password1=$("#password1").val();
	var passwords1=$("#passwords1").val();
	
	if(name1==null||name1==""){
		mui.toast("请输入完整!");
		return;
	}
	
	if(password1==null||password1==""){
		mui.toast("请输入完整!");
		return;
	}
	
	if(password1!=passwords1){
		mui.toast("两次密码输入不一样!");
		return;
	}
	var xhr = null;
	var url = setting.ip + "user/v1/add";
	var data = {
		"username": name1,
		"password": password1
	};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				mui.toast("新增用户成功!");
				setTimeout(function(){
					location=location;
				},1000);
			}
		}
	}
}

//删除用户
function userDelete(id){
	var xhr = null;
	var url = setting.ip + "user/v1/deleteUser";
	var data = {
		"id":id
	};
	xhr = new plus.net.XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				mui.toast("删除用户成功!");
				setTimeout(function(){
					location=location;
				},1000);
			}
		}
	}
}