/**
 * Created by xuzongmeng on 2018/1/26.
 */
$(function(){
    /*滑动切换*/
    var swiper = new Swiper('.swiper-container', {

        onSlideNextStart: function(swiper){
            $('.selected').next().addClass('selected');
            $('.selected').first().removeClass('selected');
        },
        onSlidePrevStart: function(swiper){
            $('.selected').prev().addClass('selected');
            $('.selected').last().removeClass('selected');
        }

    });
    /*点击菜单切换*/
    $('#nav .sub-item').click(function(){

        $('.selected').removeClass('selected');
        $(this).addClass('selected');

        var num = $(this).data('num');
        swiper.slideTo(num, 500, false);
    });
});
