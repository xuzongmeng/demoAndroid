package com.xzm.person.utils;

import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.widget.TextView;

import com.xzm.person.app.Myapplication;

/**
 *  Created by xuzongmeng on 2018/1/10.
 */

public class ThemeUtil {
    public static int getColor(@ColorRes int id) {
       return ContextCompat.getColor(Myapplication.getContext(), id);
    }
    public static Drawable getDrawable(@DrawableRes  int id) {
        return ContextCompat.getDrawable(Myapplication.getContext(), id);
    }
    public static void setTextAppearance(TextView  tv,int textAppearance) {
        TextViewCompat.setTextAppearance(tv, textAppearance);
//        return ContextCompat.getDrawable(BaseApplication.getContext(), id);
    }
}
