package com.xzm.person.utils;

import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

/**
 * Created by PC on 2017/6/1.
 * 键盘弹出 顶出布局
 */
public class ChatBug {

    public static void assistActivty(Activity activity){
        new ChatBug(activity);
    }

    private View mChildOfContent;
    private int usableHeightPrevious;
    private FrameLayout.LayoutParams frameLayoutParams;

    private ChatBug(Activity activity){
        FrameLayout content= (FrameLayout) activity.findViewById(android.R.id.content);
        mChildOfContent=content.getChildAt(0);
        mChildOfContent.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        possibly();
                    }
                });
        frameLayoutParams= (FrameLayout.LayoutParams) mChildOfContent.getLayoutParams();
    }

    private void possibly(){
        int usableHeiNow=computeUsableHeight();
        if (usableHeiNow!=usableHeightPrevious){
            int usableSansKeyBoard=mChildOfContent.getRootView().getHeight();
            int h=usableSansKeyBoard-usableHeiNow;
            if (h>(usableSansKeyBoard/4)){
                frameLayoutParams.height=usableSansKeyBoard-h;
            }else {
                frameLayoutParams.height=usableSansKeyBoard;
            }
            mChildOfContent.requestLayout();
            usableHeightPrevious=usableHeiNow;
        }
    }

    private int computeUsableHeight(){
        Rect r=new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);
        return (r.bottom);
    }
}
