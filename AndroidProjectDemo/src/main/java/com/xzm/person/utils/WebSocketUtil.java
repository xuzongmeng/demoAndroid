package com.xzm.person.utils;//package com.xzm.person.utils;
//
//
//import com.socks.library.KLog;
//import com.xzm.person.app.Myapplication;
//import com.xzm.person.utils.autobahn.WebSocket;
//import com.xzm.person.utils.autobahn.WebSocketConnection;
//import com.xzm.person.utils.autobahn.WebSocketConnectionHandler;
//import com.xzm.person.utils.autobahn.WebSocketException;
//import com.xzm.person.utils.autobahn.WebSocketMessage;
//import com.xzm.person.utils.autobahn.WebSocketOptions;
//
//
///**
// * webSocket聊天工具类
// * Created by zwl on 2016/8/29.
// */
//public class WebSocketUtil {
//    private LoopTimer mWebSocketLoopTimer;
//
//    private static WebSocketUtil instance;
//    private WebSocketOptions mOptions;
//    private WebSocket ws;
//    /**
//     * 重连次数
//     */
//    private int i;
//
//    public static WebSocketUtil getInstance() {
//        if (instance == null) {
//            instance = new WebSocketUtil();
//        }
//        return instance;
//    }
//
//    private WebSocketUtil() {
//        //初始化webWocket设置
//        mOptions = new WebSocketOptions();
//        mOptions.setSocketConnectTimeout(15 * 1000);
//        mOptions.setValidateIncomingUtf8(true);
//    }
//
//    public WebSocket connection(String host, final String location) throws WebSocketException {
//        //建立握手连接
//        WebSocketMessage.ClientHandshake hs = new WebSocketMessage.ClientHandshake(host, null, "http://zhibo.jindinghui.com.cn");
//        ws = new WebSocketConnection(hs);
//
//        //初始化定时器
//        mWebSocketLoopTimer = new LoopTimer(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    doConnection(location, mOptions);
//                } catch (WebSocketException e) {
//                    e.printStackTrace();
//                }
//                i++;
//                KLog.e("HQSocket第" + i + "次重连");
//            }
//        }, 5 * 1000);
//
//        //连接聊天WebSocket
//        doConnection(location, mOptions);
//        return ws;
//    }
//
//    /**
//     * 连接WebSocket
//     *
//     * @param location
//     * @param mOptions
//     */
//    private void doConnection(String location, WebSocketOptions mOptions) throws WebSocketException {
//        ws.connect(location, new WebSocketConnectionHandler() {
//            //连接成功
//            @Override
//            public void onOpen() {
//                KLog.i("onOpen");
////                EventUitl.sendLoginToWebSocket();
//                i = 0;
//                //停止重连
//                if (mWebSocketLoopTimer != null && mWebSocketLoopTimer.isRunning()) {
//                    mWebSocketLoopTimer.stop();
//                }
//            }
//
//            //接收数据
//            @Override
//            public void onTextMessage(String payload) {
//                KLog.i("onTextMessage");
//                KLog.json(payload);
//                //转换成jsonBean
////                if(!TextUtils.isEmpty(payload)){
////                    Chat chat = new Gson().fromJson(payload, Chat.class);
////                    if ("ping".equals(chat.getType())) {
////                        ws.sendTextMessage("{\"type\":\"pong\"}");
////                        return;
////                    }
////                    if (!checkChatRepeat(chat.getLid() + "")
////                            && "say".equals(chat.getType())
////                            && "1".equals(chat.getShstatus())
////                            && !"0".equals(chat.getMid())) {
////                        EventUitl.sendWebSocketChat(chat);
////                    }
////                }
//            }
//
//            /**
//             * 检查该消息是否存在
//             *
//             * @param
//             * @return
//             */
////            private boolean checkChatRepeat(String lid) {
////                for (String str : ZhiBo_Activity.chatIDs) {
////                    if (str.equals(lid)) {
////                        return true;
////                    }
////                }
////                return false;
////            }
//
//
//            //连接断开
//            @Override
//            public void onClose(int code, String reason) {
//                KLog.e("onClose");
//                if (mWebSocketLoopTimer == null) return;
//                //如果网络处于连接状态则5秒重连一次，如果网络处于断开状态则30秒重连一次
//                if (NetworkUtil.isConnected(Myapplication.getContext())) {
//                    mWebSocketLoopTimer.setIntervalMillis(5 * 1000);
//                } else {
//                    mWebSocketLoopTimer.setIntervalMillis(15 * 1000);
//                }
//                mWebSocketLoopTimer.delayStart();
//            }
//        }, mOptions);
//    }
//
//    /**
//     * 发送消息
//     *
//     * @param message
//     */
//    public void send(String message) {
//        if (ws != null && ws.isConnected()) {
//            ws.sendTextMessage(message);
//        } else {
//            KLog.e("webSocket —>" + ws == null ? null : "未连接");
//        }
//    }
//
//    /**
//     * 断开连接
//     */
//    public void disConnection() {
//        if (ws != null && ws.isConnected()) {
//            ws.disconnect();
//        }
//    }
//}
