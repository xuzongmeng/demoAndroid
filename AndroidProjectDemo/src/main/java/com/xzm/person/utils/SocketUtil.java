package com.xzm.person.utils;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.socks.library.KLog;
import com.vilyever.socketclient.SocketClient;
import com.vilyever.socketclient.helper.SocketClientAddress;
import com.vilyever.socketclient.helper.SocketClientDelegate;
import com.vilyever.socketclient.helper.SocketClientSendingDelegate;
import com.vilyever.socketclient.helper.SocketPacket;
import com.vilyever.socketclient.helper.SocketResponsePacket;
import com.xzm.person.app.APPConfig;
import com.xzm.person.app.Myapplication;
import com.xzm.person.ui.bean.HangQing;


/**
 * 行情socket工具类
 * Created by zwl on 2016/7/11.
 */
public class SocketUtil {
    private static SocketClient localSocketClient;
    //    private static List<HQSocketClientDelegateHandle> handles;
    private static SocketUtil instance;
    private static int i;
    private static LoopTimer mLoopTimer;
    private static
    @NonNull
    SocketResponsePacket mResponsePacket;

    private SocketUtil() {
    }

    public static SocketUtil getInstance() {
        if (instance == null) {
            instance = new SocketUtil();
        }
        return instance;

    }


    /**
     * 初始化Socket
     */
    public  void   initSocket(){
        mLoopTimer = new LoopTimer(new Runnable() {
            @Override
            public void run() {
                localSocketClient.connect();
                i++;
                KLog.e("Socket第" + i + "次重连");
            }
        }, 5 * 1000);
//        com.vilyever.socketclient.helper
        // 设置ip端口，连接超时时长
        localSocketClient = new SocketClient(new SocketClientAddress(APPConfig.Network.SOCKET, APPConfig.Network.PORT, 15 * 1000));
        // 设置发送和接收String消息的默认编码
        localSocketClient.setCharsetName("UTF-8");
        // 设置自动发送心跳包的时间间隔，若值小于0则不发送心跳包
        localSocketClient.getHeartBeatHelper().setHeartBeatInterval(-1);
        // 设置远程端多长时间内没有消息发送到本地就自动断开连接，若值小于0则不自动断开
        localSocketClient.getHeartBeatHelper().setRemoteNoReplyAliveTimeout(-1);
        // 设置自动发送心跳包的字符串，若为null则不发送心跳包
        localSocketClient.getHeartBeatHelper().setSendString(null);
        // 设置从远程端接收的心跳包字符串，onResponse回调将过滤此信息，若为null则不过滤
        localSocketClient.getHeartBeatHelper().setReceiveString(null);
    }
    /**
     * 接收socket数据
     */
    public void receiveSocketDate() {

        //注册连接
        localSocketClient.registerSocketClientDelegate(new SocketClientDelegate() {
            @Override
            public void onConnected(SocketClient client) {
                KLog.d("onConnected");

                i = 0;
                //停止重连
                if (mLoopTimer != null && mLoopTimer.isRunning()) {
                    mLoopTimer.stop();
                }
            }

            @Override
            public void onDisconnected(SocketClient client) {
                KLog.d("onDisconnected");
//                for (HQSocketClientDelegateHandle handle : handles) {
//                    handle.onDisconnected(client);
//                }
                if (mLoopTimer == null) return;
                //如果网络处于连接状态则5秒重连一次，如果网络处于断开状态则30秒重连一次
                if (NetworkUtil.isConnected(Myapplication.getContext())) {
                    mLoopTimer.setIntervalMillis(5 * 1000);
                } else {
                    mLoopTimer.setIntervalMillis(15 * 1000);
                }
                mLoopTimer.delayStart();
            }

            @Override
            public void onResponse(SocketClient client, @NonNull SocketResponsePacket responsePacket) {
                String response = responsePacket.getMessage();

                KLog.d("Socket真实数据===="+response);
                Gson gson = new Gson();
                HangQing hqDate = gson.fromJson(response, HangQing.class);
                KLog.d("Socket数据11="+hqDate.getHigh());
                KLog.d("Socket数据22="+hqDate.getCode());
                KLog.d("Socket数据33="+hqDate.getLastClose());
                KLog.d("Socket数据44="+hqDate.getLow());
                KLog.d("Socket数据55="+hqDate.getName());
                KLog.d("Socket数据66="+hqDate.getOpen());
                KLog.d("Socket数据77="+hqDate.getQuoteTime());
            }
        });
        try {
            localSocketClient.connect();
        } catch (RuntimeException e) {
            KLog.e("error：" + e.getMessage());
        }
    }


    /**
     *
     * @param发送Socket 数据
     */
    public void  sendSocketDate(final String msg){
// 对应removeSocketClientSendingDelegate
        localSocketClient.registerSocketClientSendingDelegate(new SocketClientSendingDelegate() {
            /**
             * 数据包开始发送时的回调
             */
            @Override
            public void onSendPacketBegin(SocketClient client, SocketPacket packet) {
                SocketPacket socketPacket = client.sendString(msg);
            }

            /**
             * 数据包取消发送时的回调
             * 取消发送回调有以下情况：
             * 1. 手动cancel仍在排队，还未发送过的packet
             * 2. 断开连接时，正在发送的packet和所有在排队的packet都会被取消
             */
            @Override
            public void onSendPacketCancel(SocketClient client, SocketPacket packet) {
                SocketPacket socketPacket = client.sendString(msg);
            }

            /**
             * 数据包发送的进度回调
             * progress值为[0.0f, 1.0f]
             * 通常配合分段发送使用
             * 可用于显示文件等大数据的发送进度
             */
//            @Override
//            public void onSendingPacketInProgress(SocketClient client, SocketPacket packet, float progress, int sendedLength) {
//            }

            /**
             * 数据包完成发送时的回调
             */
            @Override
            public void onSendPacketEnd(SocketClient client, SocketPacket packet) {
                SocketPacket socketPacket = client.sendString(msg);

            }

            @Override
            public void onSendPacketProgress(SocketClient client, SocketPacket packet, float progress) {
                SocketPacket socketPacket = client.sendString(msg);
            }
        });
    }

    /**
     * 断开连接
     */
    public void disConnect() {
        if (localSocketClient != null && localSocketClient.isConnected()) {
            localSocketClient.disconnect();
            localSocketClient = null;
            mLoopTimer.stop();
//            handles.clear();
        }
    }

//    public interface HQSocketClientDelegateHandle {
//        void onConnected(SocketClient client);
//
//        void onDisconnected(SocketClient client);
//
//        void onResponse(SocketClient client, @NonNull SocketResponsePacket responsePacket);
//    }
}
