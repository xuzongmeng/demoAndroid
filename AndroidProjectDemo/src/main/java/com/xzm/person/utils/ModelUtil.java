package com.xzm.person.utils;
import android.text.TextUtils;

import com.xzm.person.ui.bean.People;
import com.xzm.person.ui.bean.PeopleComparator;
import com.xzm.person.ui.widget2.sort.DropDownMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
public class ModelUtil {


    public static List<People> getPeopleData() {
        List<People> adList = new ArrayList<>();
        adList.add(new People("北京",14,"2001-09-18","白羊座"));
        adList.add(new People("上海",21,"1995-09-25","金牛座"));
        adList.add(new People("广州",31,"1985-09-25","双子座"));
        adList.add(new People("深圳",41,"1975-09-25","巨蟹座"));

        adList.add(new People("北京",15,"2000-09-18","白羊座"));
        adList.add(new People("上海",22,"1994-09-25","金牛座"));
        adList.add(new People("广州",32,"1984-09-25","双子座"));
        adList.add(new People("深圳",42,"1974-09-25","巨蟹座"));

        adList.add(new People("北京",16,"1999-09-18","白羊座"));
        adList.add(new People("上海",23,"1993-09-25","金牛座"));
        adList.add(new People("广州",33,"1983-09-25","双子座"));
        adList.add(new People("深圳",43,"1973-09-25","巨蟹座"));


        adList.add(new People("北京",17,"1998-09-18","白羊座"));
        adList.add(new People("上海",24,"1992-09-25","金牛座"));
        adList.add(new People("广州",34,"1982-09-25","双子座"));
        adList.add(new People("深圳",44,"1972-09-25","巨蟹座"));

/*======================================================*/
        adList.add(new People("上海",12,"1998-05-18","白羊座"));
        adList.add(new People("广州",15,"1998-04-25","金牛座"));
        adList.add(new People("深圳",10,"1998-03-25","双子座"));

        adList.add(new People("北京",23,"1998-05-18","白羊座"));
        adList.add(new People("广州",24,"1998-04-25","金牛座"));
        adList.add(new People("深圳",25,"1998-03-25","双子座"));


        adList.add(new People("北京",32,"1998-05-18","白羊座"));
        adList.add(new People("上海",33,"1998-04-25","金牛座"));
        adList.add(new People("深圳",34,"1998-03-25","双子座"));


        adList.add(new People("北京",51,"1966-05-18","白羊座"));
        adList.add(new People("上海",52,"1966-04-25","金牛座"));
        adList.add(new People("广州",53,"1966-03-25","双子座"));

        /*======================================================*/

        adList.add(new People("北京",11,"1966-05-18","金牛座"));
        adList.add(new People("北京",10,"1943-05-18","双子座"));
        adList.add(new People("北京",9,"1921-05-18","巨蟹座"));
        return adList;
    }

    public static List<People> getFilterDataByCity(List<People> list, Map<String, String> rule){
        System.out.println("====getFilterDataByCity"+rule.get(DropDownMenu.TYPE_CITY));
        System.out.println("====getFilterDataByCity"+rule.get(DropDownMenu.TYPE_AGE));
        System.out.println("====getFilterDataByCity"+rule.get(DropDownMenu.TYPE_GRID));
        System.out.println("====getFilterDataByCity"+rule.get(DropDownMenu.TYPE_SORT));

//        String ruleCity = rule.get(DropDownMenu.TYPE_CITY);
        String ruleAge = rule.get(DropDownMenu.TYPE_AGE);
        String ruleGrid = rule.get(DropDownMenu.TYPE_GRID);
        String ruleSort = rule.get(DropDownMenu.TYPE_SORT);

        List<People> tempListCity = new ArrayList<>();
        List<People> tempListAge = new ArrayList<>();
        List<People> tempListGrid = new ArrayList<>();
        List<People> tempListSort = new ArrayList<>();
            if (!"不限".equals(rule.get(DropDownMenu.TYPE_CITY))){
                for (int i=0; i<list.size(); i++) {
                    if (list.get(i).getCity().equals(rule.get(DropDownMenu.TYPE_CITY))) {
                        tempListCity.add(list.get(i));
                    }
                }
            }
            if (!TextUtils.isEmpty(ruleAge)){
               tempListAge = getFilterDataByAge(tempListCity, rule);
            }
            if (!TextUtils.isEmpty(ruleGrid)){
                tempListGrid = getFilterDataByConstellation(tempListAge, rule);
            }
            if (!TextUtils.isEmpty(ruleSort)){
                tempListSort = getFilterDataByAge(tempListGrid, rule);
            }
        return  tempListSort;
    }


    public static List<People> getFilterDataByAge(List<People> list, Map<String, String> rule){
        System.out.println("====getFilterDataByAge"+rule.get(DropDownMenu.TYPE_CITY));
        System.out.println("====getFilterDataByAge"+rule.get(DropDownMenu.TYPE_AGE));
        System.out.println("====getFilterDataByAge"+rule.get(DropDownMenu.TYPE_GRID));
        System.out.println("====getFilterDataByAge"+rule.get(DropDownMenu.TYPE_SORT));
        List<People> tempList = new ArrayList<>();
        if (!"不限".equals(rule.get(DropDownMenu.TYPE_AGE))){
            for (int i=0; i<list.size(); i++) {
                if (rule.get(DropDownMenu.TYPE_AGE).equals("18岁以下")&&list.get(i).getAge()<18) {
                    tempList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("18-30岁")&&list.get(i).getAge()<=30&&list.get(i).getAge()>=18){
                    tempList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("31-40岁")&&list.get(i).getAge()<=40&&list.get(i).getAge()>=31){
                    tempList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("40岁以上")&&list.get(i).getAge()>40){
                    tempList.add(list.get(i));
                }
            }
        }else {

        }
        return  tempList;
    }
    public static List<People> getFilterDataByConstellation(List<People> list, Map<String, String> rule){
        System.out.println("====getFilterDataByConstellation"+rule.get(DropDownMenu.TYPE_CITY));
        System.out.println("====getFilterDataByConstellation"+rule.get(DropDownMenu.TYPE_AGE));
        System.out.println("====getFilterDataByConstellation"+rule.get(DropDownMenu.TYPE_GRID));
        System.out.println("====getFilterDataByConstellation"+rule.get(DropDownMenu.TYPE_SORT));
        List<People> tempList = new ArrayList<>();
        if (!"不限".equals(rule.get(DropDownMenu.TYPE_GRID))){
            for (int i=0; i<list.size(); i++) {
                if (list.get(i).getConstellation().equals(rule.get(DropDownMenu.TYPE_GRID))) {
                    tempList.add(list.get(i));
                }
            }
        }
        return  tempList;
    }

    public static List<People> getFilterDataBySort(List<People> list, Map<String, String> rule) {
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_CITY));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_AGE));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_GRID));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_SORT));
         if (!"不限".equals(rule)){
             if (rule.get(DropDownMenu.TYPE_SORT).equals("生日将序")){
                 PeopleComparator peopleComparator =new PeopleComparator();
                 Collections.sort(list, peopleComparator);//用我们写好的Comparator对user进行排序
             }else if (rule.get(DropDownMenu.TYPE_SORT).equals("生日升序")){
                 Collections.sort(list);
             }
         }
        return  list;
    }








    public static List<People> getFilterData(List<People> list, Map<String, String> rule) {
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_CITY));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_AGE));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_GRID));
        System.out.println("====getFilterDataBySort"+rule.get(DropDownMenu.TYPE_SORT));

        List<People> cityList = new ArrayList<>();
        List<People> ageList = new ArrayList<>();
        List<People> gridList = new ArrayList<>();
        List<People> sortList = new ArrayList<>();
        String ruleCity = rule.get(DropDownMenu.TYPE_CITY);
        String ruleAge = rule.get(DropDownMenu.TYPE_AGE);
        String ruleGrid = rule.get(DropDownMenu.TYPE_GRID);
        String ruleSort = rule.get(DropDownMenu.TYPE_SORT);



        if (!TextUtils.isEmpty(ruleCity)){
            for (int i=0; i<list.size(); i++) {
                if (list.get(i).getCity().equals(rule.get(DropDownMenu.TYPE_CITY))) {
                    cityList.add(list.get(i));
                }
            }
        }

        if (!TextUtils.isEmpty(ruleAge)){
            for (int i=0; i<list.size(); i++) {
                if (rule.get(DropDownMenu.TYPE_AGE).equals("18岁以下")&&list.get(i).getAge()<18) {
                    ageList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("18-30岁")&&list.get(i).getAge()<=30&&list.get(i).getAge()>=18){
                    ageList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("31-40岁")&&list.get(i).getAge()<=40&&list.get(i).getAge()>=31){
                    ageList.add(list.get(i));
                }else if (rule.get(DropDownMenu.TYPE_AGE).equals("40岁以上")&&list.get(i).getAge()>40){
                    ageList.add(list.get(i));
                }
            }
        }


        if (!TextUtils.isEmpty(ruleGrid)){
            for (int i=0; i<list.size(); i++) {
                if (list.get(i).getConstellation().equals(rule.get(DropDownMenu.TYPE_GRID))) {
                    gridList.add(list.get(i));
                }
            }
        }

        if (!TextUtils.isEmpty(ruleSort)){
            if (rule.get(DropDownMenu.TYPE_SORT).equals("生日将序")){
                PeopleComparator peopleComparator =new PeopleComparator();
                Collections.sort(list, peopleComparator);//用我们写好的Comparator对user进行排序
            }else if (rule.get(DropDownMenu.TYPE_SORT).equals("生日升序")){
                Collections.sort(list);
            }
        }
        return  list;
    }
}
