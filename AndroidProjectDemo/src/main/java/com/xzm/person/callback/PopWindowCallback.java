package com.xzm.person.callback;

/**
 * Created by xuzongmeng on 2017/1/13.
 */

public interface PopWindowCallback {
    /**
     * 确认按钮滴回调
     */

    void  onConfirmClick();

    /**
     * 取消按钮滴回调
     */
    void  onCancelClick();

}
