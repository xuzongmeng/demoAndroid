package com.xzm.person.mvp.main.base;

/**
 * Created by WuXiaolong on 2016/10/19.
 * github:https://github.com/WuXiaolong/
 * 微信公众号：吴小龙同学
 * 个人博客：http://wuxiaolong.me/
 */

public interface BaseView {
    void showLoadings();

    void hideLoadings();
}
