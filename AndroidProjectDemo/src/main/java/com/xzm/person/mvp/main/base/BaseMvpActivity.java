package com.xzm.person.mvp.main.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;



/**
 */
public abstract class BaseMvpActivity<P extends BasePresenter> extends AppCompatActivity {
    protected P mvpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mvpPresenter = createPresenter();
        super.onCreate(savedInstanceState);
    }

    protected abstract P createPresenter();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mvpPresenter != null) {
            mvpPresenter.detachView();
        }
    }

//    public void showLoadings() {
//        showLoading();
//    }
//
//    public void hideLoadings() {
//        showContent();
//    }
}
