package com.xzm.person.mvp.model;

/**
 * Created by Administrator on 2016/8/5.
 */
public interface WXJXModel {
    void getWXJX(String key, int pno, int ps, WXJXModelImp.getWXJXListener listener);

    void getWXJXRefresh(String key, int pno, int ps, WXJXModelImp.WXJXRefreshListener
            refreshListener);

    void getWXJXLoadMore(String key, int pno, int ps, WXJXModelImp.WXJXLoadMoreListener loadMoreListener);
}
