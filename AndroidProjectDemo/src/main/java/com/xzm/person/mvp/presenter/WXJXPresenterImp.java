package com.xzm.person.mvp.presenter;

import com.socks.library.KLog;
import com.xzm.person.mvp.model.WXJXModelImp;
import com.xzm.person.mvp.view.WXJXView;
import com.xzm.person.ui.bean.WXJX;

;

/**
 * Created by Administrator on 2016/8/5.
 */
public class WXJXPresenterImp implements WXJXPresenter {

    public String key = "978c09d3c30f8640e3bd480b6bf4301f";
    public int pno = 1;
    public int ps = 20;
    private WXJXModelImp mWxjxModelImp;
    private WXJXView mWxjxView;

    public WXJXPresenterImp(WXJXView mWxjxView) {
        this.mWxjxView = mWxjxView;
        this.mWxjxModelImp = new WXJXModelImp();
    }

    /**
     * 默认加载第一页  20条数据
     */
    @Override
    public void getWXJX() {
        pno = 1;
        mWxjxModelImp.getWXJX(key, pno, ps, new WXJXModelImp.getWXJXListener() {
            @Override
            public void getWXJXSuccess(WXJX wxjx) {
                mWxjxView.getWXJXSuccess(wxjx);
            }

            @Override
            public void getWXJXfailed() {
                mWxjxView.getWXJXfailed();
            }
        });
    }

    /**
     * 刷新第1页  20条数据
     */
    @Override
    public void refreshWXJX() {
        pno = 1;
        mWxjxModelImp.getWXJXRefresh(key, pno, ps, new WXJXModelImp.WXJXRefreshListener() {
            @Override
            public void getWXJXRefreshSuccess(WXJX wxjx) {
                mWxjxView.getWXJXRefreshSuccess(wxjx);
            }
            @Override
            public void getWXJXRefreshfailed() {
                mWxjxView.getWXJXRefreshfailed();
            }
        });
    }

    /**
     * 加载更多
     */
    @Override
    public void loadMoreWXJX() {
        pno++;
        KLog.d("微信精选页码数1111111==" + pno);
        String number = String.valueOf(pno);
        KLog.d("微信精选页码数22222==" + number);
        mWxjxModelImp.getWXJXLoadMore(key, pno, ps, new WXJXModelImp.WXJXLoadMoreListener() {
            @Override
            public void getWXJXLoadMoreSuccess(WXJX wxjx) {
                mWxjxView.getWXJXLoadMoreSuccess(wxjx);
            }
            @Override
            public void getWXJXLoadMorefailed() {
                mWxjxView.getWXJXLoadMorefailed();
            }
        });
    }
}
