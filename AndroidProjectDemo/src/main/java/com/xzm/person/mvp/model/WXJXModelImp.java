package com.xzm.person.mvp.model;

import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.socks.library.KLog;
import com.xzm.person.ui.bean.WXJX;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2016/8/5.
 */
public class WXJXModelImp implements WXJXModel {

    @Override
    public void getWXJX(String key, int pno, int ps, final getWXJXListener listener) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://v.juhe.cn/weixin/query?").append("key=").append(key).append("&pno=").append(pno).append("&ps=").append(ps);
        String path = sb.toString();
        KLog.e("微信精选默认==" + "pno=" + pno + ",ps=" + ps);
        KLog.e("微信精选默认地址==" + path);
//        http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.configTimeout(5000);
        httpUtils.send(HttpMethod.GET, path, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                String result = responseInfo.result;

                try {
                    JSONObject jsonObject=new JSONObject(result);
                    String title = jsonObject.getString("title");
                    KLog.d("微信精选=="+title);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (result != null) {
                    Gson gson = new Gson();
                    WXJX wxjx = gson.fromJson(result, WXJX.class);
//
//                    ACache aCache = ACache.get(BaseApplication.getContext());
//                    aCache.put("key", wxjx);

                    listener.getWXJXSuccess(wxjx);
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                listener.getWXJXfailed();
            }
        });
    }

    /**
     * 刷新列表
     */
    @Override
    public void getWXJXRefresh(String key, int pno, int ps, final WXJXRefreshListener refreshListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://v.juhe.cn/weixin/query?").append("key=").append(key).append("&pno=").append(pno).append("&ps=").append(ps);
        String path = sb.toString();
        KLog.e("微信精选刷新==" + "pno=" + pno + ",ps=" + ps);
        KLog.e("微信精选刷新请求地址==" + path);
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.configTimeout(5000);
        httpUtils.send(HttpMethod.GET, path, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                String result = responseInfo.result;
                if (result != null) {
                    Gson gson = new Gson();
                    WXJX wxjx = gson.fromJson(result, WXJX.class);
                    KLog.e("请求数据===" + wxjx.getResult().getList().get(0).getTitle());
                    refreshListener.getWXJXRefreshSuccess(wxjx);

                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                refreshListener.getWXJXRefreshfailed();
            }
        });
    }

    @Override
    public void getWXJXLoadMore(String key, int pno, int ps, final WXJXLoadMoreListener loadMoreListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://v.juhe.cn/weixin/query?").append("key=").append(key).append("&pno=").append(pno).append("&ps=").append(ps);
        KLog.e("微信精选加载更多==" + "pno=" + pno + ",ps=" + ps);
        String path = sb.toString();
        KLog.e("微信精选加载更多请求地址==" + path);
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.configTimeout(5000);
        httpUtils.send(HttpMethod.GET, path, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                String result = responseInfo.result;
                if (result != null) {
                    Gson gson = new Gson();
                    WXJX wxjx = gson.fromJson(result, WXJX.class);
//                    KLog.e("请求数据===" + wxjx.getResult().getList().get(0).getTitle());
                    loadMoreListener.getWXJXLoadMoreSuccess(wxjx);
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                loadMoreListener.getWXJXLoadMorefailed();
            }
        });
    }

    public interface getWXJXListener {
        void getWXJXSuccess(WXJX wxjx);

        void getWXJXfailed();
    }

    public interface WXJXRefreshListener {
        void getWXJXRefreshSuccess(WXJX wxjx);

        void getWXJXRefreshfailed();
    }

    public interface WXJXLoadMoreListener {
        void getWXJXLoadMoreSuccess(WXJX wxjx);

        void getWXJXLoadMorefailed();
    }


    //    public void  test(String key,String pno,String ps){
//        StringBuilder  sb=new StringBuilder();
//        sb.append("http://v.juhe.cn/weixin/query?").append("key=").append(key).append("&pno=").append(pno).append("&ps=").append(ps);
//      KLog.d("微信精选请求URl="+sb.toString());
//        HttpHelper.doGet(sb.toString(), new HttpHelper.RequestCallback<WXJX>() {
//            @Override
//            public void onSuccess(WXJX wxjx) {
//                KLog.d("微信精选请求成功....."+wxjx.getResult().getList().get(0).getTitle());
//            }
//
//            @Override
//            public void onFailure(String errorStr) {
//                KLog.d("微信精选请求失败.....");
//            }
//        }, WXJX.class);
//    }
}
