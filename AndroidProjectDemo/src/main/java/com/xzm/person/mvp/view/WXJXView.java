package com.xzm.person.mvp.view;


import com.xzm.person.ui.bean.WXJX;

/**
 * Created by Administrator on 2016/8/5.
 */
public interface WXJXView {
    void getWXJXSuccess(WXJX wxjx);

    void getWXJXfailed();


    void getWXJXRefreshSuccess(WXJX wxjx);

    void getWXJXRefreshfailed();


    void getWXJXLoadMoreSuccess(WXJX wxjx);

    void getWXJXLoadMorefailed();

}
