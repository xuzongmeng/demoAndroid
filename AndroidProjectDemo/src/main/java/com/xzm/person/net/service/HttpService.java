package com.xzm.person.net.service;
import com.xzm.person.ui.bean.HttpWxjx;
import com.xzm.person.ui.bean.WXJX;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observable;

//import io.reactivex.Observable;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.schedulers.Schedulers;
/**
 * Created by xzm on 2017/2/16.
 */

public interface HttpService {

    // http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20
  /*-----------------------------带HttpResult包裹层---------------------------------*/
    @GET("weixin/query")
    Observable<HttpResult<HttpWxjx>> getHttpWxjxDate(@Query("key") String key, @Query("pno") int
            pno,
                                                     @Query("ps") int ps);

    @FormUrlEncoded
    @POST("weixin/query")
    Observable<HttpResult<HttpWxjx>> postHttpWxjxDate(@Field("key") String key, @Field("pno") int
            pno,
                                                      @Field("ps") int ps);


    /*-----------------------------不携带HttpResult包裹层---------------------------------*/
    @GET("weixin/query")
    Observable<WXJX> getWxjx(@Query("key") String key, @Query("pno") int pno,
                             @Query("ps") int ps);

    @FormUrlEncoded
    @POST("weixin/query")
    Observable<WXJX> postWxjx(@Field("key") String key, @Field("pno") int pno,
                              @Field("ps") int ps);
}
