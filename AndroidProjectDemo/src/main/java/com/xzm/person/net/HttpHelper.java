package com.xzm.person.net;


import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.xzm.person.app.Constant;

;


public class HttpHelper {
    /**
     * @param url
     * @param callback
     * @param T
     */
    public static  <T extends Object> void doGet(String url, final RequestCallback<T> callback, final Class<T> T) {
        HttpUtils http = new HttpUtils(Constant.connTimeout);
        http.send(HttpMethod.GET, url, new RequestCallBack<String>() {
            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (callback != null) {
                    callback.onFailure(arg1);
                }
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (callback != null) {
                    callback.onSuccess(DataHelper.json2Obj(arg0.result, T));
//					LogUtils.e(arg0.result);
                }

            }
        });

    }

    public static void doGet(String url, final RequestCallback<String> callback) {
        HttpUtils http = new HttpUtils(Constant.connTimeout);
        http.send(HttpMethod.GET, url, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (callback != null) {
                    callback.onFailure(arg1);
                }
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (callback != null) {
                    callback.onSuccess(arg0.result);
                }
            }

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                // TODO Auto-generated method stub
                super.onLoading(total, current, isUploading);
            }
        });
    }

    /**
     * @param url
     * @param callback
     * @param T
     */
    public static <T extends Object> void doPost(String url, final RequestCallback<T> callback, final Class<T> T) {
        HttpUtils http = new HttpUtils(Constant.connTimeout);
        http.send(HttpMethod.POST, url, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (callback != null) {
                    callback.onFailure(arg1);
                }
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (callback != null) {
                    callback.onSuccess(DataHelper.json2Obj(arg0.result, T));
                }
            }
        });
    }

    public interface RequestCallback<T extends Object> {

        void onSuccess(T t);

        void onFailure(String errorStr);
    }

}
