package com.xzm.person.net.subscriber;

/**
 * Created by xuzonmeng on 2016/9/9.
 */
public interface SubscriberOnNextListener<T> {
    void onNext(T t);
}
