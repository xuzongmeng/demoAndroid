package com.xzm.person.net;


import com.google.gson.Gson;

/**
 * Created by xuzongmeng on 2016/4/23.
 */
public class DataHelper {

    /**
     * 解析json数据，将json数据转换成对象
     */
    public static <T extends Object> T json2Obj(String json, Class<T> clazz) {
        if (json == null || json.equals("")) return null;
        Gson gson = new Gson();
        T t = (T) gson.fromJson(json, clazz);
        return t;
    }

//
//    public  static   <T extends Object> T jsonToBean(String json, Class<T> tClass){
//        if (json!=null||!"".equals(tClass)){
//            Gson gson=new Gson();
//            T t = (T)gson.fromJson(json, tClass);
//            return  t;
//
//        }
//        return null;
//    }
}
