package com.xzm.person.net.subscriber;

import android.widget.Toast;

import com.socks.library.KLog;
import com.xzm.person.app.Myapplication;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import rx.Subscriber;

/**
 * Created by xzm on 2016/9/9.
 */
public class OnNextSubscriber<T> extends Subscriber<T> {
    private SubscriberOnNextListener mSubscriberOnNextListener;

    public OnNextSubscriber(SubscriberOnNextListener mSubscriberOnNextListener) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            Toast.makeText(Myapplication.getContext(), "网络中断，请检查您的网络状态", Toast.LENGTH_SHORT).show();
        } else if (e instanceof ConnectException) {
            Toast.makeText(Myapplication.getContext(), "网络中断，请检查您的网络状态", Toast.LENGTH_SHORT).show();
        } else {
            KLog.d("Rxjava=请求微信精选数据="+e.getMessage());
            Toast.makeText(Myapplication.getContext(), "error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        if (mSubscriberOnNextListener != null) {
//            mSubscriberOnNextListener.onError(e);
        }
    }
@SuppressWarnings("unchecked")
    @Override
    public void onNext(T t) {
        if (mSubscriberOnNextListener != null) {
            mSubscriberOnNextListener.onNext(t);
        }
    }

    @Override
    public void onStart() {
//        if (!NetworkUtil.isNetworkAvailable(UIUtils.getContext())) {
//            onError(new ApiException("无网络连接", "0"));
//        }
    }
}
