package com.xzm.person.net.subscriber;

/**
 * Created by xzm on 2017/2/17.
 */

import android.content.Context;
import android.widget.Toast;

import com.socks.library.KLog;
import com.xzm.person.net.progress.ProgressCancelListener;
import com.xzm.person.ui.widget1.SuperDialog;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import rx.Subscriber;


/**
 * 用于在Http请求开始时，自动显示一个ProgressDialog
 * 在Http请求结束是，关闭ProgressDialog
 * 调用者自己对请求数据进行处理
 * Created by xuzongmeng on 2016/9/10.
 */
public class SuperProgressSubscriber<T> extends Subscriber<T> implements ProgressCancelListener {

    private SubscriberOnNextListener mSubscriberOnNextListener;

    private Context mContext;
    private SuperDialog mSuperDialog;
    public SuperProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context
            context, String hintMsg) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
        this.mContext = context;
        mSuperDialog = new SuperDialog(context).builder();
        mSuperDialog.setHintMsg(hintMsg);

    }

    private void showProgressDialog() {
        mSuperDialog.show();
    }

    private void dismissProgressDialog() {

        mSuperDialog.disMiss();
        if (mContext!=null){
            mContext=null;
        }
    }

    /**
     * 订阅开始时调用
     * 显示ProgressDialog
     */
    @Override
    public void onStart() {
        showProgressDialog();
        KLog.d("Rxjava=请求微信精选数据=开始开始开始");
    }

    /**
     * 完成，隐藏ProgressDialog
     */
    @Override
    public void onCompleted() {
        dismissProgressDialog();
        KLog.d("Rxjava=请求微信精选数据=完成完成完成");
//        Toast.makeText(context, "完成请求...", Toast.LENGTH_SHORT).show();
    }

    /**
     * 对错误进行统一处理
     * 隐藏ProgressDialog
     *
     * @param e
     */
    @Override
    public void onError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            Toast.makeText(mContext, "网络中断，请检查您的网络状态", Toast.LENGTH_SHORT).show();
        } else if (e instanceof ConnectException) {
            Toast.makeText(mContext, "网络中断，请检查您的网络状态", Toast.LENGTH_SHORT).show();
        } else {
            KLog.d("Rxjava=请求微信精选数据="+e.getMessage());
            Toast.makeText(mContext, "error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        dismissProgressDialog();

    }

    /**
     * 将onNext方法中的返回结果交给Activity或Fragment自己处理
     *
     * @param t 创建Subscriber时的泛型类型
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onNext(T t) {
        if (mSubscriberOnNextListener != null) {
            mSubscriberOnNextListener.onNext(t);
        }
    }

    /**
     * 取消ProgressDialog的时候，取消对observable的订阅，同时也取消了http请求
     */
    @Override
    public void onCancelProgress() {
        if (!this.isUnsubscribed()) {
            this.unsubscribe();
        }
    }
}