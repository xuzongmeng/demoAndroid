package com.xzm.person.net.http;

import com.xzm.person.app.APPConfig;
import com.xzm.person.net.service.HttpService;
import com.xzm.person.ui.bean.HttpWxjx;
import com.xzm.person.ui.bean.WXJX;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

//import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by xuzongmeng on 16/9/9.
 */
public class HttpMethods {

//    public static final String BASE_URL = "https://api.douban.com/v2/movie/";

    public static final String BASE_URL = "http://v.juhe.cn/";
    private static final int DEFAULT_TIMEOUT = 10;

    private Retrofit retrofit;
    private HttpService service;

    //构造方法私有
    private HttpMethods() {
        //手动创建一个OkHttpClient并设置超时时间
//        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        retrofit = new Retrofit.Builder()
                .client(builderOkHttpClient())
                //modify by zqikai 20160317 for 对http请求结果进行统一的预处理 GosnResponseBodyConvert
             .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(ResponseConvertFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        service = retrofit.create(HttpService.class);
    }

    /**
     * 初始化OkHttp
     */
    private OkHttpClient builderOkHttpClient() {
        //创建一个OkHttpClient
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        /*
         添加httpLog拦截器
         */
        if (APPConfig.DEBUG) {

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            //设置 Debug Log 模式
            httpClientBuilder.addInterceptor(loggingInterceptor);
        }

        /*
          设置超时和重连
         */
        httpClientBuilder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        return httpClientBuilder.build();
    }
    //在访问HttpMethods时创建单例
    private static class SingletonHolder{
        private static final HttpMethods INSTANCE = new HttpMethods();
    }
    //获取单例
    public static HttpMethods getInstance(){
        return SingletonHolder.INSTANCE;
    }
    private <T> void toSubscribe(Observable<T> o, Subscriber<T> s){
        o.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s);
    }


     /*-----------------------------带HttpResult包裹层---------------------------------*/
    /**
     * Get请求数据 携带htttResult统一处理层
     */
    @SuppressWarnings("unchecked")
    public void getHttpWxjx(Subscriber<HttpWxjx> subscriber, String key, int pno, int ps){

        Observable observable = service.getHttpWxjxDate(key,pno, ps).map(new HttpResultFunc<>());
        toSubscribe(observable, subscriber);
    }

    /**
     * Post请求数据 携带htttResult统一处理层
     */
    @SuppressWarnings("unchecked")
    public void postHttpWxjx(Subscriber<HttpWxjx> subscriber, String key, int pno, int ps){
        Observable observable = service.postHttpWxjxDate(key,pno, ps).map(new HttpResultFunc<>());
        toSubscribe(observable, subscriber);
    }


    /*-----------------------------不携带HttpResult包裹层---------------------------------*/


    /**
     * Get请求数据
     */
    public void getWxjx(Subscriber<WXJX> subscriber, String key, int pno, int ps){
        Observable<WXJX> observable = service.getWxjx(key,pno, ps);
        toSubscribe(observable, subscriber);
    }

    /**
     * Post请求数据
     */

    public void postWxjx(Subscriber<WXJX> subscriber, String key, int pno, int ps){
        Observable<WXJX> observable = service.postWxjx(key,pno, ps);
        toSubscribe(observable, subscriber);
    }

}
