package com.xzm.person.net.http;

/**
 * Created by xzm on 2017/2/17.
 */

import com.socks.library.KLog;
import com.xzm.person.net.service.HttpResult;

import rx.functions.Func1;

/**
 * 用来统一处理Http的resultCode,并将HttpResult的Data部分剥离出来返回给subscriber
 *
 * @param <T>   Subscriber真正需要的数据类型，也就是Data部分的数据类型
 */
public class HttpResultFunc<T> implements Func1<HttpResult<T>, T> {

    @Override
    public T call(HttpResult<T> httpResult) {
        KLog.d("==okhttp==" + httpResult.toString());
        if (httpResult.getError_code()!= 0) {
            KLog.d("Rxjava===HttpResultFunc=结果"+httpResult.getResultcode());
            KLog.d("Rxjava===HttpResultFunc=错误"+httpResult.getError_code());
            throw new ApiException(httpResult.getError_code());
        }
        return httpResult.getResult();
    }
}