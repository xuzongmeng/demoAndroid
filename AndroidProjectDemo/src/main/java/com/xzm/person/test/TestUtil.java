//package com.xzm.person.test;
//import com.xzm.person.ui.bean.Employee;
//import com.xzm.person.ui.bean.EmployeeComparator;
//import com.xzm.person.ui.bean.People;
//import com.xzm.person.ui.bean.PeopleComparator;
//import com.xzm.person.utils.ModelUtil;
//
////import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
///**
// * Created by xuzongmeng on 2017/8/22.
// */
//
//public class TestUtil {
//    private  void  testPeople(){
//        List<People> adList = new ArrayList<>();
//        adList.add(new People("北京",14,"2001-09-18","白羊座"));
//        adList.add(new People("上海",21,"1995-09-25","金牛座"));
//        adList.add(new People("广州",31,"1985-09-25","双子座"));
//        adList.add(new People("深圳",41,"1975-09-25","巨蟹座"));
//
//
//        adList.add(new People("北京",15,"2000-09-18","白羊座"));
//        adList.add(new People("上海",22,"1994-09-25","金牛座"));
//        adList.add(new People("广州",32,"1984-09-25","双子座"));
//        adList.add(new People("深圳",42,"1974-09-25","巨蟹座"));
//
//        adList.add(new People("北京",16,"1999-09-18","白羊座"));
//        adList.add(new People("上海",23,"1993-09-25","金牛座"));
//        adList.add(new People("广州",33,"1983-09-25","双子座"));
//        adList.add(new People("深圳",43,"1973-09-25","巨蟹座"));
//
//
//        adList.add(new People("北京",17,"1998-09-18","白羊座"));
//        adList.add(new People("上海",24,"1992-09-25","金牛座"));
//        adList.add(new People("广州",34,"1982-09-25","双子座"));
//        adList.add(new People("深圳",44,"1972-09-25","巨蟹座"));
//    }
//
//    @Test
//    public  void  testFunction(){
//        List<People> peopleData = ModelUtil.getPeopleData();
//        PeopleComparator peopleComparator =new PeopleComparator();
//        Collections.sort(peopleData, peopleComparator);//用我们写好的Comparator对user进行排序
//        for(int i=0;i<peopleData.size();i++){
//            System.out.println(peopleData.get(i).toString());
//        }
//
////        Collections.sort(peopleData);//用我们写好的Comparator对user进行排序
////        for(int i=0;i<peopleData.size();i++){
////            System.out.println(peopleData.get(i).toString());
////        }
//    }
//    @Test
//    public  void  testEmpl(){
//        List<Employee> employeeList = new ArrayList<Employee>();
//        employeeList.add(new Employee(1, 9, 10000, 10));
//        employeeList.add(new Employee(2, 9, 12000, 7));
//        employeeList.add(new Employee(3, 5, 10000, 12));
//        employeeList.add(new Employee(4, 5, 10000, 6));
//        employeeList.add(new Employee(5, 3, 5000, 3));
//        employeeList.add(new Employee(6, 1, 2500, 1));
//        employeeList.add(new Employee(7, 5, 8000, 10));
//        employeeList.add(new Employee(8, 3, 8000, 2));
//        employeeList.add(new Employee(9, 1, 3000, 5));
//        employeeList.add(new Employee(10, 1, 2500, 4));
//        employeeList.add(new Employee(11, 2, 2000, 4));
//
//        Comparator<Employee> comparator = new EmployeeComparator();
//        Collections.sort(employeeList, comparator);
//        System.out.println("ID\tLevel\tSalary\tYears");
//        System.out.println("=============================");
//        for (Employee employee : employeeList) {
//            System.out.printf("%d\t%d\t%d\t%d\n", employee.getId(), employee.getLevel(), employee.getSalary(), employee.getYear());
//        }
//        System.out.println("=============================");
//    }
//
//
//    @Test
//    public   void  testTime(){
//        List<String> list =new ArrayList<String>();
////        list.add("2014-09-04 10:34:41");
////        list.add("2013-08-04 13:42:19");
////        list.add("2014-09-04 16:46:49");
////        list.add("2010-01-04 12:32:00");
////        list.add("2004-04-04 10:34:41");
////        list.add("2009-05-14 23:42:19");
////        list.add("2014-12-04 06:08:49");
////        list.add("2010-01-24 01:32:00");
//
//        list.add("2014-09-04");
//        list.add("2013-08-04");
//        list.add("2014-09-04");
//        list.add("2010-01-04");
//        list.add("2004-04-04");
//        list.add("2009-05-14");
//        list.add("2014-12-04");
//        list.add("2010-01-24");
//
//        Collections.sort(list);
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }
//
//        Collections.reverse(list);
//
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println("==="+list.get(i));
//        }
//
//    }
//
//    public   void  test(){
//
//    }
//
//}
//
