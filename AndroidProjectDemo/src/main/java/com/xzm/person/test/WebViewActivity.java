//package com.xzm.person.test;
//
//import android.os.Build;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//
//import android.view.KeyEvent;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.widget.Toast;
//
//import com.xzm.person.R;
//
//
//public class WebViewActivity extends AppCompatActivity {
//    //    public static final String mURL="http://local.idoupiao
//    // .com:4888/iabcjiangsufilmpre/?openid=oxmudxLXzqKWTSnZ8ikaJscicl-Y&signcode
//    // =23b4044b11af85609d76089aea44a924#/mineOrder";
//    private WebView webView;
//    //    public static final String mURL="http://180.169.91.75:9060/i-pufa-film/#/Home";
////    public static final String mURL = "http://local.idoupiao.com:9060/i-pufa-film/index" +
////            ".html#/bills";
//
//
//    public static String mURL = "http://192.168.56.1:8085/?openid=oxmudxLXzqKWTSnZ8ikaJscicl-Y" +
//            "&signcode=23b4044b11af85609d76089aea44a924#/Home?type=1";
//
//   public static String WEB_URL="WEB_URL";
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_web_view);
//        initView();
//        initData();
//    }
//
//    private void initView() {
//        webView = findViewById(R.id.web_view);
//    }
//
//    private void initData() {
//        WebSettings settings = webView.getSettings();
//// 缓存(cache)
//        settings.setAppCacheEnabled(true);      // 默认值 false
//        settings.setAppCachePath(getCacheDir().getAbsolutePath());
//// 存储(storage)
//        settings.setDomStorageEnabled(true);    // 默认值 false
//        settings.setDatabaseEnabled(true);      // 默认值 false
//
//// 是否支持viewport属性，默认值 false
//// 页面通过`<meta name="viewport" ... />`自适应手机屏幕
//        settings.setUseWideViewPort(true);
//// 是否使用overview mode加载页面，默认值 false
//// 当页面宽度大于WebView宽度时，缩小使页面宽度等于WebView宽度
//        settings.setLoadWithOverviewMode(true);
//// 是否支持Javascript，默认值false
//        settings.setJavaScriptEnabled(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
//            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        }
////        if (DeviceUtil.isConnectNet(this)) {
////            // 根据cache-control决定是否从网络上取数据
////            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
////        } else {
////            // 没网，离线加载，优先加载缓存(即使已经过期)
////            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
////        }
//
//        webView.loadUrl(mURL);
//    }
//
//    @Override
//    public void onBackPressed() {
//        webView.goBack();
//        super.onBackPressed();
//    }
//
//    private long myTime;
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (System.currentTimeMillis() - myTime > 2000) {
//                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
//                myTime = System.currentTimeMillis();
//            } else {
////                MinstoneApplication.getInstance().exit();
//                return false;
//            }
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//}
