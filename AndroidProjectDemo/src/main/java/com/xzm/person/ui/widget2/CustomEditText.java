package com.xzm.person.ui.widget2;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xzm.person.R;


/**
 * Created by xzm on 2017/3/31.
 */

public class CustomEditText extends LinearLayout {

    private String mText;
    private String mHintText;
    private int mHintTextColor;
    private int mTextColor;
    private int mDrawable;




    private LinearLayout llCustomItemLayoutDrawableContainer;
    private ImageView ivCustomItemLayoutDrawable;
    private TextView tvCustomItemLayoutText;
    private TextView tvCustomItemLayoutHintNews;
    private View viewCustomItemLayoutBottomLine;
    private LinearLayout llBottomLineContainer;


    public CustomEditText(Context context) {
        this(context, null);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable
                .CustomEditText);

        if (typeArray != null) {
            mTextColor = typeArray.getColor(R.styleable.CustomEditText_text_color, Color.BLUE);
            mText = typeArray.getString(R.styleable.CustomEditText_text);
            mHintTextColor = typeArray.getColor(R.styleable.CustomEditText_hint_text_color, Color
                    .BLUE);
            mHintText = typeArray.getString(R.styleable.CustomEditText_hint_text);
            mText = typeArray.getString(R.styleable.CustomEditText_text);
            mDrawable = typeArray.getResourceId(R.styleable.CustomEditText_drawable, -1);


            init(context);
            typeArray.recycle();
        }


    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_custom, this, true);
        llCustomItemLayoutDrawableContainer = (LinearLayout) view.findViewById(R.id
                .ll_CustomItem_layout_drawable_container);
        ivCustomItemLayoutDrawable = (ImageView) view.findViewById(R.id
                .iv_CustomItem_layout_drawable);
        tvCustomItemLayoutText = (TextView) view.findViewById(R.id.tv_CustomItem_layout_text);
        tvCustomItemLayoutHintNews = (TextView) view.findViewById(R.id
                .tv_CustomItem_layout_hint_news);
        viewCustomItemLayoutBottomLine = view.findViewById(R.id.view_CustomItem_layout_bottom_line);
        llBottomLineContainer = (LinearLayout) view.findViewById(R.id
                .ll_CustomItem_layout_bottom_line_container);
        if (mTextColor != 0) {
            tvCustomItemLayoutText.setTextColor(mTextColor);
        }
        if (!TextUtils.isEmpty(mText)) {
            tvCustomItemLayoutText.setText(mText);
        }

        if (mDrawable != -1) {
            ivCustomItemLayoutDrawable.setImageResource(mDrawable);
        }

//        if (mDrawableVisible) {
//            llCustomItemLayoutDrawableContainer.setVisibility(View.VISIBLE);
//        } else {
//            llCustomItemLayoutDrawableContainer.setVisibility(View.GONE);
//        }
//        if (mTextHinNewsVisible) {
//            tvCustomItemLayoutHintNews.setVisibility(View.VISIBLE);
//        } else {
//            tvCustomItemLayoutHintNews.setVisibility(View.GONE);
//        }

//        if (mViewBottomLineColor != 0) {
//            viewCustomItemLayoutBottomLine.setBackgroundColor(mViewBottomLineColor);
//        }

//        if (mViewBottomLineVisible) {
//
//            llBottomLineContainer.setVisibility(View.VISIBLE);
////            viewCustomItemLayoutBottomLine.setVisibility(View.VISIBLE);
//        } else {
////            viewCustomItemLayoutBottomLine.setVisibility(View.GONE);
//            llBottomLineContainer.setVisibility(View.GONE);
//        }


    }
}
