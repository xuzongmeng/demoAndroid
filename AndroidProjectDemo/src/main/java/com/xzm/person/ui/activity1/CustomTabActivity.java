package com.xzm.person.ui.activity1;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.flyco.tablayout.widget.MsgView;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.bean.TabEntity;
import com.xzm.person.ui.fragment1.FragmentHome;
import com.xzm.person.ui.widget1.BadgeFactory;
import com.xzm.person.ui.widget1.BadgeView;
import java.util.ArrayList;
/**
 * Created by xuzongmeng on 2016/9/12.
 * 自定义顶部按钮
 */
public class CustomTabActivity extends FragmentActivity {
    private Context mContext = this;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private ArrayList<Fragment> mFragments2 = new ArrayList<>();
    private CommonTabLayout mTabLayout_2;
    private CommonTabLayout mTabLayout_3;
    private ViewPager mViewPager;
    private String[] mTitles = {"首页", "消息", "联系人", "更多"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private int[] mIconUnselectIds = {
            R.mipmap.tab_home_unselect, R.mipmap.tab_speech_unselect,
            R.mipmap.tab_contact_unselect, R.mipmap.tab_more_unselect};
    private int[] mIconSelectIds = {
            R.mipmap.tab_home_select, R.mipmap.tab_speech_select,
            R.mipmap.tab_contact_select, R.mipmap.tab_more_select};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customtab);

        mViewPager = (ViewPager) findViewById(R.id.vp_2);
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(myPagerAdapter);
        mTabLayout_2 = (CommonTabLayout) findViewById(R.id.tl_2);
        mTabLayout_3 = (CommonTabLayout) findViewById(R.id.tl_3);
//        MsgView view = mTabLayout_2.getMsgView(0);
//        mTabLayout_2.setBackgroundColor(ThemeUtil.getColor(R.color.rect));
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }



//
//        BadgeFactory.create(this)
//                .setTextColor(Color.WHITE)
//                .setWidthAndHeight(25,25)
//                .setBadgeBackground(Color.RED)
//                .setTextSize(10)
//                .setBadgeGravity(Gravity.RIGHT|Gravity.TOP)
//                .setBadgeCount(20)
//                .setShape(BadgeView.SHAPE_CIRCLE)
//                .setMargin(0,0,5,0)
//                .bind(view);

//
//
//        BadgeFactory.createDot(this).setBadgeCount(20).bind(imageView);
//        BadgeFactory.createCircle(this).setBadgeCount(20).bind(imageView);
//        BadgeFactory.createRectangle(this).setBadgeCount(20).bind(imageView);
//        BadgeFactory.createOval(this).setBadgeCount(20).bind(imageView);
//        BadgeFactory.createSquare(this).setBadgeCount(20).bind(imageView);
//        BadgeFactory.createRoundRect(this).setBadgeCount(20).bind(imageView);

        mFragments.add(creatFragment("第一种A"));
        mFragments.add(creatFragment("第一种B"));
        mFragments.add(creatFragment("第一种C"));
        mFragments.add(creatFragment("第一种D"));



        mFragments2.add(creatFragment("第二种A"));
        mFragments2.add(creatFragment("第二种B"));
        mFragments2.add(creatFragment("第二种C"));
        mFragments2.add(creatFragment("第二种D"));

        mTabLayout_2.setTabData(mTabEntities);
        mTabLayout_3.setTabData(mTabEntities, this, R.id.fl_change, mFragments2);
        Log.d("TAG==","大小==="+mFragments2.size());




        myPagerAdapter.notifyDataSetChanged();


        MsgView view = mTabLayout_2.getMsgView(0);

        MsgView view1 = mTabLayout_2.getMsgView(1);

        //
            BadgeFactory.create(CustomTabActivity.this)
                    .setTextColor(Color.WHITE)
                    .setWidthAndHeight(15,15)
                    .setBadgeBackground(Color.RED)
                    .setTextSize(10)
                    .setBadgeGravity(Gravity.RIGHT| Gravity.TOP)
                    .setBadgeCount(20)
                    .setShape(BadgeView.SHAPE_CIRCLE)
                    .setMargin(0,0,5,0)
                    .bind(view);


        BadgeFactory.create(CustomTabActivity.this)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(15,15)
                .setBadgeBackground(Color.RED)
                .setTextSize(10)
                .setBadgeGravity(Gravity.RIGHT| Gravity.TOP)
                .setBadgeCount(20)
                .setShape(BadgeView.SHAPE_CIRCLE)
                .setMargin(0,0,5,0)
                .bind(view1);


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout_2.setCurrentTab(position);


                //


//                replaceFragment(mFragments2.get(position));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabLayout_2.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                KLog.d("切换第一种Tab到111===" + position);
                mViewPager.setCurrentItem(position, false);
//                Fragment fragment = creatFragment("这里是"+position);
//
//
//                replaceFragment(fragment);
            }

            @Override
            public void onTabReselect(int position) {
                KLog.d("切换第一种Tab到222===" + position);
            }
        });

        mTabLayout_3.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                KLog.d("切换第二种Tab到222===" + position);
                replaceFragment(mFragments2.get(position));
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }


    public Fragment creatFragment(String content) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        fragmentHome.setArguments(bundle);
        return fragmentHome;
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fg = getSupportFragmentManager();
        FragmentTransaction ft = fg.beginTransaction();
        ft.replace(R.id.fl_change, fragment);
        ft.commit();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
