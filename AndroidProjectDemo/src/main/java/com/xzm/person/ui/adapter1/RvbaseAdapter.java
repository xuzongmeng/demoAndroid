package com.xzm.person.ui.adapter1;

import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xzm.person.R;
import com.xzm.person.app.Myapplication;
import com.xzm.person.ui.bean.RecycleViewBean;
import com.xzm.person.ui.widget1.SwipeMenuView;
import com.xzm.person.utils.ToastUtil;

import java.util.List;

/**
 * Created by xuzongmeng on 2017/1/13.
 */

public class RvbaseAdapter extends BaseQuickAdapter<RecycleViewBean,BaseViewHolder> {


    public RvbaseAdapter(int layoutResId, List<RecycleViewBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(final BaseViewHolder holder, final RecycleViewBean bean) {
        holder.setText(R.id.tv_recyleview,bean.getText());

        ((SwipeMenuView) holder.itemView).setIos(true).setLeftSwipe(holder.getAdapterPosition() % 2
                == 0 ? true :
                false).setSwipeEnable(true);
         holder.getView(R.id.tv_recyleview).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 ToastUtil.showToast("点击条目上的控件"+bean.getText());
             }
         });

        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                ToastUtil.showToast("点击是整个条目"+position+"位置");
            }
        });
        holder.getView(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Myapplication.getContext(), "侧滑按钮点击", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
