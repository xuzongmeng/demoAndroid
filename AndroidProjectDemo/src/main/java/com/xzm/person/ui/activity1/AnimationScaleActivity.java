package com.xzm.person.ui.activity1;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.xzm.person.R;

/**
 * Created by xuzongmeng on 2016/9/13.
 * 缩放变换动画
 */
public class AnimationScaleActivity extends AppCompatActivity {

    private ImageView ivScale1;
    private ImageView ivScale2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale_animation);
        ivScale1 = (ImageView) findViewById(R.id.iv_scale1);
        ivScale2 = (ImageView) findViewById(R.id.iv_scale2);
        init();
    }





    private void init() {
        //在垂直方向上放大3倍再还原，就可以这样写：
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(ivScale1, "scaleY", 1f, 3f, 1f);
        animator1.setDuration(5000);
        animator1.start();


        ObjectAnimator animator2 = ObjectAnimator.ofFloat(ivScale2, "scaleX", 1f, 3f, 1f);
        animator2.setDuration(5000);
        animator2.start();
    }

}
