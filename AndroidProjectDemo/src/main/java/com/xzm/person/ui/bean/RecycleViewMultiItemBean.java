package com.xzm.person.ui.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * Created by xuzongmeng on 2016/10/10.
 */

public class RecycleViewMultiItemBean implements MultiItemEntity {
    private String user_name;
    private String phone_number;
    private String user_QQ;
    private  int  status;

    public RecycleViewMultiItemBean(String user_name, String phone_number, String user_QQ, int status) {
        this.user_name = user_name;
        this.phone_number = phone_number;
        this.user_QQ = user_QQ;
        this.status = status;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getUser_QQ() {
        return user_QQ;
    }

    public void setUser_QQ(String user_QQ) {
        this.user_QQ = user_QQ;
    }



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int getItemType() {
        return status;
    }
}
