package com.xzm.person.ui.activity1;

import android.app.Activity;
import android.os.Bundle;

import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.common.WheelData;
import com.wx.wheelview.widget.WheelView;
import com.xzm.person.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by xuzongmeng on 2016/8/24.
 */
@SuppressWarnings("unchecked")
public class XWheelViewActivity extends Activity {

    WheelView mainWheelView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xwheel);
        init();
//        final XWheelView picker= (XWheelView) this.findViewById(R.id.wheel);
//        picker.setLineColor(Color.parseColor("#F30036"));
//        picker.addData("轰趴馆");
//        picker.addData("台球");
//        picker.addData("密室逃脱");
//        picker.addData("卡丁车");
//        picker.addData("桌游");
//        picker.addData("真人CS");
//        picker.addData("DIY");
//        picker.setCenterItem(4);

//        WPopupWindow popupWindow=new WPopupWindow(wh);
//        popupWindow.showAtLocation(getContentView(), Gravity.BOTTOM, 0, 0);
//        findViewById(R.id.right).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LogUtils.e("nowData->"+picker.getCenterItem());
//            }
//        });
    }

    private void init() {
        mainWheelView = (WheelView) findViewById(R.id.main_wheelview);
        mainWheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        mainWheelView.setSkin(WheelView.Skin.Holo);
        mainWheelView.setWheelData(createMainDatas());
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.selectedTextSize = 20;
        style.textSize = 16;
        mainWheelView.setStyle(style);
        mainWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(int i, Object o) {

            }
        }) ;

    }

    private List<String> createMainDatas() {
        String[] strings = {"黑龙江", "吉林", "辽宁"};
        return Arrays.asList(strings);
    }

    private ArrayList<WheelData> createDatas() {
        ArrayList<WheelData> list = new ArrayList<WheelData>();
        WheelData item;
        for (int i = 0; i < 20; i++) {
            item = new WheelData();
            item.setId(R.mipmap.ic_launcher);
            item.setName((i < 10) ? ("0" + i) : ("" + i));
            list.add(item);
        }
        return list;
    }
}
