package com.xzm.person.ui.widget1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.xzm.person.R;

/**
 * Created by xuzongmeng on 2016/10/25.
 * 暂无数据
 */

public class ViewZanWuShuJu {
    private Context mContext;
    private View zanWuShuJuView;

    public ViewZanWuShuJu(Context context) {
        this.mContext = context;
        initViews();
    }

    /**
     *初始化界面
     */
    private void initViews() {
        zanWuShuJuView = LayoutInflater.from(mContext).inflate(R.layout.view_zanwu_shuju, null);
    }

    public View getZanWuShuJuView(){
        return  zanWuShuJuView;
    }
}
