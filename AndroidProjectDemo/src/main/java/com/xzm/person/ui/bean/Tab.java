package com.xzm.person.ui.bean;

/**
 * Created by Administrator on 2016/9/23 0023.
 */

public class Tab {
//    public Drawable image;
//
//    public Drawable getImage() {
//        return image;
//    }
//
//    public void setImage(Drawable image) {
//        this.image = image;
//    }
//
//    public Tab(Drawable image) {
//        this.image = image;
//    }


    private int image;
    private String text;

    public Tab(int image, String text) {
        this.image = image;
        this.text = text;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


}
