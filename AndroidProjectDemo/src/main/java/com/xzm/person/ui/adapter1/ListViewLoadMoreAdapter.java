package com.xzm.person.ui.adapter1;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xzm.person.R;
import com.xzm.person.ui.bean.TestBean;

import java.util.ArrayList;


public class ListViewLoadMoreAdapter extends
		ArrayListAdapter<TestBean> {

	public ArrayList<TestBean> mlist;
	public Activity mContext;

	public ListViewLoadMoreAdapter(Activity context,
								   ArrayList<TestBean> data) {
		super(context, data);
		mContext = context;
		mList = data;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = mContext.getLayoutInflater();
			convertView = inflater.inflate(R.layout.item_wxjx, null);
			holder.tv_wxjx_id = (TextView) convertView
					.findViewById(R.id.tv_wxjx_id);

			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		TestBean result = mList.get(position);

		return convertView;
	}

	class ViewHolder {
		public TextView tv_wxjx_id;

	}
}
