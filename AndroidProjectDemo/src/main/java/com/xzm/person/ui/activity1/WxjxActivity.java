package com.xzm.person.ui.activity1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.mvp.presenter.WXJXPresenterImp;
import com.xzm.person.mvp.view.WXJXView;
import com.xzm.person.ui.adapter1.WxjxRecyAdapter;
import com.xzm.person.ui.bean.WXJX;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by xuzongmeng on 2016/8/26.
 */
public class WxjxActivity extends AppCompatActivity implements WXJXView {
    private RecyclerView mWxjx_recyclerview;
    private List<WXJX.ResultBean.ListBean> mWxjxList;
    private WxjxRecyAdapter mWxjxRecyAdapter;
    private WXJXPresenterImp mWxjxPresenterImp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxjx);
        mWxjx_recyclerview = (RecyclerView) this.findViewById(R.id.wxjx_recyclerview);
        init();
        initHttp();
    }

    private void initHttp() {
//        showLoading();
        //        showAnimationLoading();

        mWxjxPresenterImp = new WXJXPresenterImp(WxjxActivity.this);
        mWxjxPresenterImp.getWXJX();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 1000);
    }



    private void init() {
        mWxjxList = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager
                .VERTICAL,false);

//        mWxjx_recyclerview.addItemDecoration(new LinerLayoutDecoration(this, LinearLayoutManager.VERTICAL));


//     mWxjx_recyclerview.addItemDecoration(new RecycleViewDecoration(this,RecycleViewDecoration.VERTICAL_LIST));
//        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        mWxjx_recyclerview.setLayoutManager(layoutManager);

        mWxjxRecyAdapter = new WxjxRecyAdapter(this,R.layout.item_wxjx);
        mWxjx_recyclerview.setAdapter(mWxjxRecyAdapter);
        mWxjxRecyAdapter.addAll(mWxjxList);

//        mWxjx_refresh_layout.setLoadMore(true);
//        mWxjx_refresh_layout.autoRefresh();
//        MaterialLoadingView  materialLoadingView=new MaterialLoadingView(this);
//        mWxjx_refresh_layout.setHeader(materialLoadingView);
//        mWxjx_refresh_layout.setMaterialRefreshListener(new MaterialRefreshListener() {
//            @Override
//            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
//                mWxjxPresenterImp.refreshWXJX();
//            }
//
//            @Override
//            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
//                mWxjxPresenterImp.loadMoreWXJX();
//            }
//        });


    }

    /**
     * 自动刷新
     */
    @Override
    protected void onStart() {
        super.onStart();
//        mWxjx_refresh_layout.autoRefresh();
    }

    /**
     * 设置数据
     */
    private void setwxjxDate(WXJX wxjx) {

        mWxjxList.addAll(wxjx.getResult().getList());
        mWxjxRecyAdapter.notifyDataSetChanged();


    }

    @Override
    public void getWXJXSuccess(WXJX wxjx) {
//        showContent();
        setwxjxDate(wxjx);
        KLog.d("MVP微信精选", wxjx.getResult().getList().size());
    }

    @Override
    public void getWXJXfailed() {
        KLog.d("MVP微信精选失败.......");
    }

    @Override
    public void getWXJXRefreshSuccess(WXJX wxjx) {
        KLog.d("MVP微信精选下拉刷新成功......." + wxjx.getResult().getList().get(0).getTitle());
//        mWxjx_refresh_layout.finishRefresh();
        mWxjxList.clear();
        mWxjxList.addAll(wxjx.getResult().getList());
        mWxjxRecyAdapter.notifyDataSetChanged();
    }

    @Override
    public void getWXJXRefreshfailed() {
        KLog.d("MVP微信精选下拉刷失败.......");
    }

    @Override
    public void getWXJXLoadMoreSuccess(WXJX wxjx) {
        mWxjxList.addAll(wxjx.getResult().getList());
//        mWxjx_refresh_layout.finishRefreshLoadMore();
        mWxjxRecyAdapter.notifyDataSetChanged();
    }

    @Override
    public void getWXJXLoadMorefailed() {
        KLog.d("MVP微信精选加载更多失败.......");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWxjxPresenterImp != null) {
            mWxjxPresenterImp = null;
        }
    }


//    @Override
//    public void onReLoadDate() {
//        initHttp();
//    }
}
