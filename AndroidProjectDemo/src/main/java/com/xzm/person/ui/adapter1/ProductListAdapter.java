package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.bean.Product;

import java.util.List;

/**
 * Created by xuzongmeng on 2016/12/30.
 * 在该店铺买滴商品list列表
 */

public class ProductListAdapter extends RecyclerAdapter<Product.DataBean.OrderListBean.GoodsBean> {
    public ProductListAdapter(Context context, @Nullable List<Product.DataBean.OrderListBean.GoodsBean> data,
                              @NonNull int... layoutResIds) {
        super(context, data, layoutResIds);
    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, Product.DataBean.OrderListBean.GoodsBean item) {
        KLog.d("店铺里面的商品==="+item.getTv_product_original_price());
        helper.setImageResource(R.id.iv_product_img,item.getIv_product_img())
                .setText(R.id.tv_product_describe,item.getTv_product_describe())
                .setText(R.id.tv_product_value,item.getTv_product_value())
                .setText(R.id.tv_product_color,item.getTv_product_color())
                .setText(R.id.tv_product_fuli,item.getTv_product_fuli())
                .setText(R.id.tv_product_current_price,item.getTv_product_current_price())
                .setText(R.id.tv_product_original_price,item.getTv_product_original_price())
                .setText(R.id.tv_product_count,item.getTv_product_count());
    }

    /**
     ivProductImg = (ImageView) findViewById(R.id.iv_product_img);
     tvProductDescribe = (TextView) findViewById(R.id.tv_product_describe);
     tvProductValue = (TextView) findViewById(R.id.tv_product_value);
     tvProductColor = (TextView) findViewById(R.id.tv_product_color);
     tvProductFuli = (TextView) findViewById(R.id.tv_product_fuli);
     tvProductCurrentPrice = (TextView) findViewById(R.id.tv_product_current_price);
     tvProductOriginalPrice = (TextView) findViewById(R.id.tv_product_original_price);
     tvProductCount = (TextView) findViewById(R.id.tv_product_count);

     */
}
