package com.xzm.person.ui.fragment1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.activity3.ZhiHuBehaviorActivity;
import com.xzm.person.ui.adapter1.DemoItemAdapter;
import com.xzm.person.ui.bean.DemoItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentThree extends Fragment {
    TextView textView;
    private DemoItemAdapter mAdapter;
    private List<DemoItem> mlist;
    private RecyclerView recyclerViewDemoItem;

    public static FragmentThree newInstance(String text) {
        FragmentThree fragmentCommon = new FragmentThree();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_three, container, false);
//        textView= (TextView) view.findViewById(R.id.textView);
//        textView.setText(getArguments().getString("text"));
        initView(view);
        initDates();
        return view;
    }

    private void initView(View view) {
        recyclerViewDemoItem = view.findViewById(R.id.recyclerView_demo_item);
        recyclerViewDemoItem.setLayoutManager(new GridLayoutManager(getContext(), 2));
        TextView textView = view.findViewById(R.id.tv_toolbaer_title);
        textView.setText(getArguments().getString("text"));
    }

    public void initDates() {
        mlist = new ArrayList<>();
        mAdapter = new DemoItemAdapter(R.layout.item_demo, mlist);
        mAdapter.openLoadAnimation();
        recyclerViewDemoItem.setHasFixedSize(true);
        recyclerViewDemoItem.setAdapter(mAdapter);
        addBaseDate();
    }

    private void addBaseDate() {
//        mlist.add(new DemoItem(DataBingActivity.class, "MVVM双向绑定"));
//        mlist.add(new DemoItem(WebViewActivity.class, "WebView加载URL"));
        mlist.add(new DemoItem(ZhiHuBehaviorActivity.class, "模仿知乎Behavior"));
//        mlist.add(new DemoItem(WebViewActivity.class, "模仿百度Behavior"));
        KLog.d("====FragmentThree=大小=" + mlist.size());
        mAdapter.notifyDataSetChanged();
    }
}
