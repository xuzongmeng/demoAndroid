package com.xzm.person.ui.activity1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.adapter1.RecycleViewMultiItemAdapter;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;

import java.util.ArrayList;

/**
 * Created by xuzongmeng on 2016/10/10.
 */

public class RecycleViewMutilitemActivity extends AppCompatActivity {
    private RecyclerView recyclerViewMultiItem;
    private ArrayList<RecycleViewMultiItemBean> mList;
    private RecycleViewMultiItemAdapter mRecycleViewMultiItemAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycleview_mutil_item);
        recyclerViewMultiItem = (RecyclerView) findViewById(R.id.recyclerView_multi_item);
        init();
    }

    private void init() {

        mList = new ArrayList<>();
        //设置RecycleView的布局管理器
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager
                .VERTICAL, false);
        recyclerViewMultiItem.setLayoutManager(layoutManager);
//        mRecycleViewMultiItemAdapter = new RecycleViewMultiItemAdapter(this, mList,
//                R.layout.item_recycleview_multi_style1, R.layout.item_recycleview_multi_style2,
// R.layout.item_trade);


        mRecycleViewMultiItemAdapter = new RecycleViewMultiItemAdapter(this, mList,
                R.layout.item_recycleview_multi_style1, R.layout.item_recycleview_multi_style2);
        recyclerViewMultiItem.setAdapter(mRecycleViewMultiItemAdapter);


        for (int i = 0; i < 10; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("张三" + i, "1890744790" +
                    i, "110220" + i, 1);
            mList.add(bean);
        }
        for (int i = 0; i < 10; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("李四" + i, "1560744790" +
                    i, "440550" + i, 2);
            mList.add(bean);
        }
//        for (int i = 0; i < 20; i++) {
//            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("王五" + i, "1560744790" +
//                    i, "440550" + i, 3);
//            mList.add(bean);
//        }

        KLog.d("多条目===" + mList.size());
        mRecycleViewMultiItemAdapter.addAll(mList);
    }
}
