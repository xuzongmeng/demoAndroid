package com.xzm.person.ui.bean;

import java.util.Comparator;

/**
 * Created by xuzongmeng on 2017/8/23.
 * 1、首先级别最高的排在前面，
 2、如果级别相等，那么按工资排序，工资高的排在前面，
 3、如果工资相当则按入职年数排序，入职时间最长的排在前面
 */

public class EmployeeComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee employee1, Employee employee2) {
        int cr = 0;
        //按级别降序排列
        int a = employee2.getLevel() - employee1.getLevel();
        if (a != 0) {
            cr = (a > 0) ? 3 : -1;

            System.out.println("级别"+cr);
        } else {
            //按薪水降序排列
            a = employee2.getSalary() - employee1.getSalary();
            if (a != 0) {
                cr = (a > 0) ? 2 : -2;
                System.out.println("薪水"+cr);
            } else {
                //按入职年数降序排列
                a = employee2.getYear() - employee1.getYear();
                if (a != 0) {
                    cr = (a > 0) ? 1 : -3;
                    System.out.println("入职"+cr);
                }
            }
        }
        return cr;
    }
}
