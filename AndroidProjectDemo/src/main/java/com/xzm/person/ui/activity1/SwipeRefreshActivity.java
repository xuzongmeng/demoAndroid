package com.xzm.person.ui.activity1;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.mvp.presenter.WXJXPresenterImp;
import com.xzm.person.mvp.view.WXJXView;
import com.xzm.person.ui.adapter1.WxjxRecyAdapter;
import com.xzm.person.ui.bean.WXJX;
import com.xzm.person.utils.DeviceUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2016/8/17.
 * x
 */
public class SwipeRefreshActivity extends AppCompatActivity implements WXJXView {
    private SwipeRefreshLayout mWxjx_refresh_layout;
    private RecyclerView mWxjx_recyclerview;
    private List<WXJX.ResultBean.ListBean> mWxjxList;
    private WxjxRecyAdapter mWxjxRecyAdapter;
    private WXJXPresenterImp mWxjxPresenterImp;


    private void init() {
        mWxjxList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mWxjx_recyclerview.setLayoutManager(layoutManager);
        mWxjxRecyAdapter = new WxjxRecyAdapter(this,R.layout.item_wxjx);
        mWxjx_recyclerview.setAdapter(mWxjxRecyAdapter);
        mWxjxRecyAdapter.addAll(mWxjxList);

//        mWxjx_refresh_layout.setLoadMore(true);
//        mWxjx_refresh_layout.autoRefresh();
        mWxjx_refresh_layout.setOnRefreshListener(() -> {
            mWxjxPresenterImp.refreshWXJX();
            KLog.d("微信精选SwipeRefreshLayout刷新啦=======");
        });

    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_swipe_refresh);
        if (!DeviceUtil.isConnectNet(this)){
        }
        mWxjx_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.wxjx_refresh_layout);
        mWxjx_recyclerview = (RecyclerView) findViewById(R.id.wxjx_swipe_refresh_layout_recyclerviewx);
        mWxjx_refresh_layout.setColorSchemeResources(R.color.colorAccent, R.color.CadetBlue, R
                .color.colorPrimary, R.color.colorPrimaryDark, R.color.Yellow);//设置刷新投变换的颜色
        mWxjxPresenterImp = new WXJXPresenterImp(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mWxjxPresenterImp.getWXJX();
            }
        },3000);
        init();
    }
    /**
     * 设置数据
     */
    private void setwxjxDate(WXJX wxjx) {
        mWxjxList.addAll(wxjx.getResult().getList());
        mWxjxRecyAdapter.notifyDataSetChanged();
    }

    @Override
    public void getWXJXSuccess(WXJX wxjx) {
        setwxjxDate(wxjx);
        KLog.d("MVP微信精选", wxjx.getResult().getList().get(0).getTitle());
    }

    @Override
    public void getWXJXfailed() {
        KLog.d("MVP微信精选失败.......");

    }

    @Override
    public void getWXJXRefreshSuccess(WXJX wxjx) {

        KLog.d("MVP微信精选下拉刷新成功......." + wxjx.getResult().getList().get(0).getTitle());
//        mWxjx_refresh_layout.finishRefresh();
        mWxjxList.clear();
        mWxjxList.addAll(wxjx.getResult().getList());
        mWxjxRecyAdapter.notifyDataSetChanged();
        mWxjx_refresh_layout.setRefreshing(false);
    }

    @Override
    public void getWXJXRefreshfailed() {
        mWxjx_refresh_layout.setRefreshing(false);

        KLog.d("MVP微信精选下拉刷失败.......");
    }

    @Override
    public void getWXJXLoadMoreSuccess(WXJX wxjx) {
        mWxjxList.addAll(wxjx.getResult().getList());
//        mWxjx_refresh_layout.finishRefreshLoadMore();
        mWxjx_refresh_layout.setRefreshing(false);
        mWxjxRecyAdapter.notifyDataSetChanged();


    }


    @Override
    public void getWXJXLoadMorefailed() {
        KLog.d("MVP微信精选加载更多失败.......");
        mWxjx_refresh_layout.setRefreshing(false);
    }
//        mWxjxPresenterImp.refreshWXJX();
}
