package com.xzm.person.ui.activity1;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.xzm.person.R;


/**
 * Created by xuzongmeng on 2016/9/6.
 */
public class ProgressDialogActivity extends AppCompatActivity {
    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progressdialog);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new Dialog(ProgressDialogActivity.this, R.style.progress_dialog);
                progressDialog.setContentView(R.layout.dialog);
                progressDialog.setCancelable(true);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
                msg.setText("卖力加载中");
                progressDialog.show();
            }
        });
    }


    public void setBackgrroud(float backgrroud, Context context) {
        WindowManager.LayoutParams attributes = this.getWindow().getAttributes();
        attributes.alpha = backgrroud;
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.getWindow().setAttributes(attributes);

    }

}
