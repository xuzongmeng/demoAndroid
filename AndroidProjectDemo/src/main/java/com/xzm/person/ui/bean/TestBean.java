package com.xzm.person.ui.bean;

import java.io.Serializable;



/**
 * Created by Administrator on 2016/8/17.
 */
//@AllArgsConstructor
//@NoArgsConstructor
//@Setter
//@Getter
//@Data  // ：注解在类上；提供类所有属性的 getting 和 setting 方法，此外还提供了equals、canEqual、hashCode、toString 方法
//@Setter  //：注解在属性上；为属性提供 setting 方法
//@Getter //注解在属性上；为属性提供 getting 方法
//@Log4j  //注解在类上；为类提供一个 属性名为log 的 log4j 日志对象
//@NoArgsConstructor  //：注解在类上；为类提供一个无参的构造方法
//@AllArgsConstructor//：注解在类上；为类提供一个全参的构造方法
public class TestBean implements Serializable {
  private String name;
  private String sex;

  public TestBean(String name, String sex) {
    this.name = name;
    this.sex = sex;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }
}
