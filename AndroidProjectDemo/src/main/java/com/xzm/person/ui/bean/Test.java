package com.xzm.person.ui.bean;

import java.util.List;

/**
 * Created by xuzongmeng on 2018/4/18.
 */

public class Test {

    public static class ResultBean {
        /**
         * list : [{"id":"wechat_20180416025107","title":"7秒破百的霸气大7座SUV，卖多少万能让你忘了汉兰达？","source":"玩车教授","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62926536.static/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180416025107"},{"id":"wechat_20180416025105","title":"央视曝光赌博APP：影响恶劣、致人倾家荡产！","source":"TechWeb","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62926563.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180416025105"},{"id":"wechat_20180416023816","title":"去年中国卖了1000多万辆SUV，但SUV这3个缺点你要有点数","source":"车买买","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62926510.static/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180416023816"},{"id":"wechat_20180416023711","title":"【提醒】妈妈凌晨一点发现孩子在学习，吓得赶紧带他去医院\u2026","source":"人民日报","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62926507.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180416023711"},{"id":"wechat_20180415010764","title":"深度丨游戏不止娱乐：致游戏中新手与惶惶不知错的玩家","source":"天下军武","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62925350.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180415010764"},{"id":"wechat_20180415010418","title":"开4.3秒破百的性能宝马兜风，100公里到底要废多少油？【实测】","source":"有车以后","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62925169.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180415010418"},{"id":"wechat_20180415010445","title":"从今天起，做一个没修养的女人","source":"悦读","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-61794253.static/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180415010445"},{"id":"wechat_20180415010473","title":"恒大都砸1000亿搞高科技了，你还在用老办法干地产？！快跟上别掉队！","source":"明源地产研究院","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62925284.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180415010473"},{"id":"wechat_20180413020233","title":"中国人为什么吃不到新药？","source":"三联生活周刊","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62925030.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180413020233"},{"id":"wechat_20180413020390","title":"拼多多：让你们失望了，我现在混得很好！","source":"创业邦","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-62925089.jpg/640","mark":"","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20180413020390"}]
         * totalPage : 23161
         * ps : 10
         * pno : 1
         */

        private List<ListBean> list;

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : wechat_20180416025107
             * title : 7秒破百的霸气大7座SUV，卖多少万能让你忘了汉兰达？
             * source : 玩车教授
             * firstImg : http://zxpic.gtimg.com/infonew/0/wechat_pics_-62926536.static/640
             * mark :
             * url : http://v.juhe.cn/weixin/redirect?wid=wechat_20180416025107
             */

            private String id;
            private String title;
            private String source;
            private String firstImg;
            private String mark;
            private String url;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getFirstImg() {
                return firstImg;
            }

            public void setFirstImg(String firstImg) {
                this.firstImg = firstImg;
            }

            public String getMark() {
                return mark;
            }

            public void setMark(String mark) {
                this.mark = mark;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
}
