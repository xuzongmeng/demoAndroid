package com.xzm.person.ui.bean;

import java.util.Comparator;

/**
 * Created by xuzongmeng on 2017/8/22.
 */

public class PeopleComparator implements Comparator<People> {
    @Override
    public int compare(People o1, People o2) {
        /**
         * 将序
         */
        int flag = o2.getBirthday().compareTo(o1.getBirthday());
        System.out.println("标志==="+flag);
        return flag;
    }
}
