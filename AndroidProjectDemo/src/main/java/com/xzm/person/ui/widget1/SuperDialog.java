package com.xzm.person.ui.widget1;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.indicators.LineScalePulseOutRapidIndicator;
import com.xzm.person.R;
import com.xzm.person.utils.DeviceUtil;


/**
 * Created by xuzongmeng on 2016/7/7.
 */
public class SuperDialog {
    private LinearLayout ll_super_dialog;
    private Context context;
    private Dialog dialog;
    private Display display;
    private AVLoadingIndicatorView loading;
    private TextView tv_hintMsg;

    public SuperDialog(Context context) {
        this.context = context;
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public SuperDialog builder() {
        // 获取Dialog布局
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_super, null);
         ll_super_dialog = (LinearLayout) view.findViewById(R.id.ll_super_dialog);
        tv_hintMsg = (TextView) view.findViewById(R.id.tv_super_dialog_hintMsg);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.avloading_super_dialog);
        loading.setIndicator(new LineScalePulseOutRapidIndicator());
        loading.setIndicatorColor(Color.parseColor("#23262D"));


//        iv_hangqing_dismiss = (ImageView) view.findViewById(R.id.iv_hangqing_dismiss);
//        iv_hangqing_dismiss.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.AlertDialogStyle);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        // 调整dialog背景大小
        ll_super_dialog.setLayoutParams(new FrameLayout.LayoutParams((int) (DeviceUtil.getWidth((Activity) context) * 0.9), LinearLayout.LayoutParams.WRAP_CONTENT));

        return this;
    }

    public SuperDialog setHintMsg(String msg) {

        if (!TextUtils.isEmpty(msg)) {
            tv_hintMsg.setText(msg);
        } else {
            tv_hintMsg.setText("努力为您加载中...");
        }
        return this;
    }

    public SuperDialog setCancelable(boolean cancel) {
        dialog.setCancelable(cancel);
        return this;
    }

    public void show() {
        dialog.show();
    }

    public  void  disMiss(){
        dialog.dismiss();
    }


    private static final String[] INDICATORS=new String[]{
            "BallPulseIndicator",
            "BallGridPulseIndicator",
            "BallClipRotateIndicator",
            "BallClipRotatePulseIndicator",
            "SquareSpinIndicator",
            "BallClipRotateMultipleIndicator",
            "BallPulseRiseIndicator",
            "BallRotateIndicator",
            "CubeTransitionIndicator",
            "BallZigZagIndicator",
            "BallZigZagDeflectIndicator",
            "BallTrianglePathIndicator",
            "BallScaleIndicator",
            "LineScaleIndicator",
            "LineScalePartyIndicator",
            "BallScaleMultipleIndicator",
            "BallPulseSyncIndicator",
            "BallBeatIndicator",
            "LineScalePulseOutIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallScaleRippleIndicator",
            "BallScaleRippleMultipleIndicator",
            "BallSpinFadeLoaderIndicator",
            "LineSpinFadeLoaderIndicator",
            "TriangleSkewSpinIndicator",
            "PacmanIndicator",
            "BallGridBeatIndicator",
            "SemiCircleSpinIndicator",
            "com.wang.avi.sample.MyCustomIndicator"
    };


//    public SuperDialog setPositiveButton(String text,
//                                           final View.OnClickListener listener) {
//        showPosBtn = true;
//        if ("".equals(text)) {
//            btn_pos.setText("确定");
//        } else {
//            btn_pos.setText(text);
//        }
//        btn_pos.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onClick(v);
//                dialog.dismiss();
//            }
//        });
//        return this;
//    }

//
//    public SuperDialog setNegativeButton(String text, final View.OnClickListener listener) {
//        showNegBtn = true;
//        if ("".equals(text)) {
//            btn_neg.setText("取消");
//        } else {
//            btn_neg.setText(text);
//        }
//        btn_neg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onClick(v);
//                dialog.dismiss();
//            }
//        });
//        return this;
//    }



}
