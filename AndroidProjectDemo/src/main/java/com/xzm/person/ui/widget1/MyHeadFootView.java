package com.xzm.person.ui.widget1;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xzm.person.R;

/**
 * Created by xuzongmeng on 2017/1/13.
 */

public class MyHeadFootView {
    private Context context;
    private View view;
    private TextView textViewHeadFootView;
    private ImageView iv_head_foot_view;
    public MyHeadFootView(Context context) {
        this.context=context;
        init();
    }

    private  void init(){
        view = LayoutInflater.from(context).inflate(R.layout.layout_head_foot_view, null);
        textViewHeadFootView = (TextView) view.findViewById(R.id.textView_head_foot_view);
        iv_head_foot_view = (ImageView) view.findViewById(R.id.iv_head_foot_view);
    }

    public View getView(){
        return  view;
    }

    public MyHeadFootView builder(){
        return  this;
    }

     public MyHeadFootView setHeadFootText(String text){
         if (!TextUtils.isEmpty(text)){
             textViewHeadFootView.setText(text);
         }
         return  this;
     }

    public MyHeadFootView setHeadFootImg(int imgs){
        if (imgs!=0){
            iv_head_foot_view.setImageResource(imgs);
        }
        return  this;
    }
}
