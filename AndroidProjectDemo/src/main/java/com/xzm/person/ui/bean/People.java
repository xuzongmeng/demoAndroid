package com.xzm.person.ui.bean;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by xuzongmeng on 2017/8/22.
 */

public class People implements Serializable,Comparable<People> {
    private static final long serialVersionUID = 3561542540459160478L;
    private String city;
    private  int  age;
    private String birthday;
    private String constellation;


    public People() {
    }

    public People(String city, int age, String birthday, String constellation) {
        this.city = city;
        this.age = age;
        this.birthday = birthday;
        this.constellation = constellation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    @Override
    public int compareTo(@NonNull People o) {
      //升序
        int flag = this.getBirthday().compareTo(o.getBirthday());
        return flag;
    }

    @Override
    public String toString() {
        return "People{" +
                "city='" + city + '\'' +
                ", age=" + age +
                ", birthday='" + birthday + '\'' +
                ", constellation='" + constellation + '\'' +
                '}';
    }

    //
//    @Override
//    public int compareTo(@NonNull People o) {
//        return this.getAge().;
//    }
}
