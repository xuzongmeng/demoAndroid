package com.xzm.person.ui.adapter1;

import android.view.View;

import com.xzm.person.R;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by xuzongmeng on 2016/11/10.
 */

public class HongYangAdapterItemTwo implements ItemViewDelegate<RecycleViewMultiItemBean> {
    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_recycleview_multi_style2;
    }

    @Override
    public boolean isForViewType(RecycleViewMultiItemBean item, int position) {
        if (item.getStatus() == 2) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void convert(ViewHolder holder, final RecycleViewMultiItemBean testbean, final int position) {
        holder.getView(R.id.tv_user_name2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if (mOnItemClickListener!=null){
                     mOnItemClickListener.onItemClick(position,testbean);
                 }
            }
        });
    }

    private OnItemClickListener mOnItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
    public interface OnItemClickListener {
        void onItemClick(int postion, RecycleViewMultiItemBean bean);
    }
}
