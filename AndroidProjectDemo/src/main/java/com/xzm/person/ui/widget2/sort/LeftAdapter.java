package com.xzm.person.ui.widget2.sort;

import android.content.Context;
import android.support.annotation.NonNull;

import com.pacific.adapter.Adapter;
import com.pacific.adapter.AdapterHelper;
import com.xzm.person.R;

/**
 * Created by xuzongmeng on 2017/8/22.
 */

public class LeftAdapter extends Adapter<String> {
    private int checkItemPosition = 0;
    public LeftAdapter(Context context, @NonNull int... layoutResIds) {
        super(context, layoutResIds);
    }
    @Override
    protected void convert(AdapterHelper helper, String item) {
        helper.setText(R.id.text,item);
    }


    public void setCheckItem(int position) {
        checkItemPosition = position;
        notifyDataSetChanged();
    }
}
