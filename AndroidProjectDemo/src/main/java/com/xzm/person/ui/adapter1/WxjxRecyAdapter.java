package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pacific.adapter.Adapter;
import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.squareup.picasso.Picasso;
import com.xzm.person.R;

import com.xzm.person.ui.bean.WXJX;


/**
 * Created by Administrator on 2016/8/16.
 */
public class WxjxRecyAdapter extends RecyclerAdapter<WXJX.ResultBean.ListBean> {
    public Context context;
    public Intent intent;

    public WxjxRecyAdapter(Context context, @NonNull int... layoutResIds) {
        super(context, layoutResIds);
    }

//    @Override
//    public RecyclerView.ViewHolder onCreate(ViewGroup parent, int viewType) {
//        context = parent.getContext();
//        View itemView = LayoutInflater.from(context).inflate(R.layout.item_wxjx, parent, false);
//        return new LastViewHolder(itemView);
//    }

//    @Override
//    public void onBind(RecyclerView.ViewHolder viewHolder, int RealPosition, final WXJX.ResultBean.ListBean data) {
//
//
//    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, WXJX.ResultBean.ListBean data) {
        if (!TextUtils.isEmpty(data.getFirstImg())) {

            Picasso.with(context)
                    .load(data.getFirstImg())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into((ImageView) helper.getView(R.id.iv_wxjx_img));
        }
        helper.setText(R.id.tv_wxjx_id,data.getId()).
                setText(R.id.tv_wxjx_source,data.getSource()).
                setText(R.id.tv_title,data.getTitle());

    }



}
