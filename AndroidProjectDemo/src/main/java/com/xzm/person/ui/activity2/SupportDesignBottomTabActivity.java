package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.xzm.person.R;
import com.xzm.person.ui.adapter1.ViewPagerAdapter;
import com.xzm.person.ui.fragment1.FragmentHome;

import java.util.ArrayList;
import java.util.List;

/**
 * support design25滴底部导航控件
 */
public class SupportDesignBottomTabActivity extends AppCompatActivity {
  private List<Fragment> mlist=new ArrayList<>();
    private List<String> mlistTitle=new ArrayList<>();
    BottomNavigationView bottomNavigationView;
    ViewPager viewPager;
    MenuItem prevMenuItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_design_bottom_tab);



        viewPager = (ViewPager) findViewById(R.id.viewpager);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        mlist=new ArrayList<>();
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.item_call:
                    viewPager.setCurrentItem(0);
//                                bottomNavigationView.setItemBackgroundResource(R.drawable.selector_home);
                    break;
                case R.id.item_mail:
                    viewPager.setCurrentItem(1);
//                                bottomNavigationView.setItemBackgroundResource(R.drawable
//                                        .selector_manage_money);
                    break;
                case R.id.item_person:
                    viewPager.setCurrentItem(2);
//                                bottomNavigationView.setItemBackgroundResource(R.drawable
//                                        .selector_me);
                    break;
            }
            return false;
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // 如果想禁止滑动，可以把下面的代码取消注释
//        viewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return true;
//            }
//        });

        setupViewPager(viewPager);
    }


    public Fragment creatFragment(String content) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        fragmentHome.setArguments(bundle);
        return fragmentHome;
    }

    private void setupViewPager(ViewPager viewPager) {

           mlist.add(creatFragment("拨号"));
           mlist.add(creatFragment("信息"));
           mlist.add(creatFragment("我的"));


        mlistTitle.add("拨号");
        mlistTitle.add("信息");
        mlistTitle.add("我的");
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(),mlist,mlistTitle);

//        adapter.addFragment(creatFragment("拨号"));
//        adapter.addFragment(creatFragment("信息"));
//        adapter.addFragment(creatFragment("我的"));
        viewPager.setAdapter(adapter);
    }
}
