package com.xzm.person.ui.fragment1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.activity2.AutoReadSMSActivity;
import com.xzm.person.ui.activity2.AvLoadingViewActivity;
import com.xzm.person.ui.activity2.AvLoadingViewIndicatorActivity;
import com.xzm.person.ui.activity2.ChoosePhotoActivity;
import com.xzm.person.ui.activity2.CustomTitleBarActivity;
import com.xzm.person.ui.activity2.DiskLruCacheActivity;
import com.xzm.person.ui.activity2.LazyActivity;
import com.xzm.person.ui.activity2.NavigationActivity;
import com.xzm.person.ui.activity2.RvbaseAdapterActivity;
import com.xzm.person.ui.activity2.RvbaseAdapterMultiItemActivity;
import com.xzm.person.ui.activity2.RxjavaRetrofitActivity;
import com.xzm.person.ui.activity2.ScreenRotateActivity;
import com.xzm.person.ui.activity2.ScrollChangeColorActivity;
import com.xzm.person.ui.activity2.SlideHintBottomTabActivity;
import com.xzm.person.ui.activity2.SortActivity;
import com.xzm.person.ui.activity2.SupportDesignBottomTabActivity;
import com.xzm.person.ui.activity2.SupportDesignBottomTabActivity2;
import com.xzm.person.ui.activity2.TaobaoDingDanActivity;
import com.xzm.person.ui.activity2.ViewFlipperActivity;
import com.xzm.person.ui.activity2.WebSocketActivity;
import com.xzm.person.ui.adapter1.DemoItemAdapter;
import com.xzm.person.ui.bean.DemoItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentTwo extends Fragment {
    TextView textView;
    private DemoItemAdapter mAdapter;
    private List<DemoItem> mlist;
    private RecyclerView recyclerViewDemoItem;

    public static FragmentTwo newInstance(String text) {
        FragmentTwo fragmentCommon = new FragmentTwo();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);
//        textView= (TextView) view.findViewById(R.id.textView);
//        textView.setText(getArguments().getString("text"));
        initView(view);
        initDates();
        return view;
    }

    private void initView(View view) {
        recyclerViewDemoItem = view.findViewById(R.id.recyclerView_demo_item);
        recyclerViewDemoItem.setLayoutManager(new GridLayoutManager(getContext(), 2));
        TextView textView = view.findViewById(R.id.tv_toolbaer_title);
        textView.setText(getArguments().getString("text"));
    }

    public void initDates() {
        mlist = new ArrayList<>();
        mAdapter = new DemoItemAdapter(R.layout.item_demo, mlist);
        mAdapter.openLoadAnimation();
        recyclerViewDemoItem.setHasFixedSize(true);
        recyclerViewDemoItem.setAdapter(mAdapter);
        addBaseDate();
    }

    private void addBaseDate() {
        mlist.add(new DemoItem(AutoReadSMSActivity.class, "自动接收短信"));
        mlist.add(new DemoItem(AvLoadingViewActivity.class, "炫酷Loading"));
        mlist.add(new DemoItem(AvLoadingViewIndicatorActivity.class, "炫酷Loading导航"));
        mlist.add(new DemoItem(ChoosePhotoActivity.class, "选择相册"));
        mlist.add(new DemoItem(CustomTitleBarActivity.class, "CustomTitleBarActivity"));
        mlist.add(new DemoItem(DiskLruCacheActivity.class, "缓存框架"));
        mlist.add(new DemoItem(LazyActivity.class, "懒加载"));
//        mlist.add(new DemoItem(LoadingLayoutActivity.class, "多状态视图1"));
//        mlist.add(new DemoItem(MVPActivity.class, "MVP架构"));
        mlist.add(new DemoItem(NavigationActivity.class, "Navigation底部导航"));
        mlist.add(new DemoItem(RvbaseAdapterActivity.class, "杨明宇适配器1"));
        mlist.add(new DemoItem(RvbaseAdapterMultiItemActivity.class, "杨明宇适配器2"));
        mlist.add(new DemoItem(RxjavaRetrofitActivity.class, "RxJava和Retrofit组合"));
        mlist.add(new DemoItem(ScreenRotateActivity.class, "全屏旋转"));
        mlist.add(new DemoItem(ScrollChangeColorActivity.class, "滚动变色"));
        mlist.add(new DemoItem(SlideHintBottomTabActivity.class, "滑动隐藏导航栏"));
        mlist.add(new DemoItem(SortActivity.class, "排序列表"));
        mlist.add(new DemoItem(SupportDesignBottomTabActivity.class, "谷歌官方导航"));
        mlist.add(new DemoItem(SupportDesignBottomTabActivity2.class, "谷歌官方导航2"));
        mlist.add(new DemoItem(TaobaoDingDanActivity.class, "淘宝订单列表"));
        mlist.add(new DemoItem(ViewFlipperActivity.class, "滚动"));
        mlist.add(new DemoItem(ViewFlipperActivity.class, "滚动"));
        mlist.add(new DemoItem(WebSocketActivity.class, "WebSocketActivity"));
        KLog.d("====FragmentTwo=大小=" + mlist.size());
        mAdapter.notifyDataSetChanged();
    }
}
