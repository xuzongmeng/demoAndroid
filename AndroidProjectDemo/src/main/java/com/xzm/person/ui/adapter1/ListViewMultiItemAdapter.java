package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pacific.adapter.Adapter;
import com.pacific.adapter.AdapterHelper;
import com.xzm.person.R;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;

import java.util.List;


/**
 * Created by xuzongmeng on 2016/10/10.
 */

public class ListViewMultiItemAdapter extends Adapter<RecycleViewMultiItemBean> {



//    public RecycleViewMultiItemAdapter(Context context, @NonNull int... layoutResIds) {
//        super(context, layoutResIds);
//    }

    public ListViewMultiItemAdapter(Context context, @Nullable List<RecycleViewMultiItemBean> data, @NonNull int... layoutResIds) {
        super(context, data, layoutResIds);

    }

    @Override
    protected void convert(AdapterHelper helper, RecycleViewMultiItemBean item) {
       if (item.getStatus()==1){
           helper.setText(R.id.tv_user_name1,item.getUser_name())
                   .setText(R.id.tv_phone_number1,item.getPhone_number())
                   .setText(R.id.tv_user_qq1,item.getUser_QQ());
       }else if (item.getStatus()==2){
                   helper.setText(R.id.tv_user_name2,item.getUser_name())
                   .setText(R.id.tv_phone_number2,item.getPhone_number());
       }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getStatus();
    }


    @Override
    public int getLayoutResId(int viewType) {
        if (1== viewType) {
            return R.layout.item_recycleview_multi_style1;
        } else {
            return R.layout.item_recycleview_multi_style2;
        }
    }
}
