package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.xzm.person.R;


import butterknife.ButterKnife;
//import okhttp3.Response;
//import okhttp3.WebSocket;
//import okhttp3.WebSocketListener;
//import okhttp3.mockwebserver.MockResponse;
//import okhttp3.mockwebserver.MockWebServer;

/**
 * Created by xuzongmeng on 2016/11/1.
 * 聊天  socket 测试
 */

public class WebSocketActivity extends AppCompatActivity {




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_websocket);
        ButterKnife.bind(this);
//        initServer();
    }


//
//    private void initServer() {
//
//        MockWebServer mockWebServer = new MockWebServer();
//        mockWebServer.enqueue(new MockResponse().withWebSocketUpgrade(new WebSocketListener() {
//            @Override
//            public void onOpen(WebSocket webSocket, Response response) {
//                System.out.println("server onOpen");
//                System.out.println("server request header:" + response.request().headers());
//                System.out.println("server response header:" + response.headers());
//                System.out.println("server response:" + response);
//            }
//            @Override
//            public void onMessage(WebSocket webSocket, String string) {
//                System.out.println("server onMessage");
//                System.out.println("message:" + string);
//                //接受到5条信息后，关闭消息定时发送器
////                if(msgCount == 5){
////                    mTimer.cancel();
////                    webSocket.close(1000, "close by server");
////                    return;
////                }
//                webSocket.send("response-" + string);
//            }
//            @Override
//            public void onClosing(WebSocket webSocket, int code, String reason) {
//                System.out.println("server onClosing");
//                System.out.println("code:" + code + " reason:" + reason);
//            }
//            @Override
//            public void onClosed(WebSocket webSocket, int code, String reason) {
//                System.out.println("server onClosed");
//                System.out.println("code:" + code + " reason:" + reason);
//            }
//            @Override
//            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
//                //出现异常会进入此回调
//                System.out.println("server onFailure");
//                System.out.println("throwable:" + t);
//                System.out.println("response:" + response);
//            }
//        }));
//    }
}
