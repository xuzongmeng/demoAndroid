package com.xzm.person.ui.activity1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.bean.WXJX;
import com.xzm.person.utils.DiskLruCache;
import com.xzm.person.utils.OkHttpUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Administrator on 2016/8/17.
 */
public class TestOKhttpActivity extends Activity {

    private TextView tvOkhttp;


    public String path = "http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okhttp);


        tvOkhttp = findViewById(R.id.tv_okhttp);

        OkHttpUtils.get(path, new OkHttpUtils.ResultCallback<WXJX>() {
            @Override
            public void onSuccess(WXJX response) {
                KLog.d("okhttp==微信精选" + response);
                String title = response.getResult().getList().get(0).getTitle();
                KLog.d("okhttp==微信精选======" + title);
                tvOkhttp.setText(title);

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }


    private void testDisLruCache() {
        File file = new File("");
        if (!file.exists()) {
            file.mkdirs();
        }
        long maxSize = 100;
        try {
            DiskLruCache diskLruCache = DiskLruCache.open(file, 20, 30, maxSize);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
