package com.xzm.person.ui.activity1;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xzm.person.R;


/**
 * Created by xuzongmeng on 2016/9/13.
 * 平移动画
 *
 */
public class AnimationTranslationActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView ivTranslation1;
    private ImageView ivTranslation2;
    private TextView btnGotoScale;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_translation);
        ivTranslation1 = (ImageView) findViewById(R.id.iv_translation1);
        ivTranslation2 = (ImageView) findViewById(R.id.iv_translation2);
        btnGotoScale = (TextView) findViewById(R.id.btn_goto_scale);
        btnGotoScale.setOnClickListener(this);
        init();
    }





    private void init() {
        //先向左移出屏幕，然后再移动回来，就可以这样写：
        float curTranslationX1 = ivTranslation1.getTranslationX();
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(ivTranslation1, "translationX", curTranslationX1, -500f, curTranslationX1);
        animator1.setDuration(5000);
        animator1.start();

        float curTranslationX2 = ivTranslation1.getTranslationX();
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(ivTranslation2, "translationX", curTranslationX2, -500f, curTranslationX2, 500f);
        animator2.setDuration(5000);
        animator2.start();
    }




    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, AnimationScaleActivity.class));
    }
}

