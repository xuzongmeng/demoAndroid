//package com.xzm.person.ui.activity2;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.TypedValue;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//
//import com.socks.library.KLog;
//import com.xzm.baselibrary.base.Base2Activity;
//import com.xzm.person.R;
//import com.xzm.person.ui.widget1.CanRippleLayout;
//import com.xzm.person.ui.widget1.MyContentView;
//import com.xzm.person.ui.widget1.ViewZanWuShuJu;
//import com.xzm.person.utils.ToastUtil;
//
///**
// * Created by xuzongmeng on 2016/12/16.
// */
//
//public class LoadingLayoutActivity extends Base2Activity implements View.OnClickListener {
//
//    private Context mContext;
//    private Button testLoadingMyContentView;
//    private Button testLoadingMyContentLayout;
//    private LinearLayout llTestLoadingLayout;
//    private Button testLoadingEmptyView;
//    private Button testLoadingErrorView;
//    private Button testLoadingLoadingView;
//    private Button testLoadingNoNetworkView;
//    private Button testLoadingContentView;
//
//
//    @Override
//    protected String getToolBarTitle() {
//        return "自定义LoadingLayout";
//    }
//
//    @Override
//    protected void initViews(Bundle savedInstanceState) {
//        mContext = this;
//        testLoadingMyContentView = findViewById(R.id.test_loading_my_content_view);
//        testLoadingMyContentLayout = findViewById(R.id.test_loading_my_content_layout);
//        llTestLoadingLayout = findViewById(R.id.ll_test_loading_layout);
//        testLoadingEmptyView = findViewById(R.id.test_loading_empty_view);
//        testLoadingErrorView = findViewById(R.id.test_loading_error_view);
//        testLoadingLoadingView = findViewById(R.id.test_loading_loading_view);
//        testLoadingNoNetworkView = findViewById(R.id.test_loading_no_network_view);
//        testLoadingContentView = findViewById(R.id.test_loading_content_view);
//        CanRippleLayout.Builder.on(llTestLoadingLayout).rippleCorner(dp2Px(100)).rippleSpeed
//                (20).rippleAlpha(200).create();
//        testLoadingMyContentView.setOnClickListener(this);
//        testLoadingMyContentLayout.setOnClickListener(this);
//        llTestLoadingLayout.setOnClickListener(this);
//        testLoadingEmptyView.setOnClickListener(this);
//        testLoadingErrorView.setOnClickListener(this);
//        testLoadingLoadingView.setOnClickListener(this);
//        testLoadingNoNetworkView.setOnClickListener(this);
//        testLoadingContentView.setOnClickListener(this);
//
//    }
//
//    float dp2Px(float dp) {
//        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, this.getResources()
//                .getDisplayMetrics());
//    }
//
//    @Override
//    protected int getLayoutID() {
//        return R.layout.activity_loadinglayout;
//    }
//
//
//    @Override
//    public void initDates() {
//        showLoading();
////        showAnimationLoading();
//        showContentView();
//
////        showContent(R.layout.layout_my_contentview);
////
//////        MyContentView myContentView=new MyContentView(mContext);
//////        View contentView = myContentView.getContentView();
//////        showContent(contentView);
//    }
//
//
//    @Override
//    public void onReLoadDate() {
//        int num = 0;
//        int sumAll = num++;
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                showContent();
//            }
//        }, 1000);
//
//        KLog.d("重新加载数据被回调..........==" + sumAll);
//    }
//
//
//    private void test_my_zanwu_shuju_view_() {
//        ViewZanWuShuJu zanWuShuJu = new ViewZanWuShuJu(this);
//        View zanWuShuJuView = zanWuShuJu.getZanWuShuJuView();
//        KLog.d();
//        showContent(zanWuShuJuView);
//    }
//
//
//    private void showContentView() {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                showContent();
//            }
//        }, 1000);
//    }
//
//    private void showLongContentView() {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                showContent();
//            }
//        }, 5000);
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.test_loading_my_content_view:
//                MyContentView myContentView = new MyContentView(mContext);
//                View contentView = myContentView.getContentView();
//                showContent(contentView);
//                showLongContentView();
//                ToastUtil.showToast("点击加载自定义的View");
//                break;
//            case R.id.test_loading_my_content_layout:
//                showContent(R.layout.layout_my_contentview);
//                showLongContentView();
//                ToastUtil.showToast("点击加载自定义的Layout");
//                break;
//            case R.id.test_loading_empty_view:
//                showEmpty();
//                showContentView();
//                ToastUtil.showToast("点击暂无数据");
//                break;
//            case R.id.test_loading_error_view:
//
//                showError();
//                showContentView();
//                ToastUtil.showToast("点击错误视图");
//                break;
//            case R.id.test_loading_loading_view:
//                showLoading();
//                showContentView();
//                ToastUtil.showToast("点击加载中");
//                break;
//            case R.id.test_loading_no_network_view:
//                showNoNetwork();
//                showContentView();
//                ToastUtil.showToast("点击没有网络");
//                break;
//            case R.id.test_loading_content_view:
//                showContent();
//                showContentView();
//                ToastUtil.showToast("点击加载主内容内容");
//                break;
//        }
//    }
////    onReLoadDate()
//
//}
