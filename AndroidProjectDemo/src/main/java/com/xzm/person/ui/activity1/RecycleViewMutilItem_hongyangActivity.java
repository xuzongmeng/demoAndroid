package com.xzm.person.ui.activity1;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.xzm.person.R;
import com.xzm.person.ui.adapter1.HongYangAdapterItem;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.wrapper.LoadMoreWrapper;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by xuzongmeng on 2016/11/10.
 * <p>
 * 弘扬的多Item 并支持加载更多的适配器
 */

public class RecycleViewMutilItem_hongyangActivity extends Activity {
    private LoadMoreWrapper mLoadMoreWrapper;
    private RecyclerView recyclerviewHongyang;
    private List<RecycleViewMultiItemBean> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycleview_mutilitem_hongyang);
        recyclerviewHongyang = findViewById(R.id.recyclerview_hongyang);

        init();
    }

    private void init() {
        recyclerviewHongyang.setLayoutManager(new LinearLayoutManager(this));
        for (int i = 0; i < 5; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("张三" + i, "1890744790" +
                    i, "110220" + i, 1);
            mList.add(bean);
        }
        for (int i = 0; i < 5; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("李四" + i, "1560744790" +
                    i, "440550" + i, 2);
            mList.add(bean);
        }

        HongYangAdapterItem adapter = new HongYangAdapterItem(this, mList);
        mLoadMoreWrapper = new LoadMoreWrapper(adapter);
        mLoadMoreWrapper.setLoadMoreView(LayoutInflater.from(this).inflate(R.layout
                .default_loading, recyclerviewHongyang, false));
        mLoadMoreWrapper.setOnLoadMoreListener(new LoadMoreWrapper.OnLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 5; i++) {
                            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("李四" +
                                    i, "1560744790" +
                                    i, "440550" + i, 2);
                            mList.add(bean);
                        }
                        mLoadMoreWrapper.notifyDataSetChanged();

                    }
                }, 1000);
            }
        });

        adapter.setOnItemClickListener(new CommonAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Toast.makeText(RecycleViewMutilItem_hongyangActivity.this, "Click:" + position,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int
                    position) {
                Toast.makeText(RecycleViewMutilItem_hongyangActivity.this, "LongClick:" +
                        position, Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        recyclerviewHongyang.setAdapter(mLoadMoreWrapper);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
