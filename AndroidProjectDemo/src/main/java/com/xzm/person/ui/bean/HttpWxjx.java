package com.xzm.person.ui.bean;

import java.util.List;


/**
 * Created by xzm on 2017/2/16.
 */
//@Data
public class HttpWxjx {


//    @Override
//    public String toString() {
//        return "HttpWxjx{" +
//                "totalPage=" + totalPage +
//                ", ps=" + ps +
//                ", pno=" + pno +
//                ", list=" + list +
//                '}';
//    }

    /**
         * list : [{"id":"wechat_20170216032032","title":"【有声美文】有一种爱叫老婆，有一种付出叫为你而活",
         * "source":"沪江韩语","firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-12949035
         * .jpg/640","mark":"","url":"http://v.juhe
         * .cn/weixin/redirect?wid=wechat_20170216032032"},{"id":"wechat_20170216032143",
         * "title":"成功，是熬出来的","source":"世界经理人","firstImg":"http://zxpic.gtimg
         * .com/infonew/0/wechat_pics_-12949037.jpg/640","mark":"","url":"http://v.juhe
         * .cn/weixin/redirect?wid=wechat_20170216032143"}]
         * totalPage : 4013
         * ps : 2
         * pno : 1
         */

        private int totalPage;
        private int ps;
        private int pno;
        private List<ListBean> list;

        public int getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public int getPs() {
            return ps;
        }

        public void setPs(int ps) {
            this.ps = ps;
        }

        public int getPno() {
            return pno;
        }

        public void setPno(int pno) {
            this.pno = pno;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }
//         @Data
        public static class ListBean {

            private String id;
            private String title;
            private String source;
            private String firstImg;
            private String mark;
            private String url;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getFirstImg() {
                return firstImg;
            }

            public void setFirstImg(String firstImg) {
                this.firstImg = firstImg;
            }

            public String getMark() {
                return mark;
            }

            public void setMark(String mark) {
                this.mark = mark;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

}
