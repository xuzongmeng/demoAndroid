package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.xzm.person.R;
import com.xzm.person.ui.bean.Tab;

import java.util.List;


/**
 * Created by xuzongmeng on 2016/9/26.
 */

public class MyRecycleViewAdapter extends RecyclerAdapter<Tab> {

    public MyRecycleViewAdapter(Context context, @Nullable List<Tab> data, @NonNull int... layoutResIds) {
        super(context, data, layoutResIds);
    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, final Tab item) {
//        helper.setImageDrawable(R.id.id_num,item.getImage());
        helper.setImageResource(R.id.id_num, item.getImage());
        //设置监听
        helper.setOnClickListener(R.id.id_num, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = item.getText();
                Log.d("TAG", text);
            }
        });
    }
}
