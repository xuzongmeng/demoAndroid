//package com.xzm.person.ui.activity1;
//
//import android.app.Activity;
//import android.content.Context;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.gson.Gson;
//import com.socks.library.KLog;
//import com.xzm.person.R;
//import com.xzm.person.ui.bean.WXJX;
//import com.zhy.http.okhttp.OkHttpUtils;
//import com.zhy.http.okhttp.callback.StringCallback;
//
//import okhttp3.Call;
//
//
///**
// * Created by xuzongmeng on 2016/9/2.
// * 对OKhttp请求的封装
// */
//public class OkHttpUtilsActivity extends Activity implements View.OnClickListener {
//
//    private Context mContext;
//    private Button btn_click;
//    private TextView tv_1;
//    private TextView tv_2;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_okhttputils);
//        mContext = this;
//        initViews();
//        initDates();
//    }
//
//    /**
//     * 初始化布局
//     */
//    private void initViews() {
//
//
//        btn_click = (Button) findViewById(R.id.btn_click);
//        tv_1 = (TextView) findViewById(R.id.tv_1);
//        tv_2 = (TextView) findViewById(R.id.tv_2);
//        btn_click.setOnClickListener(this);
//
//    }
//
//    /**
//     * s初始化数据
//     */
//    private void initDates() {
//
//
//    }
//
//
//    /**
//     * 按钮点击事件滴回调
//     * @param v
//     */
//    @Override
//    public void onClick(View v) {
////        doGetHaveParameter();
////        doGetNoParameter();
//       doPostHaveParameter();
////        doPostNoParameter();
//    }
//
//    /**
//     * get请求带参数
//     */
//
//    int  page=1;
//    public void doGetHaveParameter() {
//        String path = "http://v.juhe.cn/weixin/query?";
//        String url = "http://www.csdn.net/";
//        OkHttpUtils
//                .get()
//                .url(path)
//                .addParams("key", "978c09d3c30f8640e3bd480b6bf4301f")
//                .addParams("pno", ""+page++)
//                .addParams("ps", "20")
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        KLog.d("OkHttp=Get带参数请求失败=" + e);
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        KLog.d("OkHttp=Get带参数请求=" + response.toString());
//                    }
//
//                });
//    }
//
//
//    /**
//     * get请求不带参数
//     */
//    public void doGetNoParameter() {
//        String path = "http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20";
//        OkHttpUtils
//                .get()
//                .url(path)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                        Log.d("TAG","请求数据="+e);
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//
//                        KLog.d("OkHttp=Post带参数请求=" + response.toString());
//
//                    }
//
//                });
//    }
//
//
//    /**
//     * Post请求带参数
//     */
//    public void doPostHaveParameter() {
//        String path = "http://v.juhe.cn/weixin/query?";
//        OkHttpUtils
//                .post()
//                .url(path)
//                .addParams("key", "978c09d3c30f8640e3bd480b6bf4301f")
//                .addParams("pno", "1")
//                .addParams("ps", "20")
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        KLog.d("OkHttp=Post带参数请求失败=" + e);
//
//
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//
//                        KLog.d("OkHttp=Post带参数请求=" + response.toString());
//                        Gson gson=new Gson();
//                        WXJX wxjx = gson.fromJson(response.toString(), WXJX.class);
//                        KLog.d("OkHttp=Post带参数请求=" + wxjx.getResult().getList().get(0).getTitle());
//                        KLog.d("OkHttp=Post带参数请求=" + wxjx.getError_code());
//                        if (wxjx.getError_code()==0){
//                            Toast.makeText(mContext,"请求成功", Toast.LENGTH_SHORT).show();
//
//                            tv_1.setText(wxjx.getResult().getList().get(0).getTitle());
//                        }else {
//                            Toast.makeText(mContext,"请求失败", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                });
//    }
//
//
//    /**
//     * Post请求不带参数
//     */
//    public void doPostNoParameter() {
//        String path = "http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20";
//        String url = "http://www.csdn.net/";
//        OkHttpUtils
//                .post()
//                .url(path)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        KLog.d("OkHttp=Post不带参数请求失败=" + e);
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        KLog.d("OkHttp=Post不带参数请求=" + response.toString());
//                    }
//                });
//    }
//
//
//
//}
