package com.xzm.person.ui.activity1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

import com.xzm.person.R;
import com.xzm.person.ui.widget1.AmountInDecreaseView;
import com.xzm.person.ui.widget1.StockWarningJiaJianView;


/**
 * Created by xuzongmeng on 2016/8/25.
 */
public class AmountInDecreaseViewActivity extends Activity {
    private AmountInDecreaseView viewAmount;
    private Button btnClick;
    private StockWarningJiaJianView stockwarningjiajianview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amountindecreaseview);

        viewAmount = findViewById(R.id.view_amount);
        btnClick = findViewById(R.id.btn_click);
        stockwarningjiajianview = findViewById(R.id.stockwarningjiajianview);

        final AmountInDecreaseView view_amount = (AmountInDecreaseView) this.findViewById(R.id
                .view_amount);
        double num = 0.04;
        view_amount.setNumber(num);

        stockwarningjiajianview.setNumber(0.78f);
        Button btn_click = (Button) findViewById(R.id.btn_click);
        btn_click.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            double number = view_amount.getNumber();
            System.out.println("数字==" + number);
        });



    }
}
