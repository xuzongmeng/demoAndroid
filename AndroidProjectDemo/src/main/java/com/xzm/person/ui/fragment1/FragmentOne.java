package com.xzm.person.ui.fragment1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.test.TestMainActivity;
import com.xzm.person.ui.activity1.ActivityLoadmoreListview;
import com.xzm.person.ui.activity1.AmountInDecreaseViewActivity;
import com.xzm.person.ui.activity1.AnimationAlphaActivity;
import com.xzm.person.ui.activity1.AnimationFixActivity;
import com.xzm.person.ui.activity1.AnimationRotationActivity;
import com.xzm.person.ui.activity1.AnimationScaleActivity;
import com.xzm.person.ui.activity1.AnimationTranslationActivity;
import com.xzm.person.ui.activity1.BottomTabActivity;
import com.xzm.person.ui.activity1.CoordinatorLayoutActivity;
import com.xzm.person.ui.activity1.CustomTabActivity;
//import com.xzm.person.ui.activity1.OkHttpUtilsActivity;
import com.xzm.person.ui.activity1.ProgressDialogActivity;
import com.xzm.person.ui.activity1.RecycleViewMutilItem_hongyangActivity;
import com.xzm.person.ui.activity1.RecycleViewMutilTwoItemActivity;
import com.xzm.person.ui.activity1.RecycleViewMutilitemActivity;
import com.xzm.person.ui.activity1.SnackBarActivity;
import com.xzm.person.ui.activity1.SocketActivity;
import com.xzm.person.ui.activity1.SwipeRefreshActivity;
import com.xzm.person.ui.activity1.TestOKhttpActivity;
import com.xzm.person.ui.activity1.WheelViewActivity;
import com.xzm.person.ui.activity1.WxjxActivity;
import com.xzm.person.ui.activity1.XWheelViewActivity;
import com.xzm.person.ui.adapter1.DemoItemAdapter;
import com.xzm.person.ui.bean.DemoItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentOne extends Fragment {
    TextView textView;
    private DemoItemAdapter mAdapter;
    private List<DemoItem> mlist;
    private RecyclerView recyclerViewDemoItem;

    public static FragmentOne newInstance(String text) {
        FragmentOne fragmentCommon = new FragmentOne();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);
//        textView= (TextView) view.findViewById(R.id.textView);
//        textView.setText(getArguments().getString("text"));
        initView(view);
        initDates();
        return view;
    }

    private void initView(View view) {
        recyclerViewDemoItem = view.findViewById(R.id.recyclerView_demo_item);
        recyclerViewDemoItem.setLayoutManager(new GridLayoutManager(getContext(), 2));
         textView = view.findViewById(R.id.tv_toolbaer_title);
    }

    public void initDates() {
        mlist = new ArrayList<>();
        mAdapter = new DemoItemAdapter(R.layout.item_demo, mlist);
        mAdapter.openLoadAnimation();
        recyclerViewDemoItem.setHasFixedSize(true);
        recyclerViewDemoItem.setAdapter(mAdapter);
        addBaseDate();
    }

    private void addBaseDate() {
        mlist.add(new DemoItem(ActivityLoadmoreListview.class, "ListView加载更多"));
        mlist.add(new DemoItem(AmountInDecreaseViewActivity.class, "加减View"));
        mlist.add(new DemoItem(AnimationAlphaActivity.class, "Alpha渐变动画"));
        mlist.add(new DemoItem(AnimationFixActivity.class, "组合动画"));
        mlist.add(new DemoItem(AnimationRotationActivity.class, "Rotation旋转动画"));
        mlist.add(new DemoItem(AnimationScaleActivity.class, "Scale缩放动画"));
        mlist.add(new DemoItem(AnimationTranslationActivity.class, "Translation组合动画"));
        mlist.add(new DemoItem(BottomTabActivity.class, "底部导航"));
        mlist.add(new DemoItem(CoordinatorLayoutActivity.class, "CoordinatorLayoutActivity"));
        mlist.add(new DemoItem(CustomTabActivity.class, "CustomTab底部导航"));
        mlist.add(new DemoItem(TestMainActivity.class, "弘扬OkHttpUtils"));
        mlist.add(new DemoItem(ProgressDialogActivity.class, "ProgressDialog系统弹窗"));
        mlist.add(new DemoItem(RecycleViewMutilItem_hongyangActivity.class, "hongyang多ItemType"));
        mlist.add(new DemoItem(RecycleViewMutilitemActivity.class, "RecycleView多itemType"));
        mlist.add(new DemoItem(RecycleViewMutilTwoItemActivity.class, "RecycleView多itemType2"));
        mlist.add(new DemoItem(SnackBarActivity.class, "SnackBar代替Toast"));
        mlist.add(new DemoItem(SocketActivity.class, "Socket通信"));
        mlist.add(new DemoItem(SwipeRefreshActivity.class, "SwipeRefresh官方刷新"));
        mlist.add(new DemoItem(TestOKhttpActivity.class, "OKhttp测试"));
        mlist.add(new DemoItem(WheelViewActivity.class, "WheelView轮滑"));
        mlist.add(new DemoItem(WxjxActivity.class, "微信加载数据"));
        mlist.add(new DemoItem(XWheelViewActivity.class, "WheelView轮滑2"));
        KLog.d("====fragmentOne=大小===" + mlist.size());
        textView.setText(getArguments().getString("text") + String.valueOf(mlist.size()));
        mAdapter.notifyDataSetChanged();
    }
}
