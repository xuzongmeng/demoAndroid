package com.xzm.person.ui.activity1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.xzm.person.R;
import com.xzm.person.utils.HQSocketUtil;
import com.xzm.person.utils.SocketUtil;
import com.xzm.person.utils.ToastUtil;


/**
 * Created by xuzongmeng on 2016/9/29.
 */

public class SocketActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSendSocket;
    private HQSocketUtil socketUtil;
    private SocketUtil socketUtil1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket);
        btnSendSocket = (Button) findViewById(R.id.btn_send_socket);
        socketUtil1 = SocketUtil.getInstance();
        socketUtil1.initSocket();
        socketUtil1.receiveSocketDate();
        btnSendSocket.setOnClickListener(this);
    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        ToastUtil.showToast("被点击......");
        socketUtil1.sendSocketDate("吃饭.");
    }
}
