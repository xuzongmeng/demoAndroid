package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.xzm.person.R;

public class CustomTitleBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_title_bar);
    }
}
