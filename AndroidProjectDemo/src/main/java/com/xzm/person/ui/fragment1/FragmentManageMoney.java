package com.xzm.person.ui.fragment1;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.xzm.person.R;
import com.xzm.person.ui.base.Base2Fragment;

/**
 * Created by xuzongmeng on 2017/1/16.
 */

public class FragmentManageMoney extends Base2Fragment {

    private TextView tv_fragemnt_text;
    private TextView tv_fragemnt_text_hit;
    @Override
    public int getLayoutID() {
        return R.layout.fragment_home;
    }

    @Override
    protected void lazyLoad() {
//        if (!isLoadDate){
//            initHttp();
//            tv_fragemnt_text_hit.setText("FragmentManageMoney没有初始化数据");
//        }else {
//            tv_fragemnt_text_hit.setText("FragmentManageMoney已经初始化过数据");
//
//        }

        if (!isLoad){
            initHttp();
            tv_fragemnt_text_hit.setText("FragmentManageMoney没有初始化数据");
        }else {
            tv_fragemnt_text_hit.setText("FragmentManageMoney已经初始化过数据");

        }

    }

    @Override
    public void initViews(View contentView) {
//        if (!isLoadDate){
//            showLoading();
//        }
        if (!isLoad){
            showLoading();
        }
        tv_fragemnt_text = (TextView) contentView.findViewById(R.id.tv_fragemnt_text);
        tv_fragemnt_text_hit = (TextView) contentView.findViewById(R.id.tv_fragemnt_text_hit);


        tv_fragemnt_text.setText("这是理财界面");
    }

    private void showContentView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showContent();
//                isLoadDate=true;
            }
        }, 2000);


    }


    private void initHttp() {
        showLoading();

        showContentView();
    }
}