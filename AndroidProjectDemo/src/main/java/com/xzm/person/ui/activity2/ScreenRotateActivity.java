package com.xzm.person.ui.activity2;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.xzm.person.R;
import com.xzm.person.utils.ScreenRotateUtil;

/**
 * Created by xuzongmeng on 2017/8/24.
 */

public class ScreenRotateActivity  extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_rotate);
       ScreenRotateUtil.getInstance(this).start(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }


    /**
     * 横竖屏切换或者输入法等事件触发时调用
     * 需要在清单文件中配置权限
     * 需要在当前Activity配置configChanges属性
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (ScreenRotateUtil.getInstance(this).isLandscape()) {
            Toast.makeText(this, "当前为横屏onConfigurationChanged", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "当前为竖屏onConfigurationChanged", Toast.LENGTH_SHORT).show();
        }
    }

    public void scaleFull(View view) {
        ScreenRotateUtil.getInstance(this).toggleRotate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (ScreenRotateUtil.getInstance(this).isLandscape()) {
            Toast.makeText(this, "当前为横屏", Toast.LENGTH_SHORT).show();
            ScreenRotateUtil.getInstance(this).toggleRotate();
        } else {
            Toast.makeText(this, "当前为竖屏", Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ScreenRotateUtil.getInstance(this).stop();
    }
}
