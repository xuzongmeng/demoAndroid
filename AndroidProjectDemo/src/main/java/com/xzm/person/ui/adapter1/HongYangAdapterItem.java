package com.xzm.person.ui.adapter1;

import android.content.Context;

import com.xzm.person.ui.bean.RecycleViewMultiItemBean;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;

import java.util.List;

/**
 * Created by xuzongmeng on 2016/11/10.
 */

public class HongYangAdapterItem extends MultiItemTypeAdapter<RecycleViewMultiItemBean> {
    public HongYangAdapterItem(Context context, List<RecycleViewMultiItemBean> datas) {
        super(context, datas);
        addItemViewDelegate(new HongYangAdapterItemOne());
        addItemViewDelegate(new HongYangAdapterItemTwo());
    }
}
