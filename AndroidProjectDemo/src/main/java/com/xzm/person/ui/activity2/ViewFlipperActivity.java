package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.widget.ViewFlipper;

import com.socks.library.KLog;
import com.xzm.person.R;


/**
 * Created by xuzongmeng on 2016/10/21.
 * 轮播图 和VIewPager类似
 */

public class ViewFlipperActivity extends AppCompatActivity {


    ViewFlipper viewFlipper = null;
    float startX;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_flipper);

        init();
    }

    private void init() {
        viewFlipper = (ViewFlipper) this.findViewById(R.id.viewFlipper);
        viewFlipper.setFlipInterval(3000);
        viewFlipper.startFlipping();
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                KLog.d("ACTION_DOWN按下XXX轴1111==="+startX);
                KLog.d("ACTION_DOWN按下XXX轴2222==="+event.getX());
                break;
            case MotionEvent.ACTION_UP:
                if (event.getX() > startX) { // 向右滑动
                    viewFlipper.setInAnimation(this, R.anim.in_leftright);
                    viewFlipper.setOutAnimation(this, R.anim.out_leftright);
                    viewFlipper.showNext();
                } else if (event.getX() < startX) { // 向左滑动
                    viewFlipper.setInAnimation(this, R.anim.in_rightleft);
                    viewFlipper.setOutAnimation(this, R.anim.out_rightleft);
                    viewFlipper.showPrevious();
                }
                KLog.d("ACTION_UP松开XXX轴1111==="+startX);
                KLog.d("ACTION_UP松开XXX轴2222==="+event.getX());
                break;
        }

        return super.onTouchEvent(event);
    }

}
