package com.xzm.person.ui.activity1;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.xzm.person.R;


/**
 * Created by xuzongmeng on 2016/9/14.
 * 组合 混合动画
 */
public class AnimationFixActivity extends AppCompatActivity {
    private ImageView ivFix1;
    private ImageView ivFix2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixanimation);
        ivFix1 = (ImageView) findViewById(R.id.iv_fix1);
        ivFix2 = (ImageView) findViewById(R.id.iv_fix2);
        init();
    }

    /**
     * <animator>  对应代码中的ValueAnimator
     * <objectAnimator>  对应代码中的ObjectAnimator
     * <set>  对应代码中的AnimatorSet
     */
    private void init() {
        /**
         * after(Animator anim)   将现有动画插入到传入的动画之后执行
         after(long delay)   将现有动画延迟指定毫秒后执行
         before(Animator anim)   将现有动画插入到传入的动画之前执行
         with(Animator anim)   将现有动画和传入的动画同时执行
         */
        ObjectAnimator moveIn = ObjectAnimator.ofFloat(ivFix1, "translationX", -500f, 0f);
        ObjectAnimator rotate = ObjectAnimator.ofFloat(ivFix1, "rotation", 0f, 360f);
        ObjectAnimator fadeInOut = ObjectAnimator.ofFloat(ivFix1, "alpha", 1f, 0f, 1f);


        AnimatorSet animset = new AnimatorSet();
        animset.play(rotate).with(fadeInOut).after(moveIn);
        animset.setDuration(5000);
        animset.start();
//        animset.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
        animset.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
    }


}
