package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.adapter1.RvbaseMultleAdapter;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;
import com.xzm.person.ui.widget1.DividerItemDecoration;
import com.xzm.person.ui.widget1.MyHeadFootView;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *  多种item形式
 * --陈宇明RecycleView 支持拖拽   item  动画 加载不同item 侧滑删除 多个头布局和脚布局
 */
@SuppressWarnings("deprecation")
public class RvbaseAdapterMultiItemActivity extends AppCompatActivity {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RvbaseMultleAdapter mAdapter;
    private RecyclerView   mRecyclerView;
    private List<RecycleViewMultiItemBean> mlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rvbase_adapter_multi_item);
        initViews();
        initDates();
    }

    private void initViews() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshlayout_rvbase_adapter);
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView_rvbase_adapter);
        mSwipeRefreshLayout.setEnabled(false);
    }

    private void initDates() {
         mlist=new ArrayList<>();
        mAdapter=new RvbaseMultleAdapter(mlist);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration
             .VERTICAL_LIST));
        mRecyclerView.setAdapter(mAdapter);
        for (int i = 0; i < 2; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("张三" + i, "1890744790" +
                    i, "110220" + i, 1);
            mlist.add(bean);
        }
        for (int i = 0; i < 2; i++) {
            RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("李四" + i, "1560744790" +
                    i, "440550" + i, 2);
            mlist.add(bean);
        }

        mAdapter.setOnLoadMoreListener(() -> loadMoreDate());
        MyHeadFootView builderHead1 = new MyHeadFootView(this).builder();
        builderHead1.setHeadFootImg(R.mipmap.ic_error).setHeadFootText("我的头布局1");
        mAdapter.addHeaderView(builderHead1.getView(),0);


        MyHeadFootView builderHead2 = new MyHeadFootView(this).builder();
        builderHead2.setHeadFootImg(R.mipmap.ic_launcher).setHeadFootText("我的头布局2");
        mAdapter.addHeaderView(builderHead2.getView(),0);


//
//        MyHeadFootView builder1Foot1 = new MyHeadFootView(this).builder();
//        builder1Foot1.setHeadFootImg(R.mipmap.ic_error).setHeadFootText("我是脚布局1");
//        mAdapter.addFooterView(builder1Foot1.getView(),0);
//        MyHeadFootView builder1Foot2 = new MyHeadFootView(this).builder();
//        builder1Foot2.setHeadFootImg(R.mipmap.ic_error).setHeadFootText("我是脚布局");
//        mAdapter.addFooterView(builder1Foot2.getView());


    }



    public  void loadMoreDate(){
        new Handler().postDelayed(() -> {
          KLog.d("RvbaseAdapterMultiItemActivity==加载更多时=="+mlist.size());

            if (mlist.size()>60){
//                    mAdapter.loadMoreEnd();
                mAdapter.loadMoreEnd(true);//true is gone,false is visible

                KLog.d("RvbaseAdapterMultiItemActivity==加载更多时已结加载完毕==");
            }
            for (int i = 0; i < 10; i++) {
                RecycleViewMultiItemBean bean = new RecycleViewMultiItemBean("张三=增加更多" + i,
                        "1560744790" +
                                i, "440550" + i, 2);
                mlist.add(bean);
            }
          mAdapter.loadMoreComplete();

        }, 500);
    }
}
