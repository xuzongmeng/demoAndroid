package com.xzm.person.ui.bean;

/**
 * Created by xuzongmeng on 2016/12/19.
 */

public class RecycleViewBean {
   private String text;

    public RecycleViewBean(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
