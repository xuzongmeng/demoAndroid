package com.xzm.person.ui.activity1;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.socks.library.KLog;
import com.xzm.person.R;


/**
 * Created by xuzongmeng on 2016/9/12.
 * 渐变动画
 */
public class AnimationAlphaActivity extends AppCompatActivity {

    private ImageView ivAnimator1;
    private ImageView ivAnimator2;
    private Button btnGotoRotation;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        initView();
        initValueAnimator();
        initObjectAnimator();
    }


    private void initView() {
        ivAnimator1 = (ImageView) findViewById(R.id.iv_animator1);
        ivAnimator2 = (ImageView) findViewById(R.id.iv_animator2);
        btnGotoRotation = (Button) findViewById(R.id.btn_goto_rotation);
        btnGotoRotation.setOnClickListener(v -> {
            startActivity(new Intent(this, AnimationRotationActivity.class));
        });

    }

    /**
     * 整个属性动画机制当中最核心的一个类
     */
    private void initValueAnimator() {

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0f, 1f);
        valueAnimator.setDuration(300);
        valueAnimator.start();
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float animatedValue = (float) animation.getAnimatedValue();
                KLog.d("动画属性ValueAnimator==" + animatedValue);
            }


        });
    }

    /**
     * alpha渐变动画
     */
    private void initObjectAnimator() {
        //这里如果我们想要将一个ImageView在5秒中内从常规变换成全透明，再从全透明变换成常规
        ImageView iv_animator1 = (ImageView) findViewById(R.id.iv_animator1);
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(iv_animator1, "alpha", 1f, 0f, 1f);
        animator1.setDuration(5000);
        animator1.start();

        //这里如果我们想要将一个ImageView在5秒中内从常规变换成全透明透明，换成常规半透明，变成常规
        ImageView iv_animator2 = (ImageView) findViewById(R.id.iv_animator2);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(iv_animator2, "alpha", 0f, 0.5f, 1f);
        animator2.setDuration(5000);
        animator2.start();

    }

}
