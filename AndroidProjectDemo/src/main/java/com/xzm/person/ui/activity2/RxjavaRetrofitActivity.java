package com.xzm.person.ui.activity2;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.net.http.HttpMethods;
import com.xzm.person.net.subscriber.OnNextSubscriber;
import com.xzm.person.net.subscriber.SubscriberOnNextListener;
import com.xzm.person.net.subscriber.SuperProgressSubscriber;
import com.xzm.person.ui.bean.HttpWxjx;
import com.xzm.person.ui.bean.WXJX;

//import com.xzm.person.net.subscriber.ProgressSubscriber;

@SuppressWarnings("deprecation")

public class RxjavaRetrofitActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView tvAllDate;
    private TextView tvExactDate;

    private String key = "978c09d3c30f8640e3bd480b6bf4301f";
    private int pno = 1;
    private int ps = 10;
    // http://v.juhe.cn/weixin/query?key=978c09d3c30f8640e3bd480b6bf4301f&pno=1&ps=20
    private SubscriberOnNextListener<HttpWxjx> httplistener;

    private SubscriberOnNextListener<WXJX> listener;

    HttpWxjx httpbean;

    WXJX bean;
    private Button btnGetHttpDate;
    private Button btnPostHttpDate;
    private Button btnGetDate;
    private Button btnPostDate;
    private Button btnGetExactDate;
    private Button btnClearAllDate;
    private Context context;
    private Button btnErrorParameter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rxjava_retrofit);
        initView();
        initDate();
    }

    private void initView() {
        context = this;
        btnGetHttpDate = (Button) findViewById(R.id.btn_getHttpDate);
        btnPostHttpDate = (Button) findViewById(R.id.btn_postHttpDate);

        btnGetDate = (Button) findViewById(R.id.btn_getDate);
        btnPostDate = (Button) findViewById(R.id.btn_postDate);
        btnGetExactDate = (Button) findViewById(R.id.btn_getExactDate);


        btnErrorParameter = (Button) findViewById(R.id.btn_error_parameter);
        tvAllDate = (TextView) findViewById(R.id.tv_AllDate);
        tvExactDate = (TextView) findViewById(R.id.tv_Exact_Date);

        btnClearAllDate = (Button) findViewById(R.id.btn_clear_All_Date);

        btnGetHttpDate.setOnClickListener(this);
        btnPostHttpDate.setOnClickListener(this);
        btnErrorParameter.setOnClickListener(this);
        btnPostDate.setOnClickListener(this);
        btnGetDate.setOnClickListener(this);
        btnGetExactDate.setOnClickListener(this);
        btnClearAllDate.setOnClickListener(this);
    }

    boolean isClick = false;

    private void initDate() {

           /*-----------------------------带HttpResult包裹层---------------------------------*/
        httplistener = new SubscriberOnNextListener<HttpWxjx>() {
            @Override
            public void onNext(HttpWxjx date) {
                if (date != null) {
                    KLog.d("Rxjava=请求微信精选数据=" + date.toString());

                    KLog.d("Rxjava=请求微信精选数据=====" + date.getList().get(0).getTitle());
                    httpbean = date;

                    if (isClick) {
                        tvAllDate.setText("");
                        tvAllDate.setText(date.toString());
                    } else {
                        tvAllDate.setText(date.toString());
                    }

                }

            }
        };

         /*-----------------------------不携带HttpResult包裹层---------------------------------*/
        listener = new SubscriberOnNextListener<WXJX>() {
            @Override
            public void onNext(WXJX date) {
                if (date != null) {
                    KLog.d("Rxjava=请求微信精选数据=" + date.toString());
                    KLog.d("Rxjava=请求微信精选数据=====" + date.getResult().getList().get(0).getTitle());
                    bean = date;
                    if (isClick) {
                        tvAllDate.setText("");
                        tvAllDate.setText(date.toString());
                    } else {
                        tvAllDate.setText(date.toString());
                    }

                }

            }
        };


    }

    //    /*-----------------------------带HttpResult包裹层---------------------------------*/
//    //进行网络请求  有Dialog时
//    @SuppressWarnings("unchecked")
//    private void getHttpDate() {
//        ProgressSubscriber progressSubscriber = new ProgressSubscriber(httplistener, this, "慢点啊...");
//        HttpMethods.getInstance().getHttpWxjx(progressSubscriber, key, pno, ps);
//
//    }
//
//    //进行网络请求   有Dialog时
    private void postHttpDate() {
        HttpMethods.getInstance().postHttpWxjx(new OnNextSubscriber<>(httplistener), key, pno, ps);
    }

    /*-----------------------------不携带HttpResult包裹层---------------------------------*/


    //进行网络请求   没有Dialog时
    private void getDate() {
        HttpMethods.getInstance().getWxjx(new OnNextSubscriber<>(listener), key, pno, ps);
    }


    //进行网络请求  有自定义Dialog时
    private void postDate() {
        HttpMethods.getInstance().postWxjx(new SuperProgressSubscriber<>(listener, context,
                "客官您别着急呀..."), key, pno, ps);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_getHttpDate:
                isClick = true;
//                ToastUtil.showToast("get获取Http全部数据");
//                getHttpDate();
                break;
            case R.id.btn_postHttpDate:
                isClick = true;
//                ToastUtil.showToast("post获取Http全部数据");
                postHttpDate();
                break;
            case R.id.btn_getDate:
                isClick = true;
//                ToastUtil.showToast("普通get获取全部数据");
                getDate();
                break;
            case R.id.btn_postDate:
                isClick = true;
//                ToastUtil.showToast("普通post获取全部数据");
                postDate();
                break;
            case R.id.btn_getExactDate:
//                ToastUtil.showToast("获取某条数据");
                getExactDate();
                break;
            case R.id.btn_clear_All_Date:
//                ToastUtil.showToast("清除所有数据");
                clearAllDate();
                break;

            case R.id.btn_error_parameter:
//                ToastUtil.showToast("传入错误参数");
//               errorParameter();
                break;
            default:
                break;

        }
    }

//    private void errorParameter() {
//        HttpMethods.getInstance().getHttpWxjx(new ProgressSubscriber<>(httplistener, this,
//                "慢点啊..."), "21343", pno, ps);
//    }

    private void clearAllDate() {
        tvAllDate.setText("");
        tvExactDate.setText("");
    }

    private void getExactDate() {
        if (httpbean != null) {
            tvExactDate.setText(httpbean.getList().get(0).getTitle());
        }
        if (bean != null) {
            tvExactDate.setText(bean.getResult().getList().get(0).getTitle());
        }

    }
}
