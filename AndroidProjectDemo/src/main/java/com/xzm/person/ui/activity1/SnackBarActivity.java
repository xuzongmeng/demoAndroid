package com.xzm.person.ui.activity1;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;

import com.github.mrengineer13.snackbar.SnackBar;
import com.xzm.person.R;

/**
 * Created by Administrator on 2016/8/17.
 */
public class SnackBarActivity extends Activity {
    private SnackBar mSnackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snack_bar);
        this.findViewById(R.id.btn_click1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSnackBar = new SnackBar.Builder(SnackBarActivity.this)
                        .withMessage("提示信息")
                        .withDuration(SnackBar.SHORT_SNACK)
                        .withClearQueued(true)
                        .show();
            }
        });

        initDates();

    }

    private void initDates() {

        this.findViewById(R.id.btn_click2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackbar(v);
            }
        });




        final Button btn_click3 = (Button) this.findViewById(R.id.btn_click3);
        this.findViewById(R.id.btn_click3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackbar(btn_click3);
            }
        });
    }


    public  void   showSnackbar(View view){
        final Snackbar snackbar = Snackbar.make(view, "关注非著名程序员公众号了吗？", Snackbar
                .LENGTH_LONG);
        snackbar.show();
        snackbar.setAction("关注", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
    }
}
