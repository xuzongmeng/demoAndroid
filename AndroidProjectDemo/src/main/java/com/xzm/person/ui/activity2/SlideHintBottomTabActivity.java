package com.xzm.person.ui.activity2;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.socks.library.KLog;
import com.wingsofts.byeburgernavigationview.ByeBurgerBehavior;
import com.xzm.person.R;
import com.xzm.person.ui.adapter1.RecycleViewAdapter;
import com.xzm.person.ui.bean.RecycleViewBean;
import com.xzm.person.ui.widget1.LinerLayoutDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * 滑动隐藏底部按钮
 */

public class SlideHintBottomTabActivity extends AppCompatActivity {
    private List<RecycleViewBean> mlist;
    private RecyclerView mRecyclerView;
    private LinearLayout mLinearLayout;
    private RecycleViewAdapter mAdapter;
    private ByeBurgerBehavior mBehavior;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_hint_bottom_tab);
        initViews();
        initDates();
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_slide_hint_bottom_tab);
        mLinearLayout = (LinearLayout) findViewById(R.id.ll_slide_hint_bottom_tab);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_slide_hint_bottom_tab);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mBehavior = ByeBurgerBehavior.from(mLinearLayout);
//      mBehavior.show();

//        ByeBurgerBehavior.show(mToolbar).show();
//        initListener();
    }

    private void initListener() {
        final int mScrollThreshold=0;
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                boolean isSignificantDelta  = Math.abs(dy) > mScrollThreshold;
//                 KLog.d("底部滑动==距离="+dy);
                if (dy>0){
                    onScrollUp();
                }else {
                    onScrollDown();
                }
            }


        });
    }


    /**
     * 下滑监听
     */
    private void onScrollDown() {
//下滑时要执行的代码
//        KLog.d("底部滑动==onScrollDown=================");
//   mLinearLayout.setVisibility(View.VISIBLE);
//        mLinearLayout.setSystemUiVisibility(View.GONE);
//       showBottom();
//       animateBack();
    }


    /**
     * 上滑监听
     */
    private void onScrollUp() {
//       mLinearLayout.setVisibility(View.GONE);
//上滑时要执行的代码
//        KLog.d("底部滑动==onScrollUpxxxxxxxxxxxxxxxxxxxxxx");
//      hideBottom();
//        animateHide();
    }


    AnimatorSet backAnimatorSet;//这是显示头尾元素使用的动画
    private void animateBack() {
        //先清除其他动画
        if (hideAnimatorSet != null && hideAnimatorSet.isRunning()) {
            hideAnimatorSet.cancel();
        }
        if (backAnimatorSet != null && backAnimatorSet.isRunning()) {
            //如果这个动画已经在运行了，就不管它
        } else {
            backAnimatorSet = new AnimatorSet();
            //下面两句是将头尾元素放回初始位置。
            ObjectAnimator headerAnimator = ObjectAnimator.ofFloat(mToolbar, "translationY", mToolbar
                    .getTranslationY(), 0f);
            ObjectAnimator footerAnimator = ObjectAnimator.ofFloat(mLinearLayout, "translationY", mLinearLayout
                    .getTranslationY(), 0f);
            ArrayList<Animator> animators = new ArrayList<>();
            animators.add(headerAnimator);
            animators.add(footerAnimator);
            backAnimatorSet.setDuration(500);
            backAnimatorSet.playTogether(animators);
            backAnimatorSet.start();
        }
    }

    AnimatorSet hideAnimatorSet;//这是隐藏头尾元素使用的动画
    private void animateHide(){
        //先清除其他动画
        if (backAnimatorSet!=null&&backAnimatorSet.isRunning()){
            backAnimatorSet.cancel();
        }
        //如果这个动画已经在运行了，就不管它
        if (hideAnimatorSet!=null&&hideAnimatorSet.isRunning()){
            hideAnimatorSet = new AnimatorSet();
            ObjectAnimator headerAnimator = ObjectAnimator.ofFloat(mToolbar, "translationY", mToolbar
                    .getTranslationY(), -mToolbar.getHeight());//将ToolBar隐藏到上面
            ObjectAnimator footerAnimator = ObjectAnimator.ofFloat(mLinearLayout, "translationY", mLinearLayout
                    .getTranslationY(), mLinearLayout.getHeight());//将Button隐藏到下面
            ArrayList<Animator> animators = new ArrayList<>();
            animators.add(headerAnimator);
            animators.add(footerAnimator);
            hideAnimatorSet.setDuration(500);
            hideAnimatorSet.playTogether(animators);
            hideAnimatorSet.start();
        }
    }

    private void showBottom() {


        float curTranslationY = mLinearLayout.getTranslationY();
        KLog.d("底部滑动==当前Y轴为=="+curTranslationY);

        KLog.d("底部滑动==当前高度=="+mLinearLayout.getHeight());
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(mLinearLayout, "translationY",
                curTranslationY,curTranslationY-mLinearLayout.getHeight());
        animator2.setDuration(1000);
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                KLog.d("底部滑动=显示时动画更新="+animation.getAnimatedValue());
                mLinearLayout.setY((Float) animation.getAnimatedValue());
            }
        });
        animator2.start();


//        ValueAnimator va = ValueAnimator.ofFloat(mLinearLayout.getY(), mLinearLayout.getHeight());
//        va.setDuration(300);
//        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                mLinearLayout.setY((Float) valueAnimator.getAnimatedValue());
//            }
//        });
//
//        va.start();

    }

    private void hideBottom() {
//        float curTranslationY = mLinearLayout.getTranslationY();
//        KLog.d("底部滑动==当前Y轴为=="+curTranslationY);
//        ObjectAnimator animator2 = ObjectAnimator.ofFloat(mLinearLayout, "translationY", curTranslationY,curTranslationY+mLinearLayout.getHeight());
//        animator2.setDuration(1000);
//        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                KLog.d("底部滑动=隐藏时动画更新="+animation.getAnimatedValue());
//                mLinearLayout.setY((Float) animation.getAnimatedValue());
//            }
//        });
//        animator2.start();


        ValueAnimator va = ValueAnimator.ofFloat(mLinearLayout.getY(), mLinearLayout.getY() +
                mLinearLayout.getHeight());
        va.setDuration(300);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                KLog.d("底部滑动=隐藏时动画更新="+valueAnimator.getAnimatedValue());
                mLinearLayout.setY((Float) valueAnimator.getAnimatedValue());
            }
        });
        va.start();

    }

    private void initDates() {

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        mRecyclerView.setLayoutManager(manager);//给RecyclerView设置，让RecyclerView知道该显示什么样式
        mRecyclerView.addItemDecoration(new LinerLayoutDecoration(this,LinearLayoutManager.VERTICAL));
        mAdapter = new RecycleViewAdapter(this, mlist, R.layout.item_recyclerview)
        ;//适配器对象
        mRecyclerView.setAdapter(mAdapter); //设置适配器
//            KLog.d("====" + mlist.size());

        mlist = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            RecycleViewBean bean = new RecycleViewBean("文本" + i);
            mlist.add(bean);
        }
        mAdapter.addAll(mlist);
        //设置RecycleView的布局管理器


    }
}
