package com.xzm.person.ui.activity1;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.xzm.person.R;
import com.xzm.person.ui.fragment1.FragmentFour;
import com.xzm.person.ui.fragment1.FragmentOne;
import com.xzm.person.ui.fragment1.FragmentThree;
import com.xzm.person.ui.fragment1.FragmentTwo;
import com.xzm.person.ui.tab.TabView;
import com.xzm.person.ui.tab.TabViewChild;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TabView tabView;
    private String text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabView = (TabView) findViewById(R.id.tabView);
        //start add data
        List<TabViewChild> tabViewChildList = new ArrayList<>();
        TabViewChild tabViewChild01 = new TabViewChild(R.drawable.tab01_sel, R.drawable
                .tab01_unsel, "首页", FragmentOne.newInstance("首页"));
        TabViewChild tabViewChild02 = new TabViewChild(R.drawable.tab02_sel, R.drawable
                .tab02_unsel, "分类", FragmentTwo.newInstance("分类"));
        TabViewChild tabViewChild03 = new TabViewChild(R.drawable.tab03_sel, R.drawable
                .tab03_unsel, "目录", FragmentThree.newInstance("目录"));
        TabViewChild tabViewChild04 = new TabViewChild(R.drawable.tab05_sel, R.drawable
                .tab05_unsel, "我的", FragmentFour.newInstance("我的"));
        tabViewChildList.add(tabViewChild01);
        tabViewChildList.add(tabViewChild02);
        tabViewChildList.add(tabViewChild03);
        tabViewChildList.add(tabViewChild04);
        //end add data
        tabView.setTabViewDefaultPosition(0);
        tabView.setTabViewChild(tabViewChildList, getSupportFragmentManager());
        tabView.setOnTabChildClickListener((position, imageView, textView) -> {
        });
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (System.currentTimeMillis() - myTime > 2000) {
//                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
//                myTime = System.currentTimeMillis();
//            } else {
////                MinstoneApplication.getInstance().exit();
//                return false;
//            }
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
}
