package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.xzm.person.R;
import com.xzm.person.ui.adapter1.FullyLinearLayoutManager;
import com.xzm.person.ui.adapter1.ShopListAdapter;
import com.xzm.person.ui.bean.Product;
import com.xzm.person.ui.widget1.LinerLayoutDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuzongmeng on 2016/12/30.
 * 淘宝订单列表
 */

public class TaobaoDingDanActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewShopList;
    private List<Product.DataBean.OrderListBean> mlist;
    private ShopListAdapter mShopListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taobao_dingdan);
        initViews();
        initDates();

        initHttp();
    }


    /**
     * 初始化 布局
     */
    private void initViews() {
        mRecyclerViewShopList = (RecyclerView) findViewById(R.id
                .recycler_view_product_shop_list);
    }

    /**
     * 初始化  数据
     */
    private void initDates() {

        //设置RecycleView的布局管理器
        FullyLinearLayoutManager layoutManager = new FullyLinearLayoutManager(this,
                FullyLinearLayoutManager.VERTICAL, false);
        mRecyclerViewShopList.setLayoutManager(layoutManager);
        mShopListAdapter = new ShopListAdapter(this, mlist, R.layout
                .item_product_shop);
        mRecyclerViewShopList.addItemDecoration(new LinerLayoutDecoration(this,
                LinearLayoutManager.VERTICAL));
        mRecyclerViewShopList.setAdapter(mShopListAdapter);
    }

    /**
     * 并设置数据
     * （假如这是模拟请求完了并设置数据）
     */
    private void initHttp() {


        Product product = new Product();
        Product.DataBean dataBean = new Product.DataBean();
         Product.DataBean.OrderListBean shop = null;


//        Product.DataBean.OrderListBean orderListBean = new Product.DataBean.OrderListBean();

        List<Product.DataBean.OrderListBean> shoplist = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            shop = new Product.DataBean.OrderListBean(R
                   .mipmap.ic_launcher, "阿里巴巴旗舰店铺" + i, "卖家已发货", "共2件商品", "合计：58(含运费：10元)");
            shoplist.add(shop);
        }

        List<Product.DataBean.OrderListBean.GoodsBean> productList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Product.DataBean.OrderListBean.GoodsBean goodsBean = new Product.DataBean
                    .OrderListBean.GoodsBean(R.mipmap.ic_launcher, "我爱我家旗舰店的床上4件套我爱我家旗舰店的床上4件套" +
                    i, "适用尺寸：1.8英寸", "黄色", "15天无条件换新", "50元", "99元", "4件");
            productList.add(goodsBean);
        }
        shop.setGoods(productList);

        dataBean.setOrder_list(shoplist);
        product.setData(dataBean);

        mShopListAdapter.addAll(shoplist);
    }

}
