package com.xzm.person.ui.activity1;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.xzm.person.R;

/**
 * Created by xuzongmeng on 2016/9/13.
 * 旋转动画
 */
public class AnimationRotationActivity extends AppCompatActivity {

    private ImageView ivRotation1;
    private ImageView ivRotation2;
    private Button btnGotoTranslation;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animationrotation);

        ivRotation1 = (ImageView) findViewById(R.id.iv_rotation1);
        ivRotation2 = (ImageView) findViewById(R.id.iv_rotation2);
        btnGotoTranslation = (Button) findViewById(R.id.btn_goto_translation);
        btnGotoTranslation.setOnClickListener(v -> startActivity(new Intent(this,
                AnimationTranslationActivity.class)));
        init();
    }





    private void init() {
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(ivRotation1, "rotation", 0f, 360f);
        animator1.setDuration(5000);
        animator1.start();


        ObjectAnimator animator2 = ObjectAnimator.ofFloat(ivRotation2, "rotation", 0f, 180f,
                360f, 0f);
        animator2.setDuration(5000);
        animator2.start();
    }



}
