package com.xzm.person.ui.bean;

import java.util.List;

/**
 * Created by xuzongmeng on 2016/12/30.
 */

public class Product {

    /**
     * data : {"order_list":[{"addressid":"10","canrefund":true,"cashier":"0",
     * "createtime":"2016-12-21 11:30:12","express":"","expresscom":"","expresssn":"",
     * "finishtime":"0","goods":[{"goodsid":"146","price":"0.01","s_price":"0.01",
     * "supplier_uid":"0","thumb":"http://wx.djk01
     * .com/attachment/images/3/2016/11/nEC2zIi1IZsosq2ehTeOEreON4C572.gif",
     * "title":"铭德堂枸杞王50g（测试，拍下不发货）","total":"1"}],"goodscount":1,"id":"74","iscomment":"0",
     * "isverify":"0","ordersn":"SH201612211130122445","paytype":"1","price":"0.01",
     * "refund_status":1,"refundid":"14","refundstate":"0","sellername":"东方大健康","status":"1",
     * "user_status":1,"verified":"0","verifycode":"","virtual":"0"}],"psize":10,"total":"4"}
     * message :
     * status : 1
     */

    private DataBean data;
    private String message;
    private int status;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static class DataBean {

        private int psize;
        private String total;
        private List<OrderListBean> order_list;

        public int getPsize() {
            return psize;
        }

        public void setPsize(int psize) {
            this.psize = psize;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<OrderListBean> getOrder_list() {
            return order_list;
        }

        public void setOrder_list(List<OrderListBean> order_list) {
            this.order_list = order_list;
        }

        public static class OrderListBean {

            private int shop_img;//店铺滴图片
            private String tv_shop_name;  //我爱我家旗舰店
            private String tv_shop_product_status;//卖家已发货
            private String tv_shop_produc_total_count;//在这个店铺买了多少件商品
            private String tv_shop_mailing_pay;//总消费多少

            public OrderListBean(int shop_img, String tv_shop_name, String
                    tv_shop_product_status, String tv_shop_produc_total_count, String
                                         tv_shop_mailing_pay) {
                this.shop_img = shop_img;
                this.tv_shop_name = tv_shop_name;
                this.tv_shop_product_status = tv_shop_product_status;
                this.tv_shop_produc_total_count = tv_shop_produc_total_count;
                this.tv_shop_mailing_pay = tv_shop_mailing_pay;
            }

            public int getShop_img() {
                return shop_img;
            }

            public String getTv_shop_name() {
                return tv_shop_name;
            }

            public String getTv_shop_product_status() {
                return tv_shop_product_status;
            }

            public String getTv_shop_produc_total_count() {
                return tv_shop_produc_total_count;
            }

            public String getTv_shop_mailing_pay() {
                return tv_shop_mailing_pay;
            }

            //            private String addressid;
//            private boolean canrefund;
//            private String cashier;
//            private String createtime;
//            private String express;
//            private String expresscom;
//            private String expresssn;
//            private String finishtime;
//            private int goodscount;
//            private String id;
//            private String iscomment;
//            private String isverify;
//            private String ordersn;
//            private String paytype;
//            private String price;
//            private int refund_status;
//            private String refundid;
//            private String refundstate;
//            private String sellername;
//            private String status;
//            private int user_status;
//            private String verified;
//            private String verifycode;
//            private String virtual;
            private List<GoodsBean> goods;

//            public String getAddressid() {
//                return addressid;
//            }
//
//            public void setAddressid(String addressid) {
//                this.addressid = addressid;
//            }
//
//            public boolean isCanrefund() {
//                return canrefund;
//            }
//
//            public void setCanrefund(boolean canrefund) {
//                this.canrefund = canrefund;
//            }
//
//            public String getCashier() {
//                return cashier;
//            }
//
//            public void setCashier(String cashier) {
//                this.cashier = cashier;
//            }
//
//            public String getCreatetime() {
//                return createtime;
//            }
//
//            public void setCreatetime(String createtime) {
//                this.createtime = createtime;
//            }
//
//            public String getExpress() {
//                return express;
//            }
//
//            public void setExpress(String express) {
//                this.express = express;
//            }
//
//            public String getExpresscom() {
//                return expresscom;
//            }
//
//            public void setExpresscom(String expresscom) {
//                this.expresscom = expresscom;
//            }
//
//            public String getExpresssn() {
//                return expresssn;
//            }
//
//            public void setExpresssn(String expresssn) {
//                this.expresssn = expresssn;
//            }
//
//            public String getFinishtime() {
//                return finishtime;
//            }
//
//            public void setFinishtime(String finishtime) {
//                this.finishtime = finishtime;
//            }
//
//            public int getGoodscount() {
//                return goodscount;
//            }
//
//            public void setGoodscount(int goodscount) {
//                this.goodscount = goodscount;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getIscomment() {
//                return iscomment;
//            }
//
//            public void setIscomment(String iscomment) {
//                this.iscomment = iscomment;
//            }
//
//            public String getIsverify() {
//                return isverify;
//            }
//
//            public void setIsverify(String isverify) {
//                this.isverify = isverify;
//            }
//
//            public String getOrdersn() {
//                return ordersn;
//            }
//
//            public void setOrdersn(String ordersn) {
//                this.ordersn = ordersn;
//            }
//
//            public String getPaytype() {
//                return paytype;
//            }
//
//            public void setPaytype(String paytype) {
//                this.paytype = paytype;
//            }
//
//            public String getPrice() {
//                return price;
//            }
//
//            public void setPrice(String price) {
//                this.price = price;
//            }
//
//            public int getRefund_status() {
//                return refund_status;
//            }
//
//            public void setRefund_status(int refund_status) {
//                this.refund_status = refund_status;
//            }
//
//            public String getRefundid() {
//                return refundid;
//            }
//
//            public void setRefundid(String refundid) {
//                this.refundid = refundid;
//            }
//
//            public String getRefundstate() {
//                return refundstate;
//            }
//
//            public void setRefundstate(String refundstate) {
//                this.refundstate = refundstate;
//            }
//
//            public String getSellername() {
//                return sellername;
//            }
//
//            public void setSellername(String sellername) {
//                this.sellername = sellername;
//            }
//
//            public String getStatus() {
//                return status;
//            }
//
//            public void setStatus(String status) {
//                this.status = status;
//            }
//
//            public int getUser_status() {
//                return user_status;
//            }
//
//            public void setUser_status(int user_status) {
//                this.user_status = user_status;
//            }
//
//            public String getVerified() {
//                return verified;
//            }
//
//            public void setVerified(String verified) {
//                this.verified = verified;
//            }
//
//            public String getVerifycode() {
//                return verifycode;
//            }
//
//            public void setVerifycode(String verifycode) {
//                this.verifycode = verifycode;
//            }
//
//            public String getVirtual() {
//                return virtual;
//            }
//
//            public void setVirtual(String virtual) {
//                this.virtual = virtual;
//            }


            public List<GoodsBean> getGoods() {
                return goods;
            }

            public void setGoods(List<GoodsBean> goods) {
                this.goods = goods;
            }

            public static class GoodsBean {

                private int iv_product_img;//商品的图片
                private String tv_product_describe;//描述我爱我家旗舰店的床上4件套我爱我家旗舰店的床上4件套
                private String tv_product_value; //尺寸  适用尺寸：1.8英寸
                private String tv_product_color;//颜色
                private String tv_product_fuli;//福利 7天退货  15天无条件换新
                private String tv_product_current_price;//商品当前价格（折后价）
                private String tv_product_original_price;//商品当前价格（原价）
                private String tv_product_count;//商品数量

                public GoodsBean(int iv_product_img, String tv_product_describe, String
                        tv_product_value, String tv_product_color, String tv_product_fuli, String
                        tv_product_current_price, String tv_product_original_price, String
                        tv_product_count) {
                    this.iv_product_img = iv_product_img;
                    this.tv_product_describe = tv_product_describe;
                    this.tv_product_value = tv_product_value;
                    this.tv_product_color = tv_product_color;
                    this.tv_product_fuli = tv_product_fuli;
                    this.tv_product_current_price = tv_product_current_price;
                    this.tv_product_original_price = tv_product_original_price;
                    this.tv_product_count = tv_product_count;
                }

                public int getIv_product_img() {
                    return iv_product_img;
                }

                public String getTv_product_describe() {
                    return tv_product_describe;
                }

                public String getTv_product_value() {
                    return tv_product_value;
                }

                public String getTv_product_color() {
                    return tv_product_color;
                }

                public String getTv_product_fuli() {
                    return tv_product_fuli;
                }

                public String getTv_product_current_price() {
                    return tv_product_current_price;
                }

                public String getTv_product_original_price() {
                    return tv_product_original_price;
                }

                public String getTv_product_count() {
                    return tv_product_count;
                }
                //                private String goodsid;
//                private String price;
//                private String s_price;
//                private String supplier_uid;
//                private String thumb;
//                private String title;
//                private String total;
//
//                public String getGoodsid() {
//                    return goodsid;
//                }
//
//                public void setGoodsid(String goodsid) {
//                    this.goodsid = goodsid;
//                }
//
//                public String getPrice() {
//                    return price;
//                }
//
//                public void setPrice(String price) {
//                    this.price = price;
//                }
//
//                public String getS_price() {
//                    return s_price;
//                }
//
//                public void setS_price(String s_price) {
//                    this.s_price = s_price;
//                }
//
//                public String getSupplier_uid() {
//                    return supplier_uid;
//                }
//
//                public void setSupplier_uid(String supplier_uid) {
//                    this.supplier_uid = supplier_uid;
//                }
//
//                public String getThumb() {
//                    return thumb;
//                }
//
//                public void setThumb(String thumb) {
//                    this.thumb = thumb;
//                }
//
//                public String getTitle() {
//                    return title;
//                }
//
//                public void setTitle(String title) {
//                    this.title = title;
//                }
//
//                public String getTotal() {
//                    return total;
//                }
//
//                public void setTotal(String total) {
//                    this.total = total;
//                }


            }
        }
    }
}
