package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.xzm.person.R;
import com.xzm.person.ui.bean.RecycleViewBean;

import java.util.List;

/**
 * Created by xuzongmeng on 2016/12/19.
 */

public class RecycleViewAdapter extends RecyclerAdapter<RecycleViewBean> {

    public RecycleViewAdapter(Context context, @Nullable List<RecycleViewBean> data, @NonNull int...
            layoutResIds) {
        super(context, data, layoutResIds);
    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, RecycleViewBean item) {
         helper.setText(R.id.tv_recyleview,item.getText());
    }
}
