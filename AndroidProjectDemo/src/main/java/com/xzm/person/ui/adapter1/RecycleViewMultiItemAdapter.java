package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;

import java.util.List;

/**
 * Created by xuzongmeng on 2016/10/10.
 */

public class RecycleViewMultiItemAdapter extends RecyclerAdapter<RecycleViewMultiItemBean> {

    public RecycleViewMultiItemAdapter(Context context, @Nullable List<RecycleViewMultiItemBean>
            data, @NonNull int... layoutResIds) {
        super(context, data, layoutResIds);

    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, RecycleViewMultiItemBean item) {
        KLog.d("适配器类型回调+getItemViewType");
        int position = helper.getAdapterPosition();
        KLog.d("适配器类型回调+position" + position);
        int itemViewType = getItemViewType(position);
//        if (itemViewType == 1) {
//            helper.setText(R.id.tv_user_name1, item.getUser_name())
//                    .setText(R.id.tv_phone_number1, item.getPhone_number());
//        } else if (itemViewType == 2) {
//            TextView tv_user_name2 = helper.getView(R.id.tv_user_name2);
//            KLog.d("适配器类型===tv_user_name2="+tv_user_name2);
//            helper.setText(R.id.tv_user_name2, item.getUser_name())
//                    .setText(R.id.tv_phone_number2, item.getPhone_number());
//        }

        if (itemViewType== 1) {
            helper.setText(R.id.tv_user_name1, item.getUser_name())
                    .setText(R.id.tv_phone_number1, item.getPhone_number());
        } else if (itemViewType == 2) {
            TextView tv_user_name2 = helper.getView(R.id.tv_user_name2);
            KLog.d("适配器类型===tv_user_name2="+tv_user_name2);
            helper.setText(R.id.tv_user_name2, item.getUser_name())
                    .setText(R.id.tv_phone_number2, item.getPhone_number());
        }
    }

    @Override
    public int getItemViewType(int position) {

        KLog.d("适配器类型11111====" + data.get(position).getStatus());
//        KLog.d("适配器类型回调+getItemViewType");
        if (data.get(position).getStatus()==1) {
            return 1;
        } else  {
            return 2;
        }

    }


//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (0 == viewType) {
//            return new EPGtitle_ViewHolder(mLayoutInflater.inflate(R.layout.item_epg_title, parent, false));
//        } else {
//            return new EPGitem_ViewHolder(mLayoutInflater.inflate(R.layout.item_epg, parent, false));
//        }
////
////        return super.onCreateViewHolder(parent, viewType);
//    }

    @Override
    public int getLayoutResId(int viewType) {
        KLog.d("适配器类型回调+getLayoutResId");
        if (viewType == 1) {
            return R.layout.item_recycleview_multi_style1;
        } else {
            return R.layout.item_recycleview_multi_style2;
        }
         }
    }







//    @Override
//    protected void convert(RecyclerAdapterHelper helper, RecycleViewMultiItemBean item) {
//        KLog.d("适配器类型回调+getItemViewType");
//        int position = helper.getAdapterPosition();
//        KLog.d("适配器类型回调+position" + position);
//        int itemViewType = getItemViewType(position);
//        if (itemViewType == 0) {
//            helper.setText(R.id.tv_user_name1, item.getUser_name())
//                    .setText(R.id.tv_phone_number1, item.getPhone_number())
//                    .setText(R.id.tv_user_qq1, item.getUser_QQ());
//        } else if (itemViewType == 1) {
//            helper.setText(R.id.tv_user_name2, item.getUser_name())
//                    .setText(R.id.tv_phone_number2, item.getPhone_number());
//        } else {
//            helper.setText(R.id.tv_trade_name, item.getUser_name());
//        }
//    }






//    @Override
//    public int getItemViewType(int position) {
//
//        KLog.d("适配器类型11111====" + data.get(position).getStatus());
////        KLog.d("适配器类型回调+getItemViewType");
//        if (data.get(position).getStatus() == 1) {
//            return 1;
//        } else if (data.get(position).getStatus() == 2) {
//            return 2;
//        } else {
//            return 3;
//        }
//
//    }
//
//
//    @Override
//    public int getLayoutResId(int viewType) {
//        KLog.d("适配器类型回调+getLayoutResId");
//        if (viewType == 1) {
//            return R.layout.item_recycleview_multi_style1;
//        } else if (viewType == 2) {
//            return R.layout.item_recycleview_multi_style2;
//        } else {
//            return R.layout.item_trade;
//        }
//    }

