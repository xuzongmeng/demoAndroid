package com.xzm.person.ui.activity1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.xzm.person.R;
import com.xzm.person.ui.fragment1.FragmentHome;
import com.xzm.person.utils.ThemeUtil;

import java.util.ArrayList;
import java.util.List;

import me.majiajie.pagerbottomtabstrip.Controller;
import me.majiajie.pagerbottomtabstrip.PagerBottomTabLayout;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectListener;


/**
 * Created by xuzongmeng on 2016/9/8.
 * 自定义底部按钮
 */
public class BottomTabActivity extends FragmentActivity {
    int[] testColors = {0xFF00796B, 0xFF5B4947, 0xFF607D8B, 0xFFF57C00, 0xFFF57C00};

    Controller controller;

    List<Fragment> mFragments;
    OnTabItemSelectListener listener = new OnTabItemSelectListener() {
        @Override
        public void onSelected(int index, Object tag) {
            Log.i("asd", "onSelected:" + index + "   TAG: " + tag.toString());

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            //transaction.setCustomAnimations(R.anim.push_up_in,R.anim.push_up_out);
            transaction.replace(R.id.framelayout, mFragments.get(index));
            transaction.commit();
        }

        @Override
        public void onRepeatClick(int index, Object tag) {
            Log.i("asd", "onRepeatClick:" + index + "   TAG: " + tag.toString());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_tab);
        //这里这样使用Fragment仅用于测试，请勿模仿！
        initFragment();
        BottomTabTest();
    }

    private void initFragment() {
        mFragments = new ArrayList<>();
        mFragments.add(creatFragment("A"));
        mFragments.add(creatFragment("B"));
        mFragments.add(creatFragment("C"));
        mFragments.add(creatFragment("D"));
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        // transaction.setCustomAnimations(R.anim.push_up_in,R.anim.push_up_out);
//        transaction.add(R.id.framelayout,mFragments.get(0));
//        transaction.commit();
    }

    private void BottomTabTest() {
        PagerBottomTabLayout pagerBottomTabLayout = (PagerBottomTabLayout) findViewById(R.id.tab);
        pagerBottomTabLayout.setBackgroundColor(ThemeUtil.getColor(R.color.circle));
        pagerBottomTabLayout.getAnimation();
        //用TabItemBuilder构建一个导航按钮
//        TabItemBuilder tabItemBuilder = new TabItemBuilder(this).create()
//                .setDefaultIcon(android.R.drawable.ic_menu_send)
//                .setText("信息")
//                .setSelectedColor(testColors[0])
//                .setTag("A")
//                .build();

        //构建导航栏,得到Controller进行后续控制
        controller = pagerBottomTabLayout.builder()

//                .addTabItem(tabItemBuilder)
//                .addTabItem(android.R.drawable.ic_menu_compass, "位置",testColors[1])
//                .addTabItem(android.R.drawable.ic_menu_search, "搜索",testColors[2])
//                .addTabItem(android.R.drawable.ic_menu_help, "帮助",testColors[3])

                .addTabItem(R.drawable.selector_home, "首页")
                .addTabItem(R.drawable.selector_manage_money, "理财")
                .addTabItem(R.drawable.selector_find, "发现")
                .addTabItem(R.drawable.selector_me, "我的")

//                .setMode(TabLayoutMode.CHANGE_BACKGROUND_COLOR)
//                .setMode(TabLayoutMode.CHANGE_BACKGROUND_COLOR)
//                .setMode(TabLayoutMode.HIDE_TEXT| TabLayoutMode.CHANGE_BACKGROUND_COLOR)

                .build();
        controller.setMessageNumber(2, 4);//设置消息数
        controller.setDisplayOval(1, true);

//        controller.setMessageNumber("A",2);
//        controller.setDisplayOval(0,true);

        controller.addTabItemClickListener(listener);
    }

    public Fragment creatFragment(String content) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        fragmentHome.setArguments(bundle);
        return fragmentHome;
    }
}
