package com.xzm.person.ui.widget1;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by xuzongmeng on 2016/10/12.
 * 滚动渐变颜色的ScrollView辅助
 */

public class TranslucentScrollView extends ScrollView {
    private OnScrollChangedListener mOnScrollChangedListener;

    public TranslucentScrollView(Context context) {
        super(context);
    }
    public TranslucentScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public TranslucentScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }
    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener = listener;
    }

    public interface OnScrollChangedListener {

        void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt);

    }

}
