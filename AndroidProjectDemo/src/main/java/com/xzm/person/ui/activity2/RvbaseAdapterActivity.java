package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.xzm.person.R;
import com.xzm.person.ui.adapter1.RvbaseAdapter;
import com.xzm.person.ui.bean.RecycleViewBean;

import java.util.ArrayList;
import java.util.List;

/**
 * --陈宇明RecycleView 支持拖拽 item动画 加载不同item 侧滑删除 多个头布局和脚布局
 */
public class RvbaseAdapterActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RvbaseAdapter mAdapter;
    private List<RecycleViewBean> mlist;

    private boolean isFristIn = false;
    Button btnToJump;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rv_base_adapter);
        initViews();
        initDtaes();
        isFristIn = true;

    }

    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        btnToJump = (Button) findViewById(R.id.btn_to_jump);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        btnToJump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFristIn = false;
//                startActivity(new Intent(RvbaseAdapterActivity.this, LoadingLayoutActivity.class));
            }
        });
    }

    private void initDtaes() {
        mlist = new ArrayList<>();
        mAdapter = new RvbaseAdapter(R.layout.item_recyclerview, mlist);
        mAdapter.openLoadAnimation();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

//        addDate();
//        mAdapter.notifyDataSetChanged();
       addBaseDate();

//        mAdapter.loadMoreEnd(true);

        mAdapter.disableLoadMoreIfNotFullPage(mRecyclerView);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        if (mlist.size()==50){
//                            mAdapter.loadMoreEnd();
//                            return;
//                        }
                        mAdapter.loadMoreEnd();
//                        addDate();
                    }
                }, 2000);
            }
        },mRecyclerView);
    }

    private void initAdapter() {

//        mAdapter.setNewData(mlist);
//        mAdapter.setNotDoAnimationCount(mFirstPageItemCount);

//        mRecyclerView.addOnItemTouchListener(new OnItemChildClickListener() {
//            @Override
//            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int
// position) {
//                String content = null;
//                Status status = (Status) adapter.getItem(position);
//                switch (view.getId()) {
//                    case R.id.img:
//                        content = "img:" + status.getUserAvatar();
//                        Toast.makeText(AnimationUseActivity.this, content, Toast.LENGTH_LONG)
// .show();
//                        break;
//                    case R.id.tweetName:
//                        content = "name:" + status.getUserName();
//                        Toast.makeText(AnimationUseActivity.this, content, Toast.LENGTH_LONG)
// .show();
//                        break;
//                    case R.id.tweetText:
//                        // you have set clickspan .so there should not solve any click event ,
// just empty
//                        break;
//                }
//
//            }
//        });

    }

//    @Override
//    protected void onResume() {
//        KLog.d("RvbaseAdapterActivity==重新回到焦点");
//        if (mAdapter != null && !isFristIn) {
//            if (mlist != null) {
//                for (int i = 0; i < 5; i++) {
//                    RecycleViewBean bean = new RecycleViewBean("我是新添数据" + i);
//                    mlist.add(bean);
//                }
//            }
//            KLog.d("RvbaseAdapterActivity==重新回到焦点刷新了 ");
//            mAdapter.notifyDataSetChanged();
//        }
//        super.onResume();
//    }

    private void addDate() {
        for (int i = 0; i < 10; i++) {
            RecycleViewBean bean = new RecycleViewBean("我是文本" + i);
            mlist.add(bean);
        }
        mAdapter.notifyDataSetChanged();
       mAdapter.loadMoreComplete();
    }



    private void addBaseDate() {
        for (int i = 0; i < 5; i++) {
            RecycleViewBean bean = new RecycleViewBean("我是文本" + i);
            mlist.add(bean);
        }
        mAdapter.notifyDataSetChanged();
        mAdapter.loadMoreComplete();
    }
}
