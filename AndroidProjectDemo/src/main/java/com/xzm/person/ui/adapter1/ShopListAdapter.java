package com.xzm.person.ui.adapter1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.pacific.adapter.RecyclerAdapter;
import com.pacific.adapter.RecyclerAdapterHelper;
import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.bean.Product;

import java.util.List;


/**
 * Created by xuzongmeng on 2016/12/30.
 * 商品 有哪些店家列表适配器
 */

public class ShopListAdapter extends RecyclerAdapter<Product.DataBean.OrderListBean> {

    public ShopListAdapter(Context context, @Nullable List<Product.DataBean.OrderListBean> data,
                           @NonNull int...
            layoutResIds) {

        super(context, data, layoutResIds);
    }

    @Override
    protected void convert(RecyclerAdapterHelper helper, Product.DataBean.OrderListBean item) {
        int position = helper.getAdapterPosition();
//        Product.DataBean.OrderListBean item = items.getData().getOrder_list().get(position);


        helper.setImageResource(R.id.iv_shop_img, item.getShop_img())
                .setText(R.id.tv_shop_name, item.getTv_shop_name())
                .setText(R.id.tv_shop_product_status, item.getTv_shop_product_status())
                .setText(R.id.tv_shop_produc_total_count, item.getTv_shop_produc_total_count())
                .setText(R.id.tv_shop_mailing_pay, item.getTv_shop_mailing_pay());

        KLog.d("店铺===的列表大小=="+item.getTv_shop_name());


        RecyclerView mRecyclerViewProductList = helper.getView(R.id.recycler_view_product_list);
        List<Product.DataBean.OrderListBean.GoodsBean> mProductList = item.getGoods();

//        KLog.d("店铺==商品的列表大小=="+mProductList.size());
        //设置RecycleView的布局管理器
        FullyLinearLayoutManager layoutManager = new FullyLinearLayoutManager(context,
                FullyLinearLayoutManager.VERTICAL, false);
        mRecyclerViewProductList.setLayoutManager(layoutManager);
        ProductListAdapter productListAdapter = new ProductListAdapter(context,
                mProductList, R.layout.item_product_shop_list);
        mRecyclerViewProductList.setAdapter(productListAdapter);

    }


    /**
     * private ImageView ivShopImg;
     private TextView tvShopName;
     private TextView tvShopProductStatus;
     private RecyclerView recyclerViewProductList;
     private TextView tvShopProducTotalCount;
     private TextView tvShopMailingPay;
     ivShopImg = (ImageView) findViewById(R.id.iv_shop_img);
     tvShopName = (TextView) findViewById(R.id.tv_shop_name);
     tvShopProductStatus = (TextView) findViewById(R.id.tv_shop_product_status);
     recyclerViewProductList = (RecyclerView) findViewById(R.id.recycler_view_product_list);
     tvShopProducTotalCount = (TextView) findViewById(R.id.tv_shop_produc_total_count);
     tvShopMailingPay = (TextView) findViewById(R.id.tv_shop_mailing_pay);

     */
}
