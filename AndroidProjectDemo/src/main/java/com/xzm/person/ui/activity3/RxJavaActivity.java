package com.xzm.person.ui.activity3;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.lidroid.xutils.util.LogUtils;
import com.xzm.person.R;

import java.io.File;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by xuzongmeng on 2018/2/10.
 */

public class RxJavaActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_java);
        init();
    }

    private void init() {
//        folder.listFiles()
//        File[] files = folder.listFiles();
//        File[] files = null;
////       int folders[] = new int[]{};
//        Observable.from(files).flatMap(new Func1<File, Observable<?>>() {
//            @Override
//            public Observable<?> call(File file) {
//                return Observable.from(files);
//            }
//        }).filter(new Func1<File, Boolean>() {
//
//            @Override
//            public Boolean call(File file) {
//                return file.getName().endsWith("");
//            }
//        }).map(new Func1<File, Bitmap>() {
//
//        });
        /**
         * 1) 创建 Observer
         Observer 即观察者，
         它决定事件触发的时候将有怎样的行为。
         RxJava 中的 Observer 接口的实现方式：
         */
        Observer stringObserver = new Observer<String>() {

            @Override
            public void onCompleted() {
                LogUtils.d("====onCompleted==");
            }

            @Override
            public void onError(Throwable e) {
                LogUtils.d("====onError==");
            }

            @Override
            public void onNext(String o) {
                LogUtils.d("====onNext==");
            }
        };


        /**
         * RxJava 还内置了一个实现了 Observer 的抽象类：
         * Subscriber。 Subscriber 对 Observer 接口进行了一些扩展，
         * 但他们的基本使用方式是完全一样的：
         */
        Subscriber<String> stringSubscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String o) {

            }
        };


        /**
         * 2) 创建 Observable
         Observable 即被观察者，它决定什么时候触发事件以及触发怎样的事件。
         RxJava 使用 create() 方法来创建一个 Observable ，并为它定义事件触发规则：
         */
        Observable.create(new Observable.OnSubscribe<String>() {

            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("Hello");
                subscriber.onNext("Hi");
                subscriber.onNext("Aloha");
                subscriber.onCompleted();
            }
        });

        /**
         * 基于这个方法， RxJava 还提供了一些方法用来快捷创建事件队列，例如：

         just(T...): 将传入的参数依次发送出来。
         */
        Observable observable = Observable.just("Hello", "Hi", "Aloha");
        // 将会依次调用：
// onNext("Hello");
// onNext("Hi");
// onNext("Aloha");
// onCompleted();

        /**
         *
         *  from(T[]) / from(Iterable<? extends T>) : 将传入的数组或 Iterable 拆分成具体对象后，依次发送出来。
         */
        String[] words = {"Hello", "Hi", "Aloha"};
        Observable observable1 = Observable.from(words);
        /**
         * 上面 just(T...) 的例子和 from(T[]) 的例子，都和之前的 create(OnSubscribe) 的例子是等价的。
         */
// 将会依次调用：
// onNext("Hello");
// onNext("Hi");
// onNext("Aloha");
// onCompleted();
        /**
         * 3) Subscribe (订阅)
         创建了 Observable 和 Observer 之后，再用 subscribe() 方法将它们联结起来，整条链子就可以工作了。代码形式很简单：

         observable.subscribe(observer);
         // 或者：
         observable.subscribe(subscriber)
         */
    }
}
