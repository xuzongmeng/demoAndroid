package com.xzm.person.ui.bean;


/**
 * Created by xuzongmeng on 2018/1/21.
 */

//@Data
//@AllArgsConstructor(suppressConstructorProperties = false)
public class DemoItem {
    private Class className;
    private String  description;

    public DemoItem(Class className, String description) {
        this.className = className;
        this.description = description;
    }

    public Class getClassName() {
        return className;
    }

    public void setClassName(Class className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
