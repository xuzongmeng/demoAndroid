package com.xzm.person.ui.adapter1;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xzm.person.R;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;

import java.util.List;

/**
 * Created by xuzongmeng on 2017/1/13.
 */

public class RvbaseMultleAdapter extends BaseMultiItemQuickAdapter<RecycleViewMultiItemBean,BaseViewHolder> {


    public RvbaseMultleAdapter(List<RecycleViewMultiItemBean> data) {
        super(data);
        addItemType(1, R.layout.item_recycleview_multi_style1);
        addItemType(2, R.layout.item_recycleview_multi_style2);

    }

    @Override
    protected void convert(BaseViewHolder holder, RecycleViewMultiItemBean bean) {


        int viewType = holder.getItemViewType();
        if (viewType == 1) {
            holder.setText(R.id.tv_user_name1, bean.getUser_name())
                    .setText(R.id.tv_phone_number1,bean.getPhone_number());

        } else {
            holder.setText(R.id.tv_user_name2, bean.getUser_name())
                    .setText(R.id.tv_phone_number2,bean.getPhone_number());
        }
    }
}
