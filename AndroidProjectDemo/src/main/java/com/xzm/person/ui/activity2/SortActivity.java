package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xzm.person.R;
import com.xzm.person.ui.bean.People;
import com.xzm.person.ui.widget2.sort.DropDownMenu;
import com.xzm.person.ui.widget2.sort.SortContentAdapter;
import com.xzm.person.utils.ModelUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xuzongmeng on 2017/8/21.
 */

public class SortActivity extends AppCompatActivity {
//"自定义", "双列表"
    DropDownMenu mDropDownMenu;
    private String headers[] = {"城市", "年龄", "星座", "排序"};
    private String[] types = new String[]{DropDownMenu.TYPE_CITY, DropDownMenu.TYPE_AGE, DropDownMenu.TYPE_GRID, DropDownMenu
            .TYPE_SORT, DropDownMenu.TYPE_CUSTOM, DropDownMenu.TYPE_TWO_LISTVIEW};
    private String citys[] = {"不限", "北京", "上海", "广州", "深圳"};
    private String ages[] = {"不限", "18岁以下", "18-30岁", "31-40岁", "40岁以上"};
    private String constellations[] = {"不限", "白羊座", "金牛座", "双子座", "巨蟹座",};
    private String sort[] = {"不限", "生日升序", "生日将序", "年龄升序", "年龄将序"};
    private SortContentAdapter mPeopleAdapter;
    private List<People> mPeopleDataList;
    private List<People> tempList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort);
        mDropDownMenu = (DropDownMenu) findViewById(R.id.dropDownMenu);
        initView();
    }


    private  boolean isFristFilter=true;
    private void initView() {

        View contentView = getLayoutInflater().inflate(R.layout.sort_contentview, null);
        ListView lvContent = (ListView) contentView.findViewById(R.id.lv_content);
        mPeopleAdapter = new SortContentAdapter(this, R.layout.ltem_sort);
        lvContent.setAdapter(mPeopleAdapter);
        tempList = new ArrayList<>();

        final Map<String, String> ruleMap=new HashMap<>();
        mDropDownMenu.setDropDownMenu(Arrays.asList(headers), initViewData(), contentView);
        //该监听回调只监听默认类型，如果是自定义view请自行设置，参照demo
        mDropDownMenu.addMenuSelectListener(new DropDownMenu.OnDefultMenuSelectListener() {
            @Override
            public void onSelectDefaultMenu(int index, int pos, String clickstr) {
                //index:点击的tab索引，pos：单项菜单中点击的位置索引，clickstr：点击位置的字符串
                Toast.makeText(getBaseContext(), "index=" + index + "，条件=" + clickstr, Toast.LENGTH_SHORT).show();

                    if (index == 0) {
                        ruleMap.put(DropDownMenu.TYPE_CITY,clickstr);
                        tempList = ModelUtil.getFilterDataByCity(mPeopleDataList, ruleMap);
                    } else if (index == 1) {
                        ruleMap.put(DropDownMenu.TYPE_AGE,clickstr);
                        tempList = ModelUtil.getFilterDataByAge(mPeopleDataList, ruleMap);
                    } else if (index == 2) {
                        ruleMap.put(DropDownMenu.TYPE_GRID,clickstr);
                        tempList = ModelUtil.getFilterDataByConstellation(mPeopleDataList, ruleMap);
                    } else if (index == 3) {
                        ruleMap.put(DropDownMenu.TYPE_SORT,clickstr);
                        tempList = ModelUtil.getFilterDataBySort(mPeopleDataList, ruleMap);
                    }
                System.out.println("====过滤"+ tempList.size());
                mPeopleAdapter.clear();
                mPeopleAdapter.addAll(tempList);




            }
        });
        mPeopleDataList = ModelUtil.getPeopleData();
        mPeopleAdapter.addAll(mPeopleDataList);
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.length; i++) {
            map = new HashMap<>();
            map.put(DropDownMenu.KEY, types[i]);
            switch (types[i]) {
                case DropDownMenu.TYPE_CITY:
                    map.put(DropDownMenu.VALUE, citys);
//                    map.put(DropDownMenu.SELECT_POSITION,0);
                    break;
                case DropDownMenu.TYPE_AGE:
                    map.put(DropDownMenu.VALUE, ages);
//                    map.put(DropDownMenu.SELECT_POSITION,0);
                    break;
                case DropDownMenu.TYPE_GRID:
                    map.put(DropDownMenu.VALUE, constellations);
//                    map.put(DropDownMenu.SELECT_POSITION,0);
                    break;
                case DropDownMenu.TYPE_SORT:
                    map.put(DropDownMenu.VALUE, sort);
//                    map.put(DropDownMenu.SELECT_POSITION,0);
                    break;
                case DropDownMenu.TYPE_CUSTOM:
                    map.put(DropDownMenu.VALUE, getCustomView());
                    break;
                case DropDownMenu.TYPE_TWO_LISTVIEW:
                    map.put(DropDownMenu.VALUE, getCustomView());
                    break;
                default:
                    break;
            }
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView() {
        View v = getLayoutInflater().inflate(R.layout.sort_custom, null);
        TextView btn = (TextView) v.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDropDownMenu.setTabText(2, "自定义");//设置tab标签文字
                mDropDownMenu.closeMenu();//关闭menu

                Toast.makeText(SortActivity.this, "自定义", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    @Override
    public void onBackPressed() {
        //退出activity前关闭菜单
        if (mDropDownMenu.isShowing()) {
            mDropDownMenu.closeMenu();
        } else {
            super.onBackPressed();
        }
    }
}
