package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.xzm.person.R;
import com.xzm.person.ui.adapter1.ViewPagerAdapter;
import com.xzm.person.ui.fragment1.FragmentHome;
import com.xzm.person.utils.utils2.BottomNavigationViewHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruce on 2016/11/1.
 * HomeActivity 主界面
 */

public class SupportDesignBottomTabActivity2 extends AppCompatActivity {
    private List<Fragment> mFragmentList=new ArrayList<>();
    private ViewPager viewPager;
    private MenuItem menuItem;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_design_bottom_tab2);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        //默认 >3 的选中效果会影响ViewPager的滑动切换时的效果，故利用反射去掉
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_call:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.item_mail:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.item_person:
                                viewPager.setCurrentItem(2);
                                break;
//                            case R.id.item_more:
//                                viewPager.setCurrentItem(3);
//                                break;
                        }
                        return false;
                    }
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                menuItem = bottomNavigationView.getMenu().getItem(position);
                menuItem.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //禁止ViewPager滑动
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(),mFragmentList,null);
        mFragmentList.add(creatFragment("首页"));
        mFragmentList.add(creatFragment("理财"));
        mFragmentList.add(creatFragment("其他"));
//        adapter.addFragment(BaseFragment.newInstance("新闻"));
//        adapter.addFragment(BaseFragment.newInstance("图书"));
//        adapter.addFragment(BaseFragment.newInstance("发现"));
//        adapter.addFragment(BaseFragment.newInstance("更多"));
        viewPager.setAdapter(adapter);
    }

    private Fragment creatFragment(String content) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        fragmentHome.setArguments(bundle);
        return fragmentHome;
    }
}