package com.xzm.person.ui.activity2;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.utils.ImageTools;
import com.xzm.person.utils.alximageloader.SelectPhotoActivity;
import com.xzm.person.utils.alximageloader.SelectPhotoAdapter;

import java.util.ArrayList;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static com.xzm.person.app.Myapplication.mContext;

/**
 * 选择相册
 */
public class ChoosePhotoActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvUploadIDCardPositivePage;
    private Button btnRetryUploadPostiveIDCardPage;
    private TextView tvUploadIDCardNegativePage;
    private Button btnRetryUploadIDCardNegativePage;
    private ImageView ivShowIDCardPositivePage;
    private ImageView ivShowIDCardNegativePage;
    //    private  boolean isUploadNegativeIDCard=false;//是否上传过身份证正面
    private boolean uploadIDCardPositivePage = false;//是否上传过身份证正反面标记
    private int uploadIDCardFlag = 1; //1表示点击上传正面 2 表示反面

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_photo);
        initView();
    }

    private void initView() {
        tvUploadIDCardPositivePage = (TextView) findViewById(R.id.tv_upload_IDCard_positive_page);
        tvUploadIDCardNegativePage = (TextView) findViewById(R.id.tv_upload_IDCard_negative_page);
        btnRetryUploadPostiveIDCardPage = (Button) findViewById(R.id
                .btn_retry_upload_postive_IDCard_page);
        btnRetryUploadIDCardNegativePage = (Button) findViewById(R.id
                .btn_retry_upload_IDCard_negative_page);
        ivShowIDCardPositivePage = (ImageView) findViewById(R.id.iv_show_IDCard_positive_page);
        ivShowIDCardNegativePage = (ImageView) findViewById(R.id.iv_show_IDCard_negative_page);

        tvUploadIDCardPositivePage.setOnClickListener(this);
        tvUploadIDCardNegativePage.setOnClickListener(this);
        btnRetryUploadIDCardNegativePage.setOnClickListener(this);
        btnRetryUploadPostiveIDCardPage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_retry_upload_IDCard_negative_page:
                checkReadExternalStoragePermission();
                break;
            case R.id.btn_retry_upload_postive_IDCard_page:
                checkReadExternalStoragePermission();
                break;
            case R.id.tv_upload_IDCard_positive_page:
                uploadIDCardFlag = 1;
                checkReadExternalStoragePermission();
                break;
            case R.id.tv_upload_IDCard_negative_page:
                uploadIDCardFlag = 2;
                checkReadExternalStoragePermission();
                break;
        }
    }



//
//    private void goPhoto() {
//        Intent intent = new Intent(this, SelectPhotoActivity.class);
//        startActivityForResult(intent, 10);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("Alex", "mainActivity的onActivityResult req=" + requestCode + "    result=" + requestCode);
        if (data == null || resultCode != SelectPhotoActivity.SELECT_PHOTO_OK) return;
        boolean isFromCamera = data.getBooleanExtra("isFromCamera", false);
        ArrayList<SelectPhotoAdapter.SelectPhotoEntity> selectedPhotos = data
                .getParcelableArrayListExtra("selectPhotos");
        String url = null;
        for (int i = 0; i < selectedPhotos.size(); i++) {
            KLog.d("选择的图片是" + selectedPhotos.get(i).url);
             url = selectedPhotos.get(i).url;
        }
//        ImageTools.getAbsolutePath()
//        Uri  uri= Uri.parse(url);
        KLog.d("选择的图片是== uriuriuriuriuri" + url);
        if (!TextUtils.isEmpty(url)) {
            Bitmap bitmap = ImageTools.rotateBitmap(url, 200, 200);//压缩之后再上传
            if (uploadIDCardFlag == 1) {
//                Picasso.with(this).load(new File(url)).into(ivShowIDCardPositivePage);

//                String absolutePath = ImageTools.getAbsolutePath(this, url);
//                KLog.d("选择的图片真实路径======= absolutePath" + absolutePath);





                tvUploadIDCardPositivePage.setVisibility(View.GONE);
                ivShowIDCardPositivePage.setVisibility(View.VISIBLE);
                btnRetryUploadPostiveIDCardPage.setVisibility(View.VISIBLE);
                ivShowIDCardPositivePage.setImageBitmap(bitmap);

            } else {
//                Picasso.with(this).load(url).into(ivShowIDCardNegativePage);

                tvUploadIDCardNegativePage.setVisibility(View.GONE);
                ivShowIDCardNegativePage.setVisibility(View.VISIBLE);
                btnRetryUploadIDCardNegativePage.setVisibility(View.VISIBLE);
                ivShowIDCardNegativePage.setImageBitmap(bitmap);

            }
        }
//        if (!TextUtils.isEmpty(url)) {
////            Picasso.with(this)
////                    .load(url)
////                    .into(ivShowIDCardPositivePage);
////            File directory = Environment.getExternalStorageDirectory();
////
////            Picasso.with(getBaseContext()).load(file).into(targetImageView2);
//            File file = new File(url);
//            String absolutePathxxxxxxxxxx = file.getAbsolutePath();
//
//
//            KLog.d("选择的图片真实路径=xxxxxxxxxxxxxxxxxx====== absolutePathxxxxxxxxxx" + absolutePathxxxxxxxxxx);
//            Glide.with(this).load(absolutePathxxxxxxxxxx).into(ivShowIDCardPositivePage);
////            ivShowIDCardPositivePage.setImageBitmap(getSmallBitmap(url));
//        }


        KLog.d("选择的图片是xxxxxxxxxxxxxxxxx" + isFromCamera);

    }


    public static Bitmap getSmallBitmap(String path){

        BitmapFactory.Options options=new BitmapFactory.Options();

        options.inJustDecodeBounds=true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, 320, 480);

        options.inJustDecodeBounds = false;

        Bitmap bitmap= BitmapFactory.decodeFile(path, options);

        return bitmap;

    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqHeight, int reqWidth){

        int height=options.outHeight;
        int width=options.outWidth;
        int inSampleSize=1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;

    }
    private static final int READ_EXTERNAL_STORAGE = 1;
    @AfterPermissionGranted(READ_EXTERNAL_STORAGE)
    private void checkReadExternalStoragePermission() {
        /**
         * 如果有权限
         */
        if (EasyPermissions.hasPermissions(mContext, Manifest.permission.READ_EXTERNAL_STORAGE))
        {//检查SD卡权限
            Intent intent = new Intent(this, SelectPhotoActivity.class);
            startActivityForResult(intent, 10);
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, "我们需要使用你手机的储存功能，请允许打开您的储存权限",
                    READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


//
//    @Override
//    public void onPermissionsGranted(int requestCode, List<String> perms) {
//
//    }
//
//    @Override
//    public void onPermissionsDenied(int requestCode, List<String> perms) {
//
//    }
}
