package com.xzm.person.ui.activity2;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ScrollView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.ui.widget1.TranslucentScrollView;


/**
 * Created by xuzongmeng on 2016/10/12.
 * 随ScrollView滚动改变ToolBar改变颜色
 */

public class ScrollChangeColorActivity extends AppCompatActivity implements TranslucentScrollView.OnScrollChangedListener {

    /**
     *  如果知道ARGB的取值，那么可以使用Color类的静态方法argb创建一个颜色：
     Int color = Color.argb(127,255,0,255)；// 半透明的紫色
     真正的紫色argb(255,0,255)
     */
    private TranslucentScrollView scrollView;
    private Toolbar toolbar;
    private float headerHeight;//顶部高度
    private float minHeaderHeight;//顶部最低高度，即Bar的高度
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 setContentView(R.layout.activity_scroll_changecolor);
        initView();
    }
    private void initView() {
        scrollView = (TranslucentScrollView) findViewById(R.id.scrollview);
        scrollView.setOnScrollChangedListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        RGB(35,38,47)
//        toolbar.setBackgroundColor(Color.argb(0, 35, 38, 47));
        toolbar.setBackgroundColor(Color.argb(255, 35, 38, 47));
        initMeasure();
    }

    private void initMeasure() {
        headerHeight = getResources().getDimension(R.dimen.dimen_300);
        minHeaderHeight = getResources().getDimension(R.dimen.abc_action_bar_default_height_material);

    }


    @Override

    public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
        //Y轴偏移量
        float scrollY = who.getScrollY();
        KLog.d("偏移量===="+who);
        //变化率
        float headerBarOffsetY = headerHeight - minHeaderHeight;//Toolbar与header高度的差值
        float offset = 1 - Math.max((headerBarOffsetY - scrollY) / headerBarOffsetY, 0f);
        KLog.d("偏移量1111===="+offset);
        //Toolbar背景色透明度
        toolbar.setBackgroundColor(Color.argb((int) (offset * 255), 35, 38, 47));

//        //header背景图Y轴偏移

//        imgHead.setTranslationY(scrollY / 2);

    }

}
