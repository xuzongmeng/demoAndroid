package com.xzm.person.ui.activity2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.socks.library.KLog;
import com.xzm.person.R;
import com.xzm.person.app.Constant;
import com.xzm.person.ui.bean.TestBean;
import com.xzm.person.utils.DeviceUtil;
import com.xzm.person.utils.DiskCacheManager;
import com.xzm.person.utils.DiskLruCache;
import com.xzm.person.utils.Md5Utils;
import com.xzm.person.utils.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class DiskLruCacheActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btnDiskLruCache;
    private Button btnDiskLruCache1;
    private Button btnReadDiskLruCache;
    private TextView tvShowCache;


    private DiskLruCache mDiskLruCache;
    private DiskCacheManager diskCacheManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disk_lru_cache);


        btnDiskLruCache = (Button) findViewById(R.id.btn_diskLruCache);
        btnDiskLruCache1 = (Button) findViewById(R.id.btn_diskLruCache1);
        btnReadDiskLruCache = (Button) findViewById(R.id.btn_read_diskLruCache);
        tvShowCache = (TextView) findViewById(R.id.tv_show_cache);
        btnDiskLruCache.setOnClickListener(this);
        btnDiskLruCache1.setOnClickListener(this);
        btnReadDiskLruCache.setOnClickListener(this);
        init();
    }

    private void init() {
        int versionCode = DeviceUtil.getCurrVersionCode(this);
        File file = DeviceUtil.getDiskCacheDir(this, "xzm");
        long cacheSize = 100 * 1024 * 1024;//最大缓存为100M
        try {
            mDiskLruCache = DiskLruCache.open(file, versionCode, 1, cacheSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_diskLruCache:
//                cachaDate();
                TestBean testBean = new TestBean("徐", "男");
                cacheDate(testBean);


                TestBean testBean1 = new TestBean("徐", "男");
                cacheDate(testBean);
                diskCacheManager = new DiskCacheManager(this, "xzm");
                diskCacheManager.put(Constant.TEST_CACHE_KEY, testBean1);
                break;
            case R.id.btn_diskLruCache1:
                break;
            case R.id.btn_read_diskLruCache:

                ToastUtil.showToast("读取缓存");
//                TestBean cache = getloadCache();
                TestBean jsonObject = diskCacheManager.getSerializable(Constant.TEST_CACHE_KEY);
                if (jsonObject != null) {
                    tvShowCache.setText(jsonObject.getName());
                }
                break;
        }
    }

    private void cachaDate() {
        ToastUtil.showToast("缓存数据==");
        try {
            DiskLruCache.Editor editor = mDiskLruCache.edit(Constant.TEST_CACHE_KEY);
            TestBean testBean = new TestBean("小徐", "nan");
            editor.set(0, testBean.toString());
//            OutputStream outputStream = editor.newOutputStream(0);
            editor.commit();

            DiskLruCache.Snapshot snapshot = mDiskLruCache.get(Constant.TEST_CACHE_KEY);
            String string = snapshot.getString(0);
//            TestBean obj = DataHelper.json2Obj(string, TestBean.class);
            KLog.d("缓存成功===" + string.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void cacheDate(TestBean stories) {

//        int versionCode = DeviceUtil.getCurrVersionCode(this);
//        File cacheFile = DeviceUtil.getDiskCacheDir(this, "xzm");

//        File cacheFile = DiskCacheUtil.getCacheFile(this, Constant.TEST_CACHE_KEY);
//        DiskLruCache diskLruCache = DiskCacheUtil.instance(cacheFile);
        try {
            //使用MD5加密后的字符串作为key，避免key中有非法字符
            String key = Md5Utils.string2MD5(Constant.TEST_CACHE_KEY);
            DiskLruCache.Editor editor = mDiskLruCache.edit(key);
            if (editor != null) {
                ObjectOutputStream outputStream = new ObjectOutputStream(editor.newOutputStream(0));
                outputStream.writeObject(stories);
                outputStream.close();
                editor.commit();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TestBean getloadCache() {
//        File cacheFile = DiskCacheUtil.getCacheFile(MyApplication.getContext(), Constants
// .ZHIHUCACHE);
//        DiskLruCache diskLruCache = DiskCacheUtil.instance(cacheFile);
        String key = Md5Utils.string2MD5(Constant.TEST_CACHE_KEY);
        try {
            DiskLruCache.Snapshot snapshot = mDiskLruCache.get(key);
            if (snapshot != null) {
                InputStream in = snapshot.getInputStream(0);
                ObjectInputStream ois = new ObjectInputStream(in);
                try {
                    TestBean stories = (TestBean) ois.readObject();
                    KLog.d("缓存==xxx===" + stories.getName());
                    if (stories != null) {
                        KLog.d("缓存=====" + stories.getName());
                        return stories;
                    } else {
                        return null;
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


//    public void loadCache() {
//        File cacheFile = DiskCacheUtil.getCacheFile(MyApplication.getContext(), Constants
// .ZHIHUCACHE);
//        DiskLruCache diskLruCache = DiskCacheUtil.instance(cacheFile);
//        String key = SecretUtil.getMD5Result(Constants.ZHIHUSTORY_KEY);
//        try {
//            DiskLruCache.Snapshot snapshot = diskLruCache.get(key);
//            if (snapshot != null) {
//                InputStream in = snapshot.getInputStream(0);
//                ObjectInputStream ois = new ObjectInputStream(in);
//                try {
//                    ArrayList<ZhihuStory> stories = (ArrayList<ZhihuStory>) ois.readObject();
//                    if (stories != null) {
//                        mINewsListActivity.getDataSuccess(stories);
//                    } else {
//                        mINewsListActivity.getDataFail("", "无数据");
//                    }
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


//
//    private void makeCache(ArrayList<ZhihuStory> stories) {
//        File cacheFile = DiskCacheUtil.getCacheFile(MyApplication.getContext(), Constants
// .ZHIHUCACHE);
//        DiskLruCache diskLruCache = DiskCacheUtil.instance(cacheFile);
//        try {
//            //使用MD5加密后的字符串作为key，避免key中有非法字符
//            +String key = SecretUtil.getMD5Result(Constants.ZHIHUSTORY_KEY);
//            DiskLruCache.Editor editor = diskLruCache.edit(key);
//            if (editor != null) {
//                ObjectOutputStream outputStream = new ObjectOutputStream(editor.newOutputStream
// (0));
//                outputStream.writeObject(stories);
//                outputStream.close();
//                editor.commit();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


}
