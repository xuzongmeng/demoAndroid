package com.xzm.person.ui.widget2.sort;

import android.content.Context;
import android.support.annotation.NonNull;

import com.pacific.adapter.Adapter;
import com.pacific.adapter.AdapterHelper;
import com.xzm.person.R;
import com.xzm.person.ui.bean.People;

/**
 * Created by xuzongmeng on 2017/8/22.
 */

public class SortContentAdapter extends Adapter<People> {
    public SortContentAdapter(Context context, @NonNull int... layoutResIds) {
        super(context, layoutResIds);
    }

    @Override
    protected void convert(AdapterHelper helper, People item) {
        helper.setText(R.id.tv_sort_city, item.getCity())
                .setText(R.id.tv_sort_age, item.getAge() + "")
                .setText(R.id.tv_sort_birthday, item.getBirthday())
                .setText(R.id.tv_sort_constellation, item.getConstellation());
    }
}
