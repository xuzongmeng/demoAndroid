package com.xzm.person.ui.activity1;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import com.xzm.person.R;
import com.xzm.person.ui.adapter1.ListViewLoadMoreAdapter;
import com.xzm.person.ui.bean.TestBean;
import com.xzm.person.ui.widget1.LoadMoreListView;
import com.xzm.person.utils.ToastUtil;

import java.util.ArrayList;

/**
 * Created by xuzongmeng on 2016/11/9.
 */

public class ActivityLoadmoreListview extends Activity {

     private ArrayList<TestBean> mList;
    private ListViewLoadMoreAdapter loamoreAdapter;
    private LoadMoreListView loadmorelistviewTest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loadmore_listview);
        loadmorelistviewTest = findViewById(R.id.loadmorelistview_test);
        init();


    }

    private void init() {
        mList=new ArrayList<>();
//        ArrayList<TestBean> testbeen = addDate();
//        mList.add(testbeen);
        for (int i = 0; i < 10; i++) {
            TestBean bean=new TestBean("开始"+i,"x");
            mList.add(bean);
        }

        loamoreAdapter = new ListViewLoadMoreAdapter(this,mList);
         loadmorelistviewTest.setAdapter(loamoreAdapter);
        loadmorelistviewTest.setLoadMoreListen(new LoadMoreListView.OnLoadMore() {
            @Override
            public void loadMore() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtil.showToast("加载完成");
                        for (int i = 0; i < 10; i++) {
                            TestBean bean=new TestBean("更多"+i,"nan");
                            mList.add(bean);
                            loamoreAdapter.notifyDataSetChanged();
                            loadmorelistviewTest.onLoadComplete();
                        }

                    }
                },1000);
            }
        });
    }


    private ArrayList<TestBean> addDate(){
        for (int i = 0; i < 10; i++) {
            TestBean bean=new TestBean("这是"+i,"nan");
            mList.add(bean);
        }
        return   mList;
    }
}
