package com.xzm.person.ui.adapter1;

import android.view.View;
import android.widget.Toast;

import com.xzm.person.R;
import com.xzm.person.app.Myapplication;
import com.xzm.person.ui.bean.RecycleViewMultiItemBean;
import com.xzm.person.ui.widget1.SwipeMenuView;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by xuzongmeng on 2016/11/10.
 */

public class HongYangAdapterItemOne implements ItemViewDelegate<RecycleViewMultiItemBean> {
    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_recycleview_multi_style1;
    }

    @Override
    public boolean isForViewType(RecycleViewMultiItemBean item, int position) {
        if (item.getStatus()==1){
            return  true;
        }else {
            return  false;
        }

    }

    @Override
    public void convert(final ViewHolder holder, RecycleViewMultiItemBean testbean, int position) {
//     /   holder.getConvertView().setEnabled(false);
        holder.itemView.setEnabled(false);
        ((SwipeMenuView) holder.itemView).setIos(true).setLeftSwipe(true).setSwipeEnable(true);

        SwipeMenuView swipe_menu_view = holder.getView(R.id.swipe_menu_view);
        swipe_menu_view.setonSwipeLister(new SwipeMenuView.OnSwipeLister() {
            @Override
            public void onSwiped(boolean isSwipe) {
                if (isSwipe){
//                    holder.getConvertView().setEnabled(false);
                    holder.itemView.setEnabled(false);
                }else {
//                    holder.getConvertView().setEnabled(true);
                    holder.itemView.setEnabled(true);
                }
            }
        });


//        if (position==0){
//            ((SwipeMenuLayout) holder.itemView).setIos(true).setLeftSwipe(true);
//        }else if (position==1){
//            ((SwipeMenuLayout) holder.itemView).setIos(true).setLeftSwipe(false);
//        }
//
//        SwipeMenuLayout itemView = (SwipeMenuLayout) holder.itemView;
//        holder.getConvertView().setEnabled(false);


        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Myapplication.getContext(), "整个Item被点击", Toast.LENGTH_SHORT).show();
            }
        });
        holder.setOnClickListener(R.id.tv_user_name1, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Myapplication.getContext(), "控件被点", Toast.LENGTH_SHORT).show();
            }
        });

        holder.getView(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Myapplication.getContext(), "侧滑被点", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
