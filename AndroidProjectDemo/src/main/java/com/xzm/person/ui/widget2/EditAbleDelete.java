package com.xzm.person.ui.widget2;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.xzm.person.R;
import com.xzm.person.utils.ThemeUtil;


/**
 * 给EditView中的图片添加点击清除效果
 */

public class EditAbleDelete extends AppCompatEditText {

    public String defaultValue = "";
    final Drawable imgX = ThemeUtil.getDrawable(
            R.mipmap.ic_share_white);

    public EditAbleDelete(Context context) {
        super(context);
        init();
    }

    public EditAbleDelete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    public EditAbleDelete(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    void init() {
        imgX.setBounds(0, 0, imgX.getIntrinsicWidth(),imgX.getIntrinsicHeight());
        manageClearButton();

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                EditAbleDelete et = EditAbleDelete.this;

                if (et.getCompoundDrawables()[2] == null)
                    return false;

                if (event.getAction() != MotionEvent.ACTION_UP)
                    return false;
                // Is touch on our clear button?
                if (event.getX() > et.getWidth() - et.getPaddingRight()- imgX.getIntrinsicWidth()) {
                    et.setText("");
                    EditAbleDelete.this.removeClearButton();
                }
                return false;
            }
        });

        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                EditAbleDelete.this.manageClearButton();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                EditAbleDelete.this.manageClearButton();
            }
        });
    }

    void manageClearButton() {
        if (this.getText().toString().equals("") || !this.isFocused()) {
            removeClearButton();
        } else {
            addClearButton();
        }
    }

    void addClearButton() {
        this.setCompoundDrawables(this.getCompoundDrawables()[0],
                this.getCompoundDrawables()[1], imgX,
                this.getCompoundDrawables()[3]);
    }

    void removeClearButton() {
        this.setCompoundDrawables(this.getCompoundDrawables()[0],
                this.getCompoundDrawables()[1], null,
                this.getCompoundDrawables()[3]);
    }
}