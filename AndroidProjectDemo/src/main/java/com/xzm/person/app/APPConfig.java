package com.xzm.person.app;

import android.os.Environment;

/**
 * Created by cm on 2016/6/7.
 */
public class APPConfig {
    /***
     * 获取文件的相对路径
     */

    public  static  boolean  DEBUG= true;
    public static String getFilePathInSDCard(String dir, String fileName) {
        return Environment.getExternalStorageDirectory() + "/" + dir + java.io.File.separator + fileName;
    }

    public static class Network {
        /**
         * 测试环境
         */
//        public static String SOCKET = "192.168.83.221";
        public static final String XIANHUOBAO_CACHE = "www";
        public static final String APP_UPDATA_NAME = "AndroidApkUpdate";

        /**
         * 测试环境
         */
//       public static String DOMAIN = "http://test.appserver.com/";
        /**
         * 生产环境
         */
        public static String SOCKET = "139.196.214.10";
        public static int PORT = 8815;
        /**
         * 生产环境
         */
        public static String DOMAIN = "http://app.jindinghui.com.cn/";
    }

    public static class Html {
        /**
         * 关于我们
         */
        public static final String ABOUT = Network.DOMAIN + "mine/aboutus?style=0";
        /**
         * 实时资讯
         */
        public static final String REALTIME = Network.DOMAIN + "news/realtimeList?style=0";
        /**
         * 重大数据
         */
        public static final String GREAT = Network.DOMAIN + "news/greatList?style=0";
        /**
         * 免责声明
         */
        public static final String DISCLAIMER = Network.DOMAIN + "mine/disclaimer?style=0";

        /**
         * 帮助中心
         */
        public static final String HELP_CENTER = Network.DOMAIN + "mine/basicsList?style=0";

        /**
         * 联系客服
         */
        public static final String CONTACT_MY_SERVICE = Network.DOMAIN + "/mine/contactUs?";

        /**
         * 现货宝下载地址
         */
        public static final String DOWNLOAD_XHB = Network.DOMAIN + "mine/download";

        /**
         * 长江交易宝下载地址
         */
        public static final String DOWNLOAD_CJJYB = Network.DOMAIN + "mine/jiaoyibao";
    }

    /**
     * 按照接口名为文件名进行缓存，方便管理
     */
    public static class File {
        /**
         * 用户信息缓存文件
         */
        public static final String USER_INFO = "user_info";
        /**
         * 表示是否设备已注册token
         */
        public static final String TOKEN_REGISTER = "register_token";
        /**
         * Token缓存文件
         */
        public static final String TOKEN_CACHE = "token_cache";
        /**
         * topindexData接口缓存文件
         */
        public static final String TOP_INDEX_DATA = "topIndexData";

        /**
         * getImportMessage接口缓存文件
         */
        public static final String IMPORT_MESSAGE = "importMessage";
        /**
         * getTeacherTopList接口缓存文件
         */
        public static final String TEACHER_TOP_LIST = "teacherTopList";
        /**
         * getHanDanHistoryList接口缓存文件
         */
        public static final String HANDANHISTORYLIST = "Handan_Success_Rate";
        /**
         * 获取财经要闻列表的接口缓存文件
         */
        public static final String COMMONNEWS_CJYW = "commonnews_cjyw";
        /**
         * 获取研究报告列表的接口缓存文件
         */
        public static final String COMMONNEWS_YJBG = "commonnews_yjbg";
        /**
         * 获取独家解读列表的接口缓存文件
         */
        public static final String COMMONNEWS_DJJD = "commonnews_djjd";
        /**
         * 获取市场动态列表的接口缓存文件
         */
        public static final String COMMONNEWS_SCDT = "commonnews_scdt";
        /**
         * 获取活动信息的接口缓存文件
         */
        public static final String ACTIVITYMANAGER = "activity_manager";
    }

    /**
     * 按照接口为key进行缓存
     */
    public static class Cache {
        /**
         * 用户信息key
         */
        public static final String USER_INFO = "user_info";
        /**
         * Token缓存Key
         */
        public static final String TOKEN_NAME = "app_token";
        /**
         * topindexData接口缓存key
         */
        public static final String TOP_INDEX_DATA = "topIndexData";
        /**
         * getImportMessage接口缓存key
         */
        public static final String IMPORT_MESSAGE = "importMessage";
        /**
         * getTeacherTopList接口缓存key
         */
        public static final String TEACHER_TOP_LIST = "teacherTopList";
        /**
         * getHanDanHistoryList接口缓存key
         */
        public static final String HANDAN_SUCCESS_RATE = "Handan_Success_Rate";
        /**
         * 获取财经要闻列表的接口缓存key
         */
        public static final String COMMONNEWS_CJYW = "commonnews_cjyw";
        /**
         * 获取研究报告列表的接口缓存key
         */
        public static final String COMMONNEWS_YJBG = "commonnews_yjbg";
        /**
         * 获取研究报告列表的接口缓存key
         */
        public static final String COMMONNEWS_DJJD = "commonnews_djjd";
        /**
         * 获取市场动态列表的接口缓存key
         */
        public static final String COMMONNEWS_SCDT = "commonnews_scdt";
        /**
         * 获取活动信息的接口缓存key
         */
        public static final String ACTIVITYMANAGER = "activity_manager";
    }
}
