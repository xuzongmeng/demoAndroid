package com.xzm.person.app;

/**
 * Created by Administrator on 2016/8/10.
 */
public class Constant {
    public static final int connTimeout = 6000;

    public  static String TEST_CACHE_KEY = "test_cache_key";

    public static String NOSUCHALGORITHM = "不支持此种加密方式";
    public static String UNSUPPENCODING = "不支持的编码格式";
    public static final long CACHE_MAXSIZE = 100 * 1024 * 1024;

}
