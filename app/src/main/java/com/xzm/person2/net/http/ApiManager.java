package com.xzm.person2.net.http;


import com.xzm.baselibrary.app.APPConfig;
import com.xzm.baselibrary.net.OkHttpInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**

 */
public class ApiManager {
    private static ApiManager sApiManager;
    //    private static OkHttpClient mClient;
    private Retrofit retrofit;
//    private AirFareApiService mGithubApi;

    private ApiManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(APPConfig.Network.WU_YU_BASE_URL)
                .client(buildOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiManager getInstence() {
        if (sApiManager == null) {
            synchronized (ApiManager.class) {
                if (sApiManager == null) {
                    sApiManager = new ApiManager();
                }
            }
        }
        return sApiManager;
    }

    private OkHttpClient buildOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new OkHttpInterceptor());
        if (APPConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }
        builder.connectTimeout(100, TimeUnit.SECONDS);
        builder.readTimeout(100, TimeUnit.SECONDS);
        builder.writeTimeout(100, TimeUnit.SECONDS);
        return builder.build();
    }

    /**
     *
     */
//    public AirFareApiService getWuYuService() {
//        if (mGithubApi == null) {
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl(APPConfig.Network.WU_YU_BASE_URL)
//                    .client(mClient)
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//            mGithubApi = retrofit.create(AirFareApiService.class);
//        }
//        return mGithubApi;
//    }


    public Retrofit getRetrofit() {
        return retrofit;
    }

}
