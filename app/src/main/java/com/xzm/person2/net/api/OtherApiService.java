package com.xzm.person2.net.api;

import com.xzm.person2.entity.Other;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   : 外包相关接口
 */
public interface OtherApiService {

     /* 获取机票地区列表*/
    @GET("tyb/live/selectImportantMessage")
    Observable<Other> selectImportantMessage();

//    /* 获取机票地区列表*/
//    @POST("airfare/area/list")
//    Observable<AirfareArea> airfareAreaList();
}
