package com.xzm.person2.net.api;

import com.xzm.person2.entity.AirfareArea;
import com.xzm.person2.entity.AirfareAreaVo;
import com.xzm.person2.net.http.HttpResult;

import io.reactivex.Observable;
import retrofit2.http.POST;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   : 机票相关接口
 */
public interface AirFareApiService {

//     /* 获取机票地区列表*/
    @POST("airfare/area/list")
    Observable<AirfareArea> airfareAreaListNoHttResult();

    //     /* 获取机票地区列表*/
    @POST("airfare/area/list")
    Observable<HttpResult<AirfareAreaVo>> airfareAreaList();
//    /* 获取机票地区列表*/
//    @POST("airfare/area/list")
//    Observable<AirfareArea> airfareAreaList();
}
