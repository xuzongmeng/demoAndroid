package com.xzm.person2.net.http;


import com.xzm.baselibrary.app.APPConfig;
import com.xzm.baselibrary.net.OkHttpInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**

 */
public class RetrofitManager {
    private static RetrofitManager sApiManager;

    //    private static OkHttpClient mClient;
//    private Retrofit retrofit;
//    private AirFareApiService mGithubApi;
//    public T data;
    private RetrofitManager() {
    }

    public static RetrofitManager getInstence() {
        if (sApiManager == null) {
            synchronized (RetrofitManager.class) {
                if (sApiManager == null) {
                    sApiManager = new RetrofitManager();
                }
            }
        }
        return sApiManager;
    }

    private OkHttpClient buildOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new OkHttpInterceptor());
        if (APPConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }
        builder.connectTimeout(100, TimeUnit.SECONDS);
        builder.readTimeout(100, TimeUnit.SECONDS);
        builder.writeTimeout(100, TimeUnit.SECONDS);
        return builder.build();
    }

    /**
     *
     */
    public <T> T createWuYuService(Class<T> serviceClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APPConfig.Network.WU_YU_BASE_URL)
                .client(buildOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(serviceClass);
    }

    public <T> T createWuYuOtherService(Class<T> serviceClass) {
//        Retrofit retrofit = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APPConfig.Network.XUZONGMENG_BASE_URL)
                .client(buildOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(serviceClass);

    }


//    public Retrofit getRetrofit() {
//        return retrofit;
//    }

}
