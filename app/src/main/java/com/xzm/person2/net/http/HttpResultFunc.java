package com.xzm.person2.net.http;


import com.orhanobut.logger.Logger;

import io.reactivex.functions.Function;

/**
 * 用来统一处理Http的resultCode,
 * 并将HttpResult的Data部分剥离出来返回给 observer
 * Created by xuzongmeng on 2016/6/20.
 */
public class HttpResultFunc<T> implements Function<HttpResult<T>, T> {
    @Override
    public T apply(HttpResult<T> httpResult)  {
        if (!"000000".equals(httpResult.getCode())){
            throw new ApiException(httpResult.getCode(), httpResult.getMsg());
        }
        Logger.json("HttpResultFunc" + httpResult);
        return httpResult.getData();//成功直接返回data数据
    }
}