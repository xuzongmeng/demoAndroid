package com.xzm.person2.net.http;

import android.text.TextUtils;
import android.util.Log;

import com.xzm.person2.entity.AirfareAreaVo;
import com.xzm.person2.entity.hotel.HotelListVo;
import com.xzm.person2.net.api.AirFareApiService;
import com.xzm.person2.net.api.HotelApiService;

import java.util.Map;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * author : xuzongmeng
 * date   : 2018/8/5
 * desc   :
 */
public class HttpMethods {
    private  String TAG="HttpMethods";
    //获取单例
    public static HttpMethods getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private <T> void toSubscribe(Observable<T> o, Observer<T> s) {
        o.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s);
    }



    //在访问HttpMethods时创建单例
    private static class SingletonHolder {
        private static final HttpMethods INSTANCE = new HttpMethods();
    }

    /**
     * 主要用于全局处理Token过期，当token过期时，最多重新获取3次。
     */
//    private Observable<?> refreshToken(Observable<HttpResult<?>> o) {
//        return o.flatMap(new Function<HttpResult<?>, Observable<?>>() {
//            @Override
//            public Observable<?> apply(HttpResult<?> httpResult) {
//                KLog.d("httpResult==" + httpResult.toString());
////                if (httpResult.getError() != null && "000900".equals(httpResult.getError()
//// .getCode())) {
////                    throw new ApiException("Token is old");
////                } else if ("000900".equals(httpResult.getStatus() + "")) {
////                    throw new ApiException("Token is old");
////                }
//                return Observable.just(httpResult);
//            }
//        }).retryWhen(new Function<Observable<? extends Throwable>, Observable<?>>() {
//            @Override
//            public Observable<?> apply(Observable<? extends Throwable> observable) {
//                return observable.zipWith(Observable.range(1, 3), new Function<Throwable, Integer,
//                        Throwable>() {
//                    @Override
//                    public Throwable call(Throwable throwable, Integer integer) {
//                        return throwable;
//                    }
//                })
//                        .flatMap(new Function<Throwable, Observable<?>>() {
//                            @Override
//                            public Observable<?> apply(Throwable throwable) {
//                                if (throwable instanceof ApiException) {
//                                    return xhbService.getAccessToken(
//                                            TokenUtil.getClientId(),
//                                            TokenUtil.getClient_secret(),
//                                            TokenUtil.GRANT_TYPE,
//                                            TokenUtil.getAccessTokenSign())
//                                            .map(new HttpResultFunc<TokenEntity>())
//                                            .doOnNext(new Action1<TokenEntity>() {
//                                                @Override
//                                                public void call(TokenEntity tokenEntity) {
//                                                    TokenUtil.TOKEN = tokenEntity.getAccess_token();
//                                                    token = tokenEntity.getAccess_token();
//                                                    KLog.i("Token重新获取成功");
//                                                }
//                                            });
//                                }
//                                return Observable.error(throwable);
//                            }
////
////                            @Override
////                            public Observable<?> call(Throwable throwable) {
////
////                            }
//                        });
//            }
//        });
//    }
    private Observable<String> getUserObservable (final int index, final String token) {
        return Observable.create(new ObservableOnSubscribe<String>() {

            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                Log.d(TAG, index + "使用token=" + token + "发起请求");
                //模拟根据Token去请求信息的过程。
                if (!TextUtils.isEmpty(token) && System.currentTimeMillis() - Long.valueOf(token) < 2000) {
                    e.onNext(index + ":" + token + "的用户信息");
                } else {
                    e.onError(new Throwable(ERROR_TOKEN));
                }
            }
        });
    }
    private   String ERROR_TOKEN;
    private   String ERROR_RETRY;
    private void startRequest(final int index) {
        Observable<String> observable = Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                String cacheToken = TokenLoader.getInstance().getCacheToken();
                Log.d(TAG, index + "获取到缓存Token=" + cacheToken);
                return Observable.just(cacheToken);
            }
        }).flatMap(new Function<String, ObservableSource<String>>() {
            @Override
            public ObservableSource<String> apply(String token) throws Exception {
                return getUserObservable(index, token);
            }
        }).retryWhen(new Function<Observable<Throwable>, ObservableSource<?>>() {

            private int mRetryCount = 0;

            @Override
            public ObservableSource<?> apply(Observable<Throwable> throwableObservable) throws Exception {
                return throwableObservable.flatMap(new Function<Throwable, ObservableSource<?>>() {

                    @Override
                    public ObservableSource<?> apply(Throwable throwable) throws Exception {
                        Log.d(TAG, index + ":" + "发生错误=" + throwable + ",重试次数=" + mRetryCount);
                        if (mRetryCount > 0) {
                            return Observable.error(new Throwable(ERROR_RETRY));
                        } else if (ERROR_TOKEN.equals(throwable.getMessage())) {
                            mRetryCount++;
                            return TokenLoader.getInstance().getNetTokenLocked();
                        } else {
                            return Observable.error(throwable);
                        }
                    }
                });
            }
        });
        DisposableObserver<String> observer = new DisposableObserver<String>() {

            @Override
            public void onNext(String value) {
                Log.d(TAG, index + ":" + "收到信息=" + value);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, index + ":" + "onError=" + e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, index + ":" + "onComplete");
            }
        };
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }



    public void getAirfareAreaData(Observer<AirfareAreaVo> observer, Map<String, Object> params) {
        Observable<HttpResult<AirfareAreaVo>> observable =
                RetrofitManager.getInstence().createWuYuService(AirFareApiService.class).airfareAreaList();
        toSubscribe(observable.map(new HttpResultFunc<>()), observer);
    }

    public void hotelList(Observer<HotelListVo> observer, Map<String, Object> params) {
        Observable<HttpResult<HotelListVo>> observable =
                RetrofitManager.getInstence().createWuYuService(HotelApiService.class).hotelList(params);
        toSubscribe(observable.map(new HttpResultFunc<>()), observer);
    }
}
