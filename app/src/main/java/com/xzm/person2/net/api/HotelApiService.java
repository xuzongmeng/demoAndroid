package com.xzm.person2.net.api;

import com.xzm.person2.entity.HotelList;
import com.xzm.person2.entity.hotel.HotelListVo;
import com.xzm.person2.net.http.HttpResult;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   : 酒店相关接口
 */
public interface HotelApiService {

//    /* 获取酒店列表列表*/
//    @POST("hotel/list")
//    Observable<AirfareArea> hotelList(@FieldMap Map<String, Object> params);

//    /* 获取酒店列表列表*/
//    @POST("hotel/list")
//    Observable<HotelList> hotelList(@QueryMap Map<String, Object> params);

//    /* 获取酒店列表列表*/
//    @POST("hotel/list")
//    Observable<HotelList> hotelListNoHttResult(@Body RequestBody requestBody);

    /* 获取酒店列表列表*/
    @POST("hotel/list")
    Observable<HotelList> hotelListNoHttResult(@Body Map<String, Object> params);

    /* 获取酒店列表列表*/
    @POST("hotel/list")
    Observable<HttpResult<HotelListVo>> hotelList(@Body Map<String, Object> params);

//    @POST("hotel/list")
//    Observable<HttpResult<ImportMessage>> hotelList(@Body Map<String, Object> params);
}
