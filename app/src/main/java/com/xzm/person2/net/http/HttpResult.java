package com.xzm.person2.net.http;
/**
 * 用来统一处理Http返回的结果类
 * Created by xuzongmeng on 2016/6/17.
 */

public class HttpResult<T> {


    private String msg;
    private String code;

    private T data;


    public String getMsg() {
        return msg;
    }

    public HttpResult<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getCode() {
        return code;
    }

    public HttpResult<T> setCode(String code) {
        this.code = code;
        return this;
    }

    public T getData() {
        return data;
    }

    public HttpResult<T> setData(T data) {
        this.data = data;
        return this;
    }
}
