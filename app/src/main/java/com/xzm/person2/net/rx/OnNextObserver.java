package com.xzm.person2.net.rx;

import com.orhanobut.logger.Logger;
import com.xzm.baselibrary.utils.NetworkUtils;
import com.xzm.person2.net.http.ApiException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

//import org.reactivestreams.Subscriber;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   :
 */

public  class OnNextObserver<T> implements Observer<T> {
    //    private SubscriberOnNextListener mSubscriberOnNextListener;
//
//    public OnNextSubscriber(SubscriberOnNextListener mSubscriberOnNextListener) {
//        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
//    }
    @Override
    public void onError(Throwable e) {
        Logger.d("Throwable=" + e);
    }

    @Override
    public void onComplete() {
        String msg = "订阅完成";
        Logger.d("====" + msg);
    }


    @Override
    public void onSubscribe(Disposable d) {
        if (!NetworkUtils.isConnected()) {
            onError(new ApiException("无网络连接", "0"));
        }
    }

    @Override
    public void onNext(T t) {
        Logger.d("订阅====" + t.toString());
//        if (mSubscriberOnNextListener != null) {
//            mSubscriberOnNextListener.onNext(t);
//        }
    }

//    public  abstract void onSuccess(T value);
}

