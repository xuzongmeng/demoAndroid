package com.xzm.person2.net.http;

/**
 * 接口异常处理类
 * Created by xuzongmeng on 2016/6/20.
 */
public class ApiException extends RuntimeException {

    private ApiException(String detailMessage) {
        super(detailMessage);
    }

    public ApiException(String resultCode, String detailMessage) {
        this(getApiExceptionMessage(resultCode));
    }


    /**
     * 对errorCode进行封装处理
     */
    private static String getApiExceptionMessage(String resultCode) {
        String message;
        switch (resultCode) {
            case "000001":
                message = "未知错误";
                break;
            default:
                message = "未知错误";

        }
        return message;
    }
}
