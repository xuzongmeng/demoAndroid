package com.xzm.person2.entity;

import java.util.List;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   :
 */
public class HotelList {


    @Override
    public String toString() {
        return "HotelList{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }

    /**
     * code : 000000
     * data : {"data":[{"uid":9223372036854775807,"image":"[{\"height\":180,\"path\":\"FmhfuHizlu2iT0Dzy8WxSUQT4Q99\",\"pictureType\":\"image/jpeg\",\"width\":180},{\"height\":180,\"path\":\"FmhfuHizlu2iT0Dzy8WxSUQT4Q99\",\"pictureType\":\"image/jpeg\",\"width\":180}]","marketPrice":500,"lng":116.407431,"establishTime":1529562319000,"level":3,"refreshTime":1529562266000,"name":"如家酒店","describe":"如家似家","lat":39.915156}],"end":10,"index":1,"size":10,"start":10,"sumIndex":0,"sumSize":0}
     * msg : 成功
     */

    private String code;
    private DataBeanX data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBeanX {
        @Override
        public String toString() {
            return "DataBeanX{" +
                    "end=" + end +
                    ", index=" + index +
                    ", size=" + size +
                    ", start=" + start +
                    ", sumIndex=" + sumIndex +
                    ", sumSize=" + sumSize +
                    ", data=" + data +
                    '}';
        }

        /**
         * data : [{"uid":9223372036854775807,"image":"[{\"height\":180,\"path\":\"FmhfuHizlu2iT0Dzy8WxSUQT4Q99\",\"pictureType\":\"image/jpeg\",\"width\":180},{\"height\":180,\"path\":\"FmhfuHizlu2iT0Dzy8WxSUQT4Q99\",\"pictureType\":\"image/jpeg\",\"width\":180}]","marketPrice":500,"lng":116.407431,"establishTime":1529562319000,"level":3,"refreshTime":1529562266000,"name":"如家酒店","describe":"如家似家","lat":39.915156}]
         * end : 10
         * index : 1
         * size : 10
         * start : 10
         * sumIndex : 0
         * sumSize : 0
         */



        private int end;
        private int index;
        private int size;
        private int start;
        private int sumIndex;
        private int sumSize;
        private List<DataBean> data;

        public int getEnd() {
            return end;
        }

        public void setEnd(int end) {
            this.end = end;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getSumIndex() {
            return sumIndex;
        }

        public void setSumIndex(int sumIndex) {
            this.sumIndex = sumIndex;
        }

        public int getSumSize() {
            return sumSize;
        }

        public void setSumSize(int sumSize) {
            this.sumSize = sumSize;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * uid : 9223372036854775807
             * image : [{"height":180,"path":"FmhfuHizlu2iT0Dzy8WxSUQT4Q99","pictureType":"image/jpeg","width":180},{"height":180,"path":"FmhfuHizlu2iT0Dzy8WxSUQT4Q99","pictureType":"image/jpeg","width":180}]
             * marketPrice : 500
             * lng : 116.407431
             * establishTime : 1529562319000
             * level : 3
             * refreshTime : 1529562266000
             * name : 如家酒店
             * describe : 如家似家
             * lat : 39.915156
             */

            private long uid;
            private String image;
            private int marketPrice;
            private double lng;
            private long establishTime;
            private int level;
            private long refreshTime;
            private String name;
            private String describe;
            private double lat;

            public long getUid() {
                return uid;
            }

            public void setUid(long uid) {
                this.uid = uid;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getMarketPrice() {
                return marketPrice;
            }

            public void setMarketPrice(int marketPrice) {
                this.marketPrice = marketPrice;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public long getEstablishTime() {
                return establishTime;
            }

            public void setEstablishTime(long establishTime) {
                this.establishTime = establishTime;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public long getRefreshTime() {
                return refreshTime;
            }

            public void setRefreshTime(long refreshTime) {
                this.refreshTime = refreshTime;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescribe() {
                return describe;
            }

            public void setDescribe(String describe) {
                this.describe = describe;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }
        }
    }
}
