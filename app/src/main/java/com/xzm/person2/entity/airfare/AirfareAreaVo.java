package com.xzm.person2.entity;

import java.util.List;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   :
 */
public class AirfareAreaVo {
    /**
     * code : 000000
     * data : [{"terminalName":"北京T1海航","address_id":5249973297872896,"name":"北京市"},{"terminalName":"北京T2上航","address_id":5249973297872896,"name":"北京市"},{"terminalName":"北京T3国航","address_id":5249973297872896,"name":"北京市"},{"terminalName":"上海浦东机场T1","address_id":5249941213544448,"name":"上海市"},{"terminalName":"上海浦东机场T2","address_id":5249941213544448,"name":"上海市"}]
     * msg : 成功
     */

    private String code;
    private String msg;
    private List<DataBean> data;


    @Override
    public String toString() {
        return "AirfareArea{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        @Override
        public String toString() {
            return "DataBean{" +
                    "terminalName='" + terminalName + '\'' +
                    ", address_id=" + address_id +
                    ", name='" + name + '\'' +
                    '}';
        }

        /**
         * terminalName : 北京T1海航
         * address_id : 5249973297872896
         * name : 北京市
         */

        private String terminalName;
        private long address_id;
        private String name;

        public String getTerminalName() {
            return terminalName;
        }

        public void setTerminalName(String terminalName) {
            this.terminalName = terminalName;
        }

        public long getAddress_id() {
            return address_id;
        }

        public void setAddress_id(long address_id) {
            this.address_id = address_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


//    private List<DataBean> data;
//
//    public List<DataBean> getData() {
//        return data;
//    }
//
//    public void setData(List<DataBean> data) {
//        this.data = data;
//    }
//
//    public static class DataBean {
//        /**
//         * address_id : 5249973297872896
//         * name : 北京市
//         * terminalName : 北京T1海航
//         */
//
//        private long address_id;
//        private String name;
//        private String terminalName;
//
//        public long getAddress_id() {
//            return address_id;
//        }
//
//        public void setAddress_id(long address_id) {
//            this.address_id = address_id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getTerminalName() {
//            return terminalName;
//        }
//
//        public void setTerminalName(String terminalName) {
//            this.terminalName = terminalName;
//        }
//    }
}
