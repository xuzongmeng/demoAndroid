package com.xzm.person2.entity;

/**
 * author : xuzongmeng
 * date   : 2018/8/3
 * desc   :
 */
public class Other {


    /**
     * data : {"object":{"annvalue":"43","autoid":"12","badpers":67,"createTime":1526619446000,"day":"34","goodpers":56,"id":1,"imageurl":"http://xzm1102207843.oss-cn-beijing.aliyuncs.com/tyb/home/live_sub_img2.png","importence":"2","name":"哈哈","prevalue":"31","question":"黄金正处\u201d卧薪尝胆\u201c时期 有望触及五年高点","time":1525576364000,"updataTime":1526619446000}}
     * msg :
     * status : 0
     * success : true
     */

    private DataBean data;
    private String msg;
    private int status;
    private boolean success;


    @Override
    public String toString() {
        return "Other{" +
                "data=" + data +
                ", msg='" + msg + '\'' +
                ", status=" + status +
                ", success=" + success +
                '}';
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class DataBean {

        @Override
        public String toString() {
            return "DataBean{" +
                    "object=" + object +
                    '}';
        }

        /**
         * object : {"annvalue":"43","autoid":"12","badpers":67,"createTime":1526619446000,"day":"34","goodpers":56,"id":1,"imageurl":"http://xzm1102207843.oss-cn-beijing.aliyuncs.com/tyb/home/live_sub_img2.png","importence":"2","name":"哈哈","prevalue":"31","question":"黄金正处\u201d卧薪尝胆\u201c时期 有望触及五年高点","time":1525576364000,"updataTime":1526619446000}
         */

        private ObjectBean object;

        public ObjectBean getObject() {
            return object;
        }

        public void setObject(ObjectBean object) {
            this.object = object;
        }

        public static class ObjectBean {

            @Override
            public String toString() {
                return "ObjectBean{" +
                        "annvalue='" + annvalue + '\'' +
                        ", autoid='" + autoid + '\'' +
                        ", badpers=" + badpers +
                        ", createTime=" + createTime +
                        ", day='" + day + '\'' +
                        ", goodpers=" + goodpers +
                        ", id=" + id +
                        ", imageurl='" + imageurl + '\'' +
                        ", importence='" + importence + '\'' +
                        ", name='" + name + '\'' +
                        ", prevalue='" + prevalue + '\'' +
                        ", question='" + question + '\'' +
                        ", time=" + time +
                        ", updataTime=" + updataTime +
                        '}';
            }

            /**
             * annvalue : 43
             * autoid : 12
             * badpers : 67
             * createTime : 1526619446000
             * day : 34
             * goodpers : 56
             * id : 1
             * imageurl : http://xzm1102207843.oss-cn-beijing.aliyuncs.com/tyb/home/live_sub_img2.png
             * importence : 2
             * name : 哈哈
             * prevalue : 31
             * question : 黄金正处”卧薪尝胆“时期 有望触及五年高点
             * time : 1525576364000
             * updataTime : 1526619446000
             */

            private String annvalue;
            private String autoid;
            private int badpers;
            private long createTime;
            private String day;
            private int goodpers;
            private int id;
            private String imageurl;
            private String importence;
            private String name;
            private String prevalue;
            private String question;
            private long time;
            private long updataTime;

            public String getAnnvalue() {
                return annvalue;
            }

            public void setAnnvalue(String annvalue) {
                this.annvalue = annvalue;
            }

            public String getAutoid() {
                return autoid;
            }

            public void setAutoid(String autoid) {
                this.autoid = autoid;
            }

            public int getBadpers() {
                return badpers;
            }

            public void setBadpers(int badpers) {
                this.badpers = badpers;
            }

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public int getGoodpers() {
                return goodpers;
            }

            public void setGoodpers(int goodpers) {
                this.goodpers = goodpers;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImageurl() {
                return imageurl;
            }

            public void setImageurl(String imageurl) {
                this.imageurl = imageurl;
            }

            public String getImportence() {
                return importence;
            }

            public void setImportence(String importence) {
                this.importence = importence;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrevalue() {
                return prevalue;
            }

            public void setPrevalue(String prevalue) {
                this.prevalue = prevalue;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public long getTime() {
                return time;
            }

            public void setTime(long time) {
                this.time = time;
            }

            public long getUpdataTime() {
                return updataTime;
            }

            public void setUpdataTime(long updataTime) {
                this.updataTime = updataTime;
            }
        }
    }
}
