package com.xzm.person2.ui.activity1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.xzm.person2.R;

/**
 * author : xuzongmeng
 * date   : 2018/8/25
 * desc   :
 */
public class DatePickerViewActivity  extends AppCompatActivity {

    private CalendarView calendarView;
    private TimePicker timePicker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker_view);

//        context = this;
//        calendarView = findViewById(R.id.calendarViewId);
//
//        calendarView.setOnDateChangeListener((view, year, monthOfYear, dayOfMonth) -> {
//            String content = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
//            Toast.makeText(DatePickerViewActivity.this, "你选择了:\n"+content, Toast.LENGTH_SHORT).show();
//        });

        timePicker = findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        timePicker.setMinute(0);
        timePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
                String content = hourOfDay+"-"+(minute+1)+"-";
                Toast.makeText(DatePickerViewActivity.this, "你选择了:\n"+content, Toast.LENGTH_SHORT).show();

        });

    }
}
