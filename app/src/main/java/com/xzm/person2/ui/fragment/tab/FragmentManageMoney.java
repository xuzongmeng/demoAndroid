package com.xzm.person2.ui.fragment.tab;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import com.xzm.baselibrary.base.Base2Fragment;
import com.xzm.person2.R;

/**
 * Created by xuzongmeng on 2017/1/16.
 */

public class FragmentManageMoney extends Base2Fragment {

    private TextView tv_fragemnt_text;
    private TextView tv_fragemnt_text_hit;
    @Override
    public int getLayoutID() {
        return R.layout.fragment_manage_money;
    }

    @Override
    protected void lazyLoad() {

        if (!isLoad){
            initHttp();
//            tv_fragemnt_text_hit.setText("FragmentManageMoney没有初始化数据");
        }else {
//            tv_fragemnt_text_hit.setText("FragmentManageMoney已经初始化过数据");

        }

//        if (!isLoad){
//            initHttp();
//            binding.tvFragemntTextHit.setText("FragmentManageMoney没有初始化数据");
//        }else {
//            binding.tvFragemntTextHit.setText("FragmentManageMoney已经初始化过数据");
//
//        }

    }

    @Override
    public void initViews(View contentView) {
        tv_fragemnt_text = (TextView) contentView.findViewById(R.id.tv_fragemnt_text);
        tv_fragemnt_text_hit = (TextView) contentView.findViewById(R.id.tv_fragemnt_text_hit);
//        if (!isLoadDate){
//            showLoading();
//        }
        if (!isLoad){
            showLoading();
        }



        tv_fragemnt_text.setText("这是理财界面");
    }

    private void showContentView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showContent();
//                isLoadDate=true;
            }
        }, 2000);


    }


    private void initHttp() {
        showLoading();

        showContentView();
    }
}