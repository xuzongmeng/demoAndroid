package com.xzm.person2.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<String> mTitleList;
    private List<Fragment> mFragmentList ;
    public ViewPagerAdapter(FragmentManager manager, List<Fragment> fragmentList, List<String>
            titleList) {

        super(manager);
        this.mFragmentList=fragmentList;
        this.mTitleList=titleList;
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        if (mTitleList == null) return null;
        return mTitleList.get(position);//页卡标题
    }

//
//    public void addFragment(Fragment fragment) {
//        mFragmentList.add(fragment);
//    }



}
