package com.xzm.person2.ui.activity1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.xzm.baselibrary.utils.ToastUtil;
import com.xzm.person2.R;
import com.xzm.person2.ui.widget.ExistLoginDialog;


/**
 * 自定义控件
 */
public class MyViewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_view);
    }

}
