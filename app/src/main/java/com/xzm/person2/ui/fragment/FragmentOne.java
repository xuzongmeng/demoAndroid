package com.xzm.person2.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.xzm.person2.R;
import com.xzm.person2.ui.activity1.DatePickerViewActivity;
import com.xzm.person2.ui.activity1.DialogActivity;
import com.xzm.person2.ui.activity1.LazyActivity;
import com.xzm.person2.ui.activity1.MeiTuanActivity;
import com.xzm.person2.ui.activity1.MyViewActivity;
import com.xzm.person2.ui.activity1.RxJavaActivity;
import com.xzm.person2.ui.adapter.DemoItemAdapter;
import com.xzm.person2.ui.component.sortedcontact.MainActivity;
import com.xzm.person2.ui.entity.DemoItem;

import java.util.ArrayList;
import java.util.List;


//import com.xzm.person.ui.activity1.OkHttpUtilsActivity;


public class FragmentOne extends Fragment {
    TextView textView;
    private DemoItemAdapter mAdapter;
    private List<DemoItem> mlist;
    private RecyclerView recyclerViewDemoItem;

    public static FragmentOne newInstance(String text) {
        FragmentOne fragmentCommon = new FragmentOne();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);
//        textView= (TextView) view.findViewById(R.id.textView);
//        textView.setText(getArguments().getString("text"));
        initView(view);
        initDates();
        return view;
    }

    private void initView(View view) {
        recyclerViewDemoItem = view.findViewById(R.id.recyclerView_demo_item);
        recyclerViewDemoItem.setLayoutManager(new GridLayoutManager(getContext(), 2));
         textView = view.findViewById(R.id.tv_toolbaer_title);
    }

    public void initDates() {
        mlist = new ArrayList<>();
        mAdapter = new DemoItemAdapter(R.layout.item_demo, mlist);
        mAdapter.openLoadAnimation();
        recyclerViewDemoItem.setHasFixedSize(true);
        recyclerViewDemoItem.setAdapter(mAdapter);
        addBaseDate();
    }

    @SuppressLint("SetTextI18n")
    private void addBaseDate() {
//        mlist.add(new DemoItem(WebViewActivity.class, "WebView加载网页1"));
//        mlist.add(new DemoItem(WebViewActivity2.class, "WebViewA2加载网页2"));
        mlist.add(new DemoItem(RxJavaActivity.class, "RxJava操作符"));
        mlist.add(new DemoItem(LazyActivity.class, "懒加载"));
        mlist.add(new DemoItem(MeiTuanActivity.class, "美团下拉筛选"));
        mlist.add(new DemoItem(MainActivity.class, "城市字母索引"));
        mlist.add(new DemoItem(DatePickerViewActivity.class, "时间选择器"));
        mlist.add(new DemoItem(DialogActivity.class, "弹框"));
        mlist.add(new DemoItem(MyViewActivity.class, "自定义View"));

//        LogUtils.d("====fragmentOne=大小===" + mlist.size());
//        Logger.d("hello");

        textView.setText(getArguments().getString("text") + String.valueOf(mlist.size()));
        mAdapter.notifyDataSetChanged();
    }
}
