package com.xzm.person2.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wang.avi.AVLoadingIndicatorView;
import com.xzm.person2.R;

/**
 * author : xuzongmeng
 * date   : 2018/10/25
 * desc   :
 */
public class ExistLoginDialog implements View.OnClickListener {

    private LinearLayout ll_exist_login_dialog;
    private TextView tv_exist_login_confirm;
    private TextView tv_exsit_login_cancel;
    private Context context;
    private Dialog dialog;
    private Display display;
    private AVLoadingIndicatorView loading;
    private TextView tv_hintMsg;
    private View view;

    public ExistLoginDialog(Context context) {
        this.context = context;
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ExistLoginDialog builder() {
        // 获取Dialog布局
        view = LayoutInflater.from(context).inflate(R.layout.dialog_exist, null);

        ll_exist_login_dialog = (LinearLayout) view.findViewById(R.id.ll_exist_login_dialog);
        tv_exist_login_confirm = (TextView) view.findViewById(R.id.tv_exist_login_confirm);
        tv_exsit_login_cancel = (TextView) view.findViewById(R.id.tv_exsit_login_cancel);
//        tv_hintMsg = (TextView) view.findViewById(R.id.tv_super_dialog_hintMsg);
        tv_exist_login_confirm.setOnClickListener(this);
        tv_exsit_login_cancel.setOnClickListener(this);
        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.AlertDialogStyle);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        // 调整dialog背景大小
        ll_exist_login_dialog.setLayoutParams(new FrameLayout.LayoutParams((int) (display
                .getWidth() * 0.7), LinearLayout.LayoutParams.MATCH_PARENT));

        return this;
    }

    public ExistLoginDialog setHintMsg(String msg) {

        if (!TextUtils.isEmpty(msg)) {
            tv_hintMsg.setText(msg);
        } else {
            tv_hintMsg.setText("努力为您加载中...");
        }
        return this;
    }

    public ExistLoginDialog setCancelable(boolean cancel) {
        dialog.setCancelable(cancel);
        return this;
    }

    public void show() {
        dialog.show();
        setShowAnima(view);
    }

    public  void  disMiss(){

        setDisMissAnima(view);
    }

    /**
     * 设置弹出时动画
     */
    private  void  setShowAnima(View view){
//        ObjectAnimator animator1 = ObjectAnimator.ofFloat(view, "scaleY", 0.5f, 1f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(view, "scaleX", 0.5f, 1f);
//        ObjectAnimator animator3 = ObjectAnimator.ofFloat(view, "alpha", 0f, 0.5f, 1f);

//        int width = display.getWidth();
       ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", 0, 200, 0,200,0);
//        animator.setDuration(2000);
//        ObjectAnimator animator4 = ObjectAnimator.ofFloat(view, "translationX", -width,0);
//        animator.setDuration(2000);
//        animator.start();
//        ObjectAnimator objectAnimatorScale = ObjectAnimator.ofFloat(view, "rotation", 0f, 360f);

        ObjectAnimator objectAnimator4 = ObjectAnimator.ofFloat(view, "rotationY", 0.0f, 360f);
//        objectAnimator4.setInterpolator(new PathInterpolator(0.3f, 0.0f, 0.8f, 1.0f));
//        objectAnimator4.setDuration(1000);
        AnimatorSet set = new AnimatorSet();
//        set.play(animator3).with(animator1).with(animator2).with(animator4);
        set.play(objectAnimator4).with(animator2).with(animator);
        set.setDuration(2000);
        set.start();
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 设置关闭时动画
     */
    private  void  setDisMissAnima(View view){
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.5f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.5f);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(view, "alpha", 1f, 0.5f, 0f);
        int width = display.getWidth();
//        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationX", 0, 200, -200,0);
        ObjectAnimator animator4 = ObjectAnimator.ofFloat(view, "translationX", 0,width);

        AnimatorSet set = new AnimatorSet();
        set.play(animator3).with(animator1).with(animator2).with(animator4);
        set.setDuration(800);
        set.start();
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                dialog.dismiss();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_exist_login_confirm:
//              ToastUtil.showToast("点击确认");
                if (mOnExistLoignListener!=null){
                    mOnExistLoignListener.onExist();
                }
                break;

            case R.id.tv_exsit_login_cancel:
                disMiss();
                break;
        }
    }

    private     OnExistLoignListener   mOnExistLoignListener;
    public  void  setOnExistLoignListener(OnExistLoignListener  onExistLoignListener){
        this.mOnExistLoignListener=onExistLoignListener;
    }
    public   interface   OnExistLoignListener{
        void  onExist();
    }
}
