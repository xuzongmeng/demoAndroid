package com.xzm.person2.ui.activity1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import com.xzm.person2.R;
import com.xzm.person2.ui.adapter.ViewPagerAdapter;
import com.xzm.person2.ui.fragment.tab.FragmentFind;
import com.xzm.person2.ui.fragment.tab.FragmentHome;
import com.xzm.person2.ui.fragment.tab.FragmentManageMoney;
import com.xzm.person2.ui.fragment.tab.FragmentPerson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuzongmeng on 2017/1/16.
 * 测试懒加载
 */

public class LazyActivity extends AppCompatActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private LayoutInflater mInflater;
    private List<String> mTitleList = new ArrayList<>();//页卡标题集合
//    private Fragment view1, view2, view3, view4, view5;//页卡视图
    private List<Fragment> mList = new ArrayList<>();//页卡视图集合
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lazy);

        initViews();
        initDates();
    }
    private void initViews() {
        mViewPager = findViewById(R.id.vp_view);
        mTabLayout =  findViewById(R.id.tabs);
//        mViewPager.setOffscreenPageLimit(0);
//        mInflater = LayoutInflater.from(this);
//        view1 = mInflater.inflate(R.layout.activity_main, null);
//        view2 = mInflater.inflate(R.layout.activity_main, null);
//        view3 = mInflater.inflate(R.layout.activity_main, null);
//        view4 = mInflater.inflate(R.layout.activity_main, null);
//        view5 = mInflater.inflate(R.layout.activity_main, null);

        //添加页卡视图
        mList.add(new FragmentHome());
        mList.add(new FragmentManageMoney());
        mList.add(new FragmentPerson());
        mList.add(new FragmentFind());

        //添加页卡标题
        mTitleList.add("首页");
        mTitleList.add("理财");
        mTitleList.add("个人中心");
        mTitleList.add("发现");

        mTabLayout.setTabMode(TabLayout.MODE_FIXED);//设置tab模式，当前为系统默认模式
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(0)));//添加tab选项卡
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(1)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(2)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(3)));


        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getSupportFragmentManager(),mList,mTitleList);
        mViewPager.setAdapter(mAdapter);//给ViewPager设置适配器
        mTabLayout.setupWithViewPager(mViewPager);//将TabLayout和ViewPager关联起来。
//        mTabLayout.setTabsFromPagerAdapter(mAdapter);//给Tabs设置适配器

    }

    private void initDates() {

    }
}
