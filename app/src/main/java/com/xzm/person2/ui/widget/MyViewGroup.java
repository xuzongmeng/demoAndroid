package com.xzm.person2.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.logger.Logger;
import com.xzm.baselibrary.utils.DeviceUtil;
import com.xzm.baselibrary.utils.LogUtils;
import com.xzm.baselibrary.utils.ResUtils;
import com.xzm.person2.R;

public class MyViewGroup extends ViewGroup {
    private int defaultSize;

    public MyViewGroup(Context context) {
        super(context);
    }

    public MyViewGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyViewGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyView);
        defaultSize = typedArray.getDimensionPixelSize(R.styleable.MyView_default_size, 100);
        typedArray.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //将所有的子View进行测量，这会触发每个子View的onMeasure函数
        //注意要与measureChild区分，measureChild是对单个view进行测量
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int childCount = getChildCount();
        if (childCount == 0) {
            Logger.w("=====childCount=");
            setMeasuredDimension(0, 0);
        } else {
            //如果宽高都是包裹内容
            if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST) {
                //我们将高度设置为所有子View的高度相加，宽度设为子View中最大的宽度
                Logger.w("=====111111111=AT_MOST=");
                int width = getMaxChildWidth();
                int height = getTotalHeight();
                setMeasuredDimension(width, height);
            } else if (heightMode == MeasureSpec.AT_MOST) {//如果只有高度是包裹内容
                //宽度设置为ViewGroup自己的测量宽度，高度设置为所有子View的高度总和
                Logger.w("=====heightMode=AT_MOST=");
                setMeasuredDimension(widthSize, getTotalHeight());
            } else if (widthMode == MeasureSpec.AT_MOST) {//如果只有高度是包裹内容
                //宽度设置为子View中宽度最大的值，高度设置为ViewGroup自己的测量值
                Logger.i("=====widthMode=AT_MOST=");
                setMeasuredDimension(getMaxChildWidth(), heightSize);
            }
            //else if (heightMode==MeasureSpec.EXACTLY){
            //                Logger.i("=====heightMode=EXACTLY=");
            //                setMeasuredDimension(getMaxChildWidth(),getTotalHeight());
            //            }
        }
    }

    /***
     * 获取子View中宽度最大的值
     */
    private int getMaxChildWidth() {
        int childCount = getChildCount();
        int maxWidth = 0;
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            if (childView.getMeasuredWidth() > maxWidth) {
                maxWidth = childView.getMeasuredWidth();
            }

        }
        return maxWidth;
    }

    /***
     * 将所有子View的高度相加
     **/
    private int getTotalHeight() {
        int childCount = getChildCount();
        int height = 0;
        for (int i = 0; i < childCount; i++) {
            View chileView = getChildAt(i);
            height += chileView.getMeasuredHeight();
        }
        return height;
    }

    /**
     * 代码中的注释我已经写得很详细，不再对每一行代码进行讲解。上面的onMeasure将子View测量好了，
     * 以及把自己的尺寸也设置好了，接下来我们去摆放子View吧~
     */

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int childCount = getChildCount();
        int curHeight = top;
        int dp = DeviceUtil.px2dp(getContext(), top);
        int px = DeviceUtil.dp2px(getContext(), top);
        Logger.w("=====dp=" + dp);
        Logger.w("=====px=" + px);
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            int width = childView.getMeasuredWidth();
            int height = childView.getMeasuredHeight();
            childView.layout(left, curHeight, left + width, curHeight + height);
            curHeight += height;

        }
    }

}
