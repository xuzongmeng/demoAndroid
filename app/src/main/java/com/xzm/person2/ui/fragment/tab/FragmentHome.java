package com.xzm.person2.ui.fragment.tab;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.xzm.baselibrary.base.Base2Fragment;
import com.xzm.person2.R;

/**
 * Created by xuzongmeng on 2016/9/8.
 */
public class FragmentHome extends Base2Fragment {

    private TextView tv_fragemnt_text;
    private TextView tv_fragemnt_text_hit;

    @Override
    public int getLayoutID() {
        return R.layout.fragment_home;
    }

    @Override
    protected void lazyLoad() {

//        if (!isLoadDate){
//              initHttp();
//            tv_fragemnt_text_hit.setText("FragmentHome=没有初始化数据");
//        }else {
//            tv_fragemnt_text_hit.setText("FragmentHome=已经初始化过数据");
//        }
        if (!isLoad){
            initHttp();
//            binding.tvFragemntTextHit.setText("FragmentHome=没有初始化数据");
           tv_fragemnt_text_hit.setText("FragmentHome=没有初始化数据");
        }else {
//            binding.tvFragemntTextHit.setText("FragmentHome=已经初始化过数据");
            tv_fragemnt_text_hit.setText("FragmentHome=已经初始化过数据");
        }


    }

    @Override
    public void initViews(View view) {
//        if (!isLoadDate){
//            showLoading();
//        }

        if (!isLoad){
            showLoading();
        }
        tv_fragemnt_text = (TextView) view.findViewById(R.id.tv_fragemnt_text);
        tv_fragemnt_text_hit = (TextView) view.findViewById(R.id.tv_fragemnt_text_hit);
        tv_fragemnt_text.setText("这是首页界面");

    }



    private void showContentView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showContent();
//                isLoadDate=true;
            }
        }, 2000);
    }



    private void initHttp() {


        showLoading();

        showContentView();
    }
}
