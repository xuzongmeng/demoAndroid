package com.xzm.person2.ui.fragment.tab;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.xzm.baselibrary.base.Base2Fragment;
import com.xzm.person2.R;

/**
 * Created by xuzongmeng on 2017/1/16.
 * 个人中心
 */

public class FragmentPerson extends Base2Fragment {

    private TextView tv_fragemnt_text;
    private TextView tv_fragemnt_text_hit;

    @Override
    public int getLayoutID() {
        return R.layout.fragment_person;
    }

    @Override
    protected void lazyLoad() {
//        if (!isLoadDate){
//            initHttp();
//            tv_fragemnt_text_hit.setText("FindFragemnt=没有初始化数据");
//        }else {
//            tv_fragemnt_text_hit.setText("FindFragemnt=已经初始化过数据");
//        }

        if (!isLoad){
            initHttp();
            tv_fragemnt_text_hit.setText("FindFragemnt=没有初始化数据");
        }else {
            tv_fragemnt_text_hit.setText("FindFragemnt=已经初始化过数据");
        }
    }

    @Override
    public void initViews(View contentView) {
//        if (!isLoadDate){
//            showLoading();
//        }
        if (!isLoad){
            showLoading();
        }

        tv_fragemnt_text = (TextView) contentView.findViewById(R.id.tv_fragemnt_text);
        tv_fragemnt_text_hit = (TextView) contentView.findViewById(R.id.tv_fragemnt_text_hit);

        tv_fragemnt_text.setText("这是个人中心界面");
    }

    private void showContentView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showContent();
//                isLoadDate=true;
            }
        }, 2000);
    }

    private void initHttp() {
        showLoading();

        showContentView();
    }

}
