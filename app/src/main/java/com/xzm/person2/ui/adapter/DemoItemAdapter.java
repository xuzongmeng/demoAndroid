package com.xzm.person2.ui.adapter;

import android.content.Intent;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xzm.person2.R;
import com.xzm.person2.ui.entity.DemoItem;

import java.util.List;


/**
 * Created by xuzongmeng on 2017/1/13.
 */

public class DemoItemAdapter extends BaseQuickAdapter<DemoItem,BaseViewHolder> {
    public DemoItemAdapter(int layoutResId, List<DemoItem> data) {
        super(layoutResId, data);
    }
    @Override
    protected void convert(final BaseViewHolder holder, final DemoItem item) {
        holder.setText(R.id.tv_class_name,item.getClassName().getSimpleName())
        .setText(R.id.tv_description,item.getDescription());
        holder.itemView.setOnClickListener(v -> {
            mContext.startActivity(new Intent(mContext, item.getClassName()));
//            ToastUtil.showToast("点击="+item.getClassName().getSimpleName());
        });

    }
}
