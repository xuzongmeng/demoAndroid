package com.xzm.person2.ui.activity1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.orhanobut.logger.Logger;
import com.xzm.person2.R;
import com.xzm.person2.entity.AirfareArea;
import com.xzm.person2.entity.AirfareAreaVo;
import com.xzm.person2.entity.HotelList;
import com.xzm.person2.entity.Other;
import com.xzm.person2.entity.hotel.HotelListVo;
import com.xzm.person2.net.api.AirFareApiService;
import com.xzm.person2.net.api.HotelApiService;
import com.xzm.person2.net.api.OtherApiService;
import com.xzm.person2.net.http.ApiManager;
import com.xzm.person2.net.http.HttpMethods;
import com.xzm.person2.net.http.RetrofitManager;
import com.xzm.person2.net.rx.OnNextObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 被观察者（Observable）
 * 观察者（Observer）
 * 订阅（subscribe）
 * 原文地址 :https://juejin.im/post/5b17560e6fb9a01e2862246f
 */
public class RxJavaActivity extends AppCompatActivity {
    private static String TAG = "RxJavaActivity";

    /**
     * 创建观察者：
     */
    private void xxxx() {
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_java);
        //        Log.d(TAG, "=========================currentThread 哈哈哈: " + Thread.currentThread().getName());
//        Logger.d("====XXXXXXXXXXXXXXXXXXXXXXXX=" + TAG);
//        Log.d(TAG, "=========================currentThread 哈哈哈: ");
//       test1();
//        testCreate();
//        testJust();
//        testfromArray();
//        testfromCallable();
//        testFutureTask();
//        testfromIterable();
//        testdefer();
//        getData();
//        getData3();
//        getData4();
//        getDataOther();
        getHotelList();
        getAirfareAreaList();
    }

    private void test1() {
        /**
         * 创建被观察者：
         * emitter 发射器
         */
        Observable<Integer> observable = Observable.create(emitter -> {
            Logger.d(TAG, "=========================currentThread name: ");
            emitter.onNext(1);
            emitter.onNext(2);
            emitter.onNext(3);
            emitter.onComplete();
        });
        /**
         * 创建观察者：：
         */
        Observer<Integer> observer = new Observer<Integer>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "======================onSubscribe");
            }

            @Override
            public void onNext(Integer integer) {
                Log.d(TAG, "======================onNext " + integer);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "======================onError");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "======================onComplete");
            }
        };
        /**
         * 订阅
         */
        observable.subscribe(observer);
    }

    /**
     * 1. create 操作符
     */
    private void testCreate() {
        Observable<String> observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                e.onNext("Hello Observer");
                e.onComplete();
            }
        });

    }

    /**
     * 2. just 操作符
     * 创建一个被观察者，并发送事件，发送的事件不可以超过10个以上。
     */
    private void testJust() {
        Observable.just(1, 2, 3)
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "=================onSubscribe");
                    }

                    @Override
                    public void onNext(Integer integer) {
                        Log.d(TAG, "=================onNext " + integer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "=================onError ");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "=================onComplete ");
                    }
                });


    }

    /**
     * 3. testfromArray 操作符
     * 这个方法和 just() 类似，只不过 fromArray 可以传入多于10个的变量，并且可以传入一个数组。
     */
    private void testfromArray() {
        Integer array[] = {1, 2, 3, 4};
        Observable.fromArray(array)
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "=================onSubscribe");
                    }

                    @Override
                    public void onNext(Integer integer) {
                        Log.d(TAG, "=================onNext " + integer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "=================onError ");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "=================onComplete ");
                    }
                });

    }

    /**
     * 4. fromCallable 操作符
     * 这里的 Callable 是 java.util.concurrent 中的 Callable，Callable 和 Runnable 的用法基本一致，
     * 只是它会返回一个结果值，这个结果值就是发给观察者的。
     */
    @SuppressLint("CheckResult")
    private void testfromCallable() {
        Integer array[] = {1, 2, 3, 4};
        Observable.fromCallable(new Callable<Integer>() {

            @Override
            public Integer call() throws Exception {
                return 2;
            }
        }).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                Log.d(TAG, "================accept " + integer);
            }
        });


    }

    /**
     * 5. fromFuture 操作符
     * 参数中的 Future 是 java.util.concurrent 中的 Future，Future 的作用是增加了 cancel()
     * 等方法操作 Callable，它可以通过 get() 方法来获取 Callable 返回的值
     */
    @SuppressLint("CheckResult")
    private void testFutureTask() {
        FutureTask<String> futureTask = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Log.d(TAG, "CallableDemo is Running");
                return "返回结果";
            }
        });

        Observable.fromFuture(futureTask)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        futureTask.run();
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Log.d(TAG, "================accept " + s);
                    }
                });


    }

    /**
     * 6. fromIterable 直接发送一个 List 集合数据给观察者
     */
    private void testfromIterable() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        Observable.fromIterable(list)
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "=================onSubscribe" + d.toString());
                    }

                    @Override
                    public void onNext(Integer integer) {
                        Log.d(TAG, "=================onNext " + integer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "=================onError ");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "=================onComplete ");
                    }
                });

    }

    /**
     * 7. defer 操作符
     * 这个方法的作用就是直到被观察者被订阅后才会创建被观察者。
     */
    private void testdefer() {
// i 要定义为成员变量
        Integer i = 100;

        Integer finalI = i;
        Observable<Integer> observable = Observable.defer(new Callable<ObservableSource<? extends Integer>>() {
            @Override
            public ObservableSource<? extends Integer> call() throws Exception {
                return Observable.just(finalI);
            }
        });

        i = 200;

        Observer observer = new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Integer integer) {
//                Logger.v("================onNext " + integer);
                Logger.d("================onNext" + integer);
//                Logger.i("================onNext " + integer);
//                Logger.w("================onNext " + integer);
                Logger.e("================onNext " + integer);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        i = 300;
        observable.subscribe(observer);

    }

    /**
     * X. XXX 操作符
     */
    private void testXX() {


    }


    /**
     * 这种形式不可以请求到后台数据
     */
    public void getData() {
//        ApiManager.getInstence().getRetrofit().create(AirFareApiService.class).AirfareAreaList()
        ApiManager.getInstence().getRetrofit().create(AirFareApiService.class)
                .airfareAreaListNoHttResult()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AirfareArea>() {
                    @Override
                    public void onNext(AirfareArea value) {
                        Logger.d("====onNext=" + value.toString());
                    }

                    @Override
                    public void onSubscribe(Disposable d) {
                        Logger.d("====onSubscribe=");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d("====onError=");
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("====onComplete=");
                    }

                });

    }

//
//    public void getData2() {
////        ApiManager.getInstence().getRetrofit().create(AirFareApiService.class).AirfareAreaList()
//
//        Map<String,Object> params = new HashMap<>();
//        params.put("asc", true);
//        params.put("index", 1);
//        params.put("lat", 0);
//        params.put("lng", 0);
//        params.put("name", "");
//        params.put("size", 10);
//        params.put("sort", 0);
//        ApiManager.getInstence().getRetrofit().create(HotelApiService.class)
//                .hotelList(params)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<HotelList>() {
//                    @Override
//                    public void onNext(HotelList value) {
//                        Logger.d("====onNext=" + value.toString());
//                    }
//
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        Logger.d("====onSubscribe=");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Logger.d("====onError=");
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Logger.d("====onComplete=");
//                    }
//
//                });
//
//    }


    /**
     * 这种形式可以请求到后台数据
     */
//    public void getData3() {
//        JSONObject params = new JSONObject();
//        try {
//            params.put("asc", true);
//            params.put("index", 1);
//            params.put("lat", 0);
//            params.put("lng", 0);
//            params.put("name", "");
//            params.put("size", 10);
//            params.put("sort", 0);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), params.toString());
//        ApiManager.getInstence().getRetrofit().create(HotelApiService.class)
//                .hotelList(requestBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<HotelList>() {
//                    @Override
//                    public void onNext(HotelList value) {
//                        Logger.d("====onNext=" + value.toString());
//                    }
//
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        Logger.d("====onSubscribe=");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Logger.d("====onError=");
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Logger.d("====onComplete=");
//                    }
//
//                });
//
//    }





    /**
     * 这种形式可以请求到后台数据
     */
    public void getData4() {
        Map<String, Object> params = new HashMap<>();
        params.put("asc", true);
        params.put("index", 1);
        params.put("lat", 0);
        params.put("lng", 0);
        params.put("name", "");
        params.put("size", 10);
        params.put("sort", 0);
        ApiManager.getInstence().getRetrofit().create(HotelApiService.class)
                .hotelListNoHttResult(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HotelList>() {
                    @Override
                    public void onNext(HotelList value) {
                        Logger.d("====onNext=111111" + value.toString());
                    }

                    @Override
                    public void onSubscribe(Disposable d) {
                        Logger.d("====onSubscribe=111111=");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d("====onError==11111111");
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("====onComplete=1111111=");
                    }

                });

    }



    /**
     * 这种形式可以请求到后台数据
     */
    public void getDataOther() {

//        Map<String, Object> params = new HashMap<>();
//        params.put("asc", true);
//        params.put("index", 1);
//        params.put("lat", 0);
//        params.put("lng", 0);
//        params.put("name", "");
//        params.put("size", 10);
//        params.put("sort", 0);
        RetrofitManager.getInstence().createWuYuOtherService(OtherApiService.class)
                .selectImportantMessage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Other>() {
                    @Override
                    public void onNext(Other value) {
                        Logger.d("====onNext===2222222" + value.toString());
                    }

                    @Override
                    public void onSubscribe(Disposable d) {
                        Logger.d("====onSubscribe=2222222");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d("====onError=222222");
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("====onComplete=22222");
                    }

                });

    }


    /**
     * 获取机票地区列表
     */
    public void getAirfareAreaList() {
        Map<String, Object> params = new HashMap<>();
        params.put("asc", true);
        params.put("index", 1);
        params.put("lat", 0);
        params.put("lng", 0);
        params.put("name", "");
        params.put("size", 10);
        params.put("sort", 0);
        HttpMethods.getInstance().getAirfareAreaData(new OnNextObserver<AirfareAreaVo>() {
            @Override
            public void onNext(AirfareAreaVo value) {
                Logger.d("====onNext=getAirfareAreaList" +value.toString());
            }
        },params);
    }


    /**
     * 获取酒店列表
     */
    public void getHotelList() {
        Map<String, Object> params = new HashMap<>();
        params.put("asc", true);
        params.put("index", 1);
        params.put("lat", 0);
        params.put("lng", 0);
        params.put("name", "");
        params.put("size", 10);
        params.put("sort", 0);
        HttpMethods.getInstance().hotelList(new OnNextObserver<HotelListVo>() {
            @Override
            public void onNext(HotelListVo value) {
//                Logger.json(value.toString());
            }
        },params);

    }
}
