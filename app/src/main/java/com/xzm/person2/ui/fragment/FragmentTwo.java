package com.xzm.person2.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xzm.baselibrary.utils.LogUtils;
import com.xzm.person2.R;
import com.xzm.person2.ui.activity1.MainActivity;
import com.xzm.person2.ui.adapter.DemoItemAdapter;
import com.xzm.person2.ui.entity.DemoItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentTwo extends Fragment {
    TextView textView;
    private DemoItemAdapter mAdapter;
    private List<DemoItem> mlist;
    private RecyclerView recyclerViewDemoItem;

    public static FragmentTwo newInstance(String text) {
        FragmentTwo fragmentCommon = new FragmentTwo();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);
//        textView= (TextView) view.findViewById(R.id.textView);
//        textView.setText(getArguments().getString("text"));
        initView(view);
        initDates();
        return view;
    }

    private void initView(View view) {
        recyclerViewDemoItem = view.findViewById(R.id.recyclerView_demo_item);
        recyclerViewDemoItem.setLayoutManager(new GridLayoutManager(getContext(), 2));
        TextView textView = view.findViewById(R.id.tv_toolbaer_title);
        textView.setText(getArguments().getString("text"));
    }

    public void initDates() {
        mlist = new ArrayList<>();
        mAdapter = new DemoItemAdapter(R.layout.item_demo, mlist);
        mAdapter.openLoadAnimation();
        recyclerViewDemoItem.setHasFixedSize(true);
        recyclerViewDemoItem.setAdapter(mAdapter);
        addBaseDate();
    }

    private void addBaseDate() {
        mlist.add(new DemoItem(MainActivity.class, "自动接收短信"));
//        LogUtils.d("====FragmentTwo=大小=" + mlist.size());
        mAdapter.notifyDataSetChanged();
    }
}
