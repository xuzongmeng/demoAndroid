package com.xzm.person2.ui.activity1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.xzm.baselibrary.utils.ToastUtil;
import com.xzm.person2.R;
import com.xzm.person2.ui.widget.ExistLoginDialog;


/**
 * 被观察者（Observable）
 * 观察者（Observer）
 * 订阅（subscribe）
 * 原文地址 :https://juejin.im/post/5b17560e6fb9a01e2862246f
 */
public class DialogActivity extends AppCompatActivity implements ExistLoginDialog
        .OnExistLoignListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        initView();
    }

    private void initView() {
        TextView tv_show_dialog = findViewById(R.id.tv_show_dialog);
        tv_show_dialog.setOnClickListener(v -> {
            showDialog();
        });
    }

    private void showDialog() {
        ExistLoginDialog mDialog = new ExistLoginDialog(this).builder();
        mDialog.show();
        mDialog.setOnExistLoignListener(this);
    }

    @Override
    public void onExist() {
        ToastUtil.showToast("退出登录");
    }
}
