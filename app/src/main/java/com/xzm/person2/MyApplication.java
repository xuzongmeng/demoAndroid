package com.xzm.person2;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.xzm.baselibrary.app.APPConfig;
import com.xzm.baselibrary.utils.SpUtil;

public class MyApplication extends Application {
    private static Context appContext;

    public static Context getContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        initLogger();
        SpUtil.initContext(this);
    }

    /**
     * 初始化日志
     */
    private void initLogger() {
        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                return APPConfig.DEBUG;
            }
        });
    }
}
